<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;


/*Desenvolvimento*/
//$db['default']['database'] = "desenvolvimento_kimyto_dwpdr";
//$db['db_cliente']['database'] = "desenvolvimento_kimyto_dwpdr_integracao";
/*Produção*/
$db['db_cliente']['database'] = "kimyto_dwpdr_integracao";
$db['default']['database'] = "kimyto_dwpdr";


$db['default']['hostname'] = "localhost";
$db['default']['username'] = "postgres";
$db['default']['password'] = "dw040708";

$db['default']['dbdriver'] = 'postgre';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;
$db['default']['port'] = 5432;

$db['db_cliente']['hostname'] = 'localhost';
$db['db_cliente']['username'] = "postgres";
$db['db_cliente']['password'] = "dw040708";

$db['db_cliente']['dbdriver'] = "postgre";
$db['db_cliente']['dbprefix'] = "";
$db['db_cliente']['pconnect'] = FALSE;
$db['db_cliente']['db_debug'] = TRUE;
$db['db_cliente']['cache_on'] = FALSE;
$db['db_cliente']['cachedir'] = "";
$db['db_cliente']['char_set'] = "utf8";
$db['db_cliente']['dbcollat'] = "utf8_general_ci";
$db['db_cliente']['swap_pre'] = '';
$db['db_cliente']['autoinit'] = TRUE;
$db['db_cliente']['stricton'] = FALSE;
$db['db_cliente']['erp'] = "protheus";
$db['db_cliente']['port'] = 5432;


/* End of file database.php */
/* Location: ./application/config/database.php */