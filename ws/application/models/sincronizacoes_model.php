<?php

class Sincronizacoes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		//$this->db_cliente = $this->load->database('db_cliente', TRUE);
		//$this->config->load('db_cliente_' . $this->db_cliente->erp);
        //$this->_db_cliente = $this->config->item('db_cliente');
    }
    
	/**
	* Metódo:		salvar_sincronizacao
	* 
	* Descrição:	Função Utilizada para salvar "DADOS (LOGS)" de sincronizações
	* 
	* Data:			21/09/2012
	* Modificação:	21/09/2012
	* 
	* @access		public
	* @param		string 		$tipo					- Model que esta sendo sincronizado (Ex: prospects, pedidos, historico de prospects)
	* @param		json 		$dados					- Dados em json enviados pela sincronização
	* @param		string 		$id_usuario				- ID do usuário que está enviando a sincroniazção
	* @param		string 		$codigo_representante	- CODIGO do REPRESENTANTE que está enviando a sincroniazção
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function salvar_sincronizacao($tipo, $dados, $id_usuario, $codigo_representante)
	{
		
		$data = array(
			'tipo'					=> $tipo,
			'endereco_ip' 			=> $this->input->ip_address(),
			'navegador' 			=> $this->agent->browser() . ' ' . $this->agent->version(),
			'navegador_string' 		=> $this->agent->agent_string(),
			'so' 					=> $this->agent->platform(),
			'url' 					=> $this->agent->referrer(),
			'timestamp' 			=> time(),
			'data' 					=> date('Y-m-d H:i:s'),
			'id_usuario' 			=> $id_usuario,
			'codigo_representante'	=> $codigo_representante,
			'dados' 				=> $dados
		);
		
		$this->db->insert('usuarios_sincronizacoes', $data); 
		
		return $this->db->insert_id();
		
	}
	
	/**
	* Metódo:		salvar_erro
	* 
	* Descrição:	Função Utilizada para salvar "DADOS (LOGS)" de sincronizações com ERRO
	* 
	* Data:			21/09/2012
	* Modificação:	21/09/2012
	* 
	* @access		public
	* @param		string 		$id_sincronizacao					- ID da sincronização realizada epelo metodo "salvar_sincronizacao"
	* @param		string 		$mensagem							- Mensagem com o erro
	* @param		json 		$dados								- dados do sql com erro
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function salvar_erro($id_sincronizacao, $mensagem, $dados)
	{
		$data = array(
			'id_sincronizacao'	=> $id_sincronizacao,
			'mensagem'			=> utf8_encode($mensagem),
			'dados'				=> $dados
		);
		
		$this->db->insert('usuarios_sincronizacoes_erros', $data); 
		
		//Muda a situação do erro para "S" para indicar que há erro
		$this->db->where('id', $id_sincronizacao);
		$this->db->update('usuarios_sincronizacoes', array('erro' => 'S')); 
	}
	

}