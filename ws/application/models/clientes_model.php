<?php

class Clientes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	function exportar_clientes($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		
		$parametros_consulta['id'] 						= $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'codigo', $parametros_consulta);
		
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{

		$id 					= $dados['id'];
		$codigo_empresa 		= $dados['codigo_empresa'];
	
	
	
		//-----------------------------------------------------------------------------------
		//SubSQL
		$this->db_cliente
			->select_max($this->_db_cliente['tabelas']['pedidos'] . '.' . $this->_db_cliente['campos']['pedidos']['codigo'])
			->from($this->_db_cliente['tabelas']['pedidos'])
			->where($this->_db_cliente['campos']['pedidos']['codigo_cliente'], $this->_db_cliente['campos']['clientes']['codigo'], FALSE)
			->where($this->_db_cliente['campos']['pedidos']['loja_cliente'], $this->_db_cliente['campos']['clientes']['loja'], FALSE)
			->where($this->_db_cliente['campos']['pedidos']['delecao'] . ' !=', '*');
		$subSql_ultimo_pedido = $this->db_cliente->sub_sql('ultimo_pedido');
		
		//-----------------------------------------------------------------------------------
	
		// Campos para o SELECT
		// * Retornar todos os campos
		$select = select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], NULL, FALSE);
		
		
		
		
		// * retornar o ultimo pedido
		$select[] = $subSql_ultimo_pedido;
		// *
		
		// * retornar o ultimo pedido
		//$select[] = "(SELECT SUM(" . $this->_db_cliente['campos']['titulos']['valor'] . ") FROM " . $this->_db_cliente['tabelas']['titulos'] . " WHERE " . $this->_db_cliente['campos']['titulos']['codigo_cliente'] . " = " . $this->_db_cliente['campos']['clientes']['codigo'] . " AND " . $this->_db_cliente['campos']['titulos']['loja_cliente'] . " = " . $this->_db_cliente['campos']['clientes']['loja'] . " AND " . $this->_db_cliente['campos']['titulos']['data_vencimento'] . " < '" . date('Ymd') . "' AND CAST(" . $this->_db_cliente['campos']['titulos']['data_baixa'] . " AS TEXT) = '' AND " . $this->_db_cliente['campos']['titulos']['delecao'] . " != '*') AS total_titulos_ventidos";
		// *
		
		//$select += formatar_euf($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], $codigo_empresa);

		$select[] = " cliente.empresa AS empresa, '' AS unidade";
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['clientes']['recno'] . ' >', $id);
		}

		$this->db_cliente->where($this->_db_cliente['campos']['clientes']['delecao'] . ' IS NULL', null);
	
	
		// Consulta
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['clientes']);
		//debug_pre($this->db_cliente->_compile_select());
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
}