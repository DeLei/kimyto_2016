<?php

class Formas_pagamento_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
    	
	
	function exportar_formas_pagamento($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'codigo', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
	
		$id 				= $dados['id'];
		$codigo_empresa 	= $dados['codigo_empresa'];
	
		// Campos para o SELECT
		// * Retornar todos os campos
		$select = select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], NULL, FALSE, 'filial');
		
		$select += formatar_euf($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], $codigo_empresa);
		
		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['codigo'] . ' >', $id);
		}
				
		$this->db_cliente->where($this->_db_cliente['campos']['formas_pagamento']['delecao'] . ' IS NULL', null);
	
	
		// Consulta
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['formas_pagamento']);
		
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] 						= $id;
		$parametros_consulta['codigo_empresa']			= NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	

}