<?php

class Regioes_vendas_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_regioes($id = NULL, $pacote = NULL, $id_usuario = NULL)
	{
		
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['id_usuario'] 			= $id_usuario;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'regiao_venda.id', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
		
		$id 					= $dados['id'];
		$id_usuario 			= $dados['id_usuario'];
		$codigo_empresa 		= NULL;
	
		$this->load->model('usuario_regiao_venda_model');
	
		$regioes_venda_representante = $this->usuario_regiao_venda_model->obter_codigos_regioes_representante($id_usuario);
	
		

		// * Retornar todos os campos
		$select_regiao_venda = select_all($this->_db_cliente['tabelas']['regiao_venda'], $this->_db_cliente['campos']['regiao_venda'], NULL, FALSE,'data_validade');
		
		$select_regiao_venda[] = "to_char(".$this->_db_cliente['campos']['regiao_venda']['data_validade'] .", 'yyyymmdd') AS data_validade ";
		
		$this->db_cliente->where_in($this->_db_cliente['campos']['regiao_venda']['codigo'], $regioes_venda_representante );
		
			
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['regiao_venda'] . '.' . $this->_db_cliente['campos']['regiao_venda']['recno'] . ' >', $id);
		}

	
		// Consulta
		$this->db_cliente->select($select_regiao_venda, false)->from($this->_db_cliente['tabelas']['regiao_venda']);
		
		
		//debug_pre($this->db_cliente->_compile_select());
		
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $id_usuario)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['id_usuario'] = $id_usuario;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	
	

}