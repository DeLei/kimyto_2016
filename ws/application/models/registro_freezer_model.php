<?php

class Registro_freezer_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }
	
	/**
	* Metódo:		exportar_eventos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de Transportadoras
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_freezer($id = NULL, $pacote = NULL)
	{
		
		
		$parametros_consulta['id'] 						= $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar transportadoreas
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id = $dados['id'];
	
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Filiais a partir deste ID
			$this->db_cliente->where(array('id >' => $id));
		}
		
		// Selecionar
		$this->db_cliente->select('id');	
		$this->db_cliente->select('codigo_cliente');	
		$this->db_cliente->select('codigo_freezer');	
		$this->db_cliente->select('temperatura_freezer');	
		$this->db_cliente->select('limpeza');	
		$this->db_cliente->select('manutencao');
		$this->db_cliente->select('data');
		$this->db_cliente->select('codigo');
		$this->db_cliente->select('empresa');
		$this->db_cliente->select('importado');
		$this->db_cliente->select('\'1\' exportado', false);
			
		// Consulta
		$this->db_cliente->from('registro_freezer');
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	/**
	* Metódo:		importar
	* 
	* Descrição:	Função Utilizada para inserir registros de freezers no banco, e inserir LOGS
	* 
	* Data:			31/03/2016
	* Modificação:	31/03/2016
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW força de vendas
	* @param		string 		$id_usuario					- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{

		foreach(json_decode($dados) as $registroFreezer) {
			$this->load->model('sincronizacoes_model');
			
			$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('registro_freezer', json_encode($registroFreezer), $id_usuario, $codigo_representante);
			
			$notificacao = '';

			if($id_sincronizacao)
			{
				$registros = $registroFreezer;
				
				$registrosFreezersBatch = array();

				$registrosFreezersBatch[] = array(
					/* Informações de Cabeçalho */
					'codigo_cliente' => $registroFreezer->codigo_cliente,
					'codigo_freezer' => $registroFreezer->codigo_freezer,
					'temperatura_freezer' => $registroFreezer->temperatura_freezer,
					'limpeza' => $registroFreezer->limpeza,
					'manutencao' => $registroFreezer->manutencao,
					'codigo' => $registroFreezer->codigo,							
					'empresa' => $registroFreezer->empresa,
					'importado' => $registroFreezer->importado,
					'data' => $registroFreezer->data
				);

				try{		
				
					$this->db_cliente->insert_batch('registro_freezer', $registrosFreezersBatch);

				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($registros)); 
				}	
				
			}
			
		}

		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
				
		if($dados_erros)
		{
			$codigos_erro = array();
		
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->codigo_cliente, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->codigo_cliente . ' - ' . $dados_json->codigo_filial;
					$nome_erro[] = $dados_json->codigo_cliente . ' - ' . $dados_json->codigo_filial;
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');//, 'notificacao' => $notificacao);
		}
	}
	


}