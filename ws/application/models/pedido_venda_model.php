<?php

class Pedido_venda_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
	
	
	/**
	* Metódo:		exportar_pedidos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados dos Pedidos
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_pedidos($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
	
		$parametros_consulta['id'] 					 = $id;
	
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['tabelas']['pedido_venda'] . '.' . $this->mapeamento['campos']['pedido_venda']['id'], $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		
		$id 					= $dados['id'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['pedido_venda'], $this->_db_cliente['campos']['pedido_venda'], '',FALSE, 'data');
		
		$select_pedido[] = "to_char(".$this->_db_cliente['campos']['pedido_venda']['data'] .", 'yyyymmdd') AS data ";
		$select_pedido[] = "to_char(".$this->_db_cliente['campos']['pedido_venda']['regiao_data_validade'] .", 'yyyymmdd') AS regiao_data_validade ";
		
		$select = array_merge(
			$select_pedido, 
			array()
			
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		
		
		

		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['pedido_venda'] . '.' . $this->_db_cliente['campos']['pedido_venda']['id'] . ' >', $id);
		}
		
		
		
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['pedido_venda']);
		
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_empresa']		 = NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	//--------------
	//------------------
	//--------------------
	//------------------
	//--------------
	/**
	* Metódo:		importar
	* 
	* Descrição:	Função Utilizada para inserir pedidos no banco, e inserir LOGS
	* 
	* Data:			08/09/2012
	* Modificação:	08/09/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW força de vendas
	* @param		string 		$id_usuario					- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$dadosPedidos = array();

		foreach(json_decode($dados) as $dadoPedido) {
			if (!(array_key_exists($dadoPedido->pedido_id_pedido, $dadosPedidos))) {
				$dadosPedidos[$dadoPedido->pedido_id_pedido] = array();
			}
			$dadosPedidos[$dadoPedido->pedido_id_pedido][] = $dadoPedido;
		}

		
		
		
		foreach($dadosPedidos as $pedidoProcessar) {
			$this->load->model('sincronizacoes_model');
			
			$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pedido_venda', json_encode($pedidoProcessar), $id_usuario, $codigo_representante);
			
			$notificacao = '';

			if($id_sincronizacao)
			{
				$pedido = $pedidoProcessar;
				
				$pedidosBatch = array();

				foreach($pedido as $pedidoItem) {
					
						$pedidosBatch[] = array(

							/* Informações de Cabeçalho */
							'codigo_cliente' => $pedidoItem->codigo_cliente,
							'codigo_empresa' => $pedidoItem->codigo_empresa,
							'codigo_filial' => $pedidoItem->codigo_filial,
							'codigo_vendedor' => $pedidoItem->codigo_vendedor,
							'codigo_motivo' => $pedidoItem->codigo_motivo,
							'codigo_pedido' => $pedidoItem->codigo_pedido,							
							'regiao_venda' => $pedidoItem->regiao_venda,
							'regiao_data_validade' => $pedidoItem->regiao_data_validade,
							'data' => $pedidoItem->data
						);
					}
				

				try{		
				
					$this->db_cliente->insert_batch('pedido_venda', $pedidosBatch);

				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($pedido)); 
				}	
				
			}
			
		}

		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
				
		if($dados_erros)
		{
			$codigos_erro = array();
		
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->codigo_cliente, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->codigo_cliente . ' - ' . $dados_json->codigo_filial;
					$nome_erro[] = $dados_json->codigo_cliente . ' - ' . $dados_json->codigo_filial;
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');//, 'notificacao' => $notificacao);
		}
	}
	



	
	/**
	* Metódo:		obter_campos_valores
	* 
	* Descrição:	Função Utilizada para retornar os campo com valores
	* 
	* Data:			21/09/2012
	* Modificação:	21/09/2012
	* 
	* @access		public
	* @param		array 		$pedido				- Dados dos Pedidos 
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_campos_valores($pedido, $gerar_recno = TRUE)
	{
		foreach($pedido as $indice => $valor)
		{
			if($indice == 'filial')
			{
				continue;
			}

			$indice = str_replace("pedido_", "", $indice, $count);
			
			if($count > 1)
			{
				$indice = "pedido_" . $indice;
			}

			if($gerar_recno)
			{
				if($indice == 'chave') // gerar Recno
				{
					$valor = $this->gerar_recno();
				}
			}

			if(!empty($this->mapeamento['campos']['pedidos_dw'][$indice]))
			{
				
				if(empty($valor)) // Não podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if(strtoupper($valor) == 'UNDEFINED') // Não podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if($indice == 'time_emissao')
				{
					$valor = (int) $valor;
				}
				
				
				if($indice == 'total_desconto_item') //  Calculando o valor do desconto
				{
					
					//Aplicar desconto em cima do preço de tabela que pedido_preco_unitario
					if($pedido->pedido_desconto1 > 0)
					{
						$pedido->pedido_preco_unitario = $pedido->pedido_preco_unitario - ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto1 / 100));
					}
					
					//Calcular o valor do desconto
					$valor = $pedido->pedido_quantidade * ($pedido->pedido_preco_unitario * ($pedido->pedido_desconto_item / 100));
				}
			
				$valores[$this->mapeamento['campos']['pedidos_dw'][$indice]] = $valor;
			}
			
		}
		
		$valores[$this->mapeamento['campos']['pedidos_dw']['delecao']] = ' ';
		
		return $valores;
	}
	
	


}