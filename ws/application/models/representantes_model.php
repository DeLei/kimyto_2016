<?php

class Representantes_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
    
	function exportar_representante($codigo_representante = NULL)
	{
		$representante = consulta_union_empresas($this, $codigo_representante, FALSE);
		
		return $representante;
	}
	
	//Consulta realizada no ERP
	function consulta_erp($dados = NULL)
	{
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		//--------------------------------------
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		
		//--------------------------------------
	
		$select = select_all($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes'], NULL, FALSE, 'filial');
		
		//$compartilhamentoSelect = formatar_euf($this->_db_cliente['tabelas']['representantes'], $this->_db_cliente['campos']['representantes']['filial'], $codigo_empresa);
	
		$this->db_cliente
			->select($select)
			//->select($compartilhamentoSelect, false)
			->from($this->_db_cliente['tabelas']['representantes'])
			->where(array(
				$this->_db_cliente['campos']['representantes']['codigo'] => $codigo_representante,
				$this->_db_cliente['campos']['representantes']['delecao'] . ' IS NULL' => null
			));
			
	}
	
	
	function obter_codigos_representantes()
	{
	
		$dados = $this->db
		->select('usuarios.id')
		->select('codigo')		
		->select('key')
		->from('usuarios_aparelhos')
		->join('usuarios', 'usuarios.id = usuarios_aparelhos.usuarios_id AND usuarios.codigo = usuarios_aparelhos.usuarios_codigo', 'left')
		->where(
			array(
				'usuarios.status' => 'ativo'
			)
		)
		->where_in('usuarios.grupo', array('representantes', 'supervisores'))
		->get()->result_array();

		
		$_codigos = array();
		
		if($dados)
		{
			
			foreach($dados as $representante)
			{
				$_codigos[] = array(
											'key' 		=> trim($representante['key']),
											'codigo' 	=> $representante['codigo'],
											'id' 		=> $representante['id'],
										);
			}
		}
		
		
		return $_codigos;
	}
	

}