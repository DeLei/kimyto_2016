<?php

class Motivo_Nao_Venda_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_motivo_nao_venda($id = NULL, $pacote = NULL)
	{
		
		$parametros_consulta['id'] 					 = $id;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'motivo_nao_venda.id', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$codigo_empresa 		= NULL;
		
	
		
	
		// * Retornar todos os campos
		$select_motivo_nao_venda = select_all($this->_db_cliente['tabelas']['motivo_nao_venda'], $this->_db_cliente['campos']['motivo_nao_venda'], '');
		
		

		
		// Condições do SQL (WHERE)
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['tabelas']['motivo_nao_venda'] . '.' . $this->_db_cliente['campos']['motivo_nao_venda']['recno'] . ' >', $id);
		}

		
		
		
	
		// Consulta
		$this->db_cliente->select($select_motivo_nao_venda, false)->from($this->_db_cliente['tabelas']['motivo_nao_venda']);
		
		//debug_pre($this->db_cliente->_compile_Select());
	
		
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id)
	{	
		$parametros_consulta['id'] = $id;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	
	

}