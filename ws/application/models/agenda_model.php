<?php

class Agenda_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
    }
	
	/**
	* Metódo:		exportar_agenda
	* 
	* Descrição:	Função Utilizada para pegar retornar dados de comprimissos da agenda
	* 
	* Data:				09/09/2013
	* Modificação:	09/09/2013
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_agenda($id = NULL, $pacote = NULL, $id_usuario = NULL)
	{
		$parametros_consulta['id'] 							= $id;
		$parametros_consulta['id_usuario'] 					= $id_usuario;
		
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, NULL, $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar dados de comprimissos da agenda
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{	
		$id 				= $dados['id'];
		$id_usuario 	= $dados['id_usuario'];
		
		// Condições do SQL (WHERE)
		if($id)
		{
			// Buscar Compromissos a partir deste ID
			$this->db->where(array('id >' => $id));
		}
		
		// Condições do SQL (WHERE)
		if($id_usuario)
		{
			// Travando por usuário
			$this->db->where(array('usuarios.id' => $id_usuario));
		}
	
		// Selecionar
		$this->db->select('usuarios.status as usuario_status');
		$this->db->select('usuarios.nome as usuario_nome');
		$this->db->select('compromissos.id as id');	
		$this->db->select('compromissos.timestamp as timestamp');	
		$this->db->select('compromissos.id_usuario as id_usuario');	
		$this->db->select('compromissos.cpf as cpf');	
		$this->db->select('compromissos.status as status');	
		$this->db->select('compromissos.nome as nome');	
		$this->db->select('compromissos.titulo as titulo');	
		$this->db->select('compromissos.descricao as descricao');	
		$this->db->select('compromissos.tipo as tipo');
		$this->db->select('compromissos.codigo_tipo as codigo_tipo');
		$this->db->select('compromissos.id_usuario_cad as id_usuario_cad');
		$this->db->select('compromissos.id_representante as id_representante');
		$this->db->select('compromissos.detalhes as detalhes');
		$this->db->select('compromissos.inicio_timestamp as inicio_timestamp');
		$this->db->select('compromissos.final_timestamp as final_timestamp');
		$this->db->select('compromissos.class as class');
		$this->db->select('compromissos.latitude as latitude');
		$this->db->select('compromissos.longitude as longitude');
		$this->db->select('compromissos.versao as versao');
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$this->db->select("'1' as exportado", false);
		$this->db->select("'0' as editado", false);
		$this->db->select("'0' as remover", false);
		$this->db->select("'0' as erro", false);
		
		//Join
		$this->db->join('usuarios', 'usuarios.id = compromissos.id_usuario_cad');
			
		// Consulta
		$this->db->from('compromissos');
	}
	
	/**
	* Metódo:			importar
	* 
	* Descrição:		Função Utilizada para inserir compromisso no banco, e inserir LOGS
	* 
	* Data:				17/09/2013
	* Modificação:	17/09/2013
	* 
	* @access		public
	* @param		json 			$dados							- Dados das Pendências enviados pelo DW força de vendas
	* @param		string 		$id_usuario						- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('pendencias', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$compromissos = json_decode($dados);
			
			foreach($compromissos as $compromisso)
			{
				try{	
				
					$valores['timestamp']						=	(int) isset_valor($compromisso->timestamp);					
						
					$valores['id_usuario'] 						=	isset_valor($compromisso->id_usuario);
					$valores['id_usuario_cad'] 					=	isset_valor($compromisso->id_usuario_cad);
					$valores['id_representante'] 				=	isset_valor($compromisso->id_representante);
					
					$valores['detalhes'] 							=	isset_valor($compromisso->detalhes);
					$valores['status'] 							=	isset_valor($compromisso->status);
					$valores['tipo'] 								=	isset_valor($compromisso->tipo);
					
					$valores['nome'] 								=	isset_valor($compromisso->nome);
					$valores['codigo_tipo'] 						=	isset_valor($compromisso->codigo_tipo);
					
					$valores['titulo'] 								=	isset_valor($compromisso->titulo);
					$valores['descricao'] 						=	isset_valor($compromisso->descricao);
					
					$valores['inicio_timestamp'] 				=	isset_valor($compromisso->inicio_timestamp);
					$valores['final_timestamp'] 				=	isset_valor($compromisso->final_timestamp);
						
					$valores['class'] 								=	isset_valor($compromisso->class);
					
					$valores['latitude'] 							=	isset_valor($compromisso->latitude);					
					$valores['longitude'] 						=	isset_valor($compromisso->longitude);					
					$valores['versao'] 							=	isset_valor($compromisso->versao);				
					
					//ADICIONAR PENDÊNCIA
					if($compromisso->remover == '' AND $compromisso->editado == '' AND $compromisso->exportado == '')
					{	
						//Adicionar a ligação entre a pendência e o usuário
						$this->db->insert('compromissos', $valores);
					}
					
					//ENCERRAR A PENDÊNCIA
					if(trim($compromisso->editado) == '1')
					{
						//Executar UPDATE na pendência
						$this->db->update('compromissos', $valores, array('id' => $compromisso->id));	
					}
					
					//REMOVER PENDÊNCIA
					if($compromisso->remover == '1')
					{
						$this->db->delete('compromissos', array('id'  => $compromisso->id));
					}
				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($compromissos)); 
				}
			}
		}
		
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
			
		if($dados_erros)
		{
			$codigos_erro = array();
		
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->id, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->id;
					$nome_erro[] = $dados_json->id . ' - ' . $dados_json->titulo;
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');
		}
	}
	
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $id_usuario)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['id_usuario'] = $id_usuario;
	
		return retornar_total($this, $parametros_consulta);
	}

}