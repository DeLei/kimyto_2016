<?php

class Usuario_regiao_venda_model extends CI_Model {


	public $compartilhada = TRUE;

    function __construct()
    {
        parent::__construct();
		
	//	$this->db_cliente = $this->load->database('db_cliente', TRUE);
    }

	
	function exportar_regioes($id = NULL, $pacote = NULL, $id_usuario = NULL)
	{
		
		$parametros_consulta['id'] 					= $id;
		$parametros_consulta['id_usuario'] 			= $id_usuario;
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, 'usuario_regiao_venda.regiao_codigo', $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
    
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que serra executado para retornar clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados)
	{
	
		$id 					= $dados['id'];
		$id_usuario 			= $dados['id_usuario'];
		$codigo_empresa 		= NULL;
	
		// Condições do SQL (WHERE)
		if($id_usuario)
		{
			// Travando por usuário
			$this->db->where(array('usuario_regiao_venda.usuario_id' => $id_usuario));
		}
	
		
	
		// Consulta
		$this->db->from('usuario_regiao_venda');
		
		
		//debug_pre($this->db->_compile_select());
		
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de clientes
	* 
	* Data:			11/09/2012
	* Modificação:	11/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id, $id_usuario)
	{	
		$parametros_consulta['id'] = $id;
		$parametros_consulta['id_usuario'] = $id_usuario;
		
		return retornar_total($this, $parametros_consulta);
	}
	
	
	
	
	function obter_codigos_regioes_representante($id_usuario){
		$regioes_representante= array();
		// Travando por usuário
		
		$this->db->select('regiao_codigo');
		
		$this->db->where(array('usuario_regiao_venda.usuario_id' => $id_usuario));
	
		// Consulta
		$this->db->from('usuario_regiao_venda');
		
		
		
		$dados  = $this->db->get()->result_array();
		foreach($dados as $regiao){
			$regioes_representante[] = $regiao['regiao_codigo'];
		
		}
		
		
		return $regioes_representante;
	}	

}