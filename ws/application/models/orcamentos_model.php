<?php

class Orcamentos_model extends CI_Model {


    function __construct()
    {
        parent::__construct();
		
		$this->db_cliente = $this->load->database('db_cliente', TRUE);
		$this->mapeamento = mapeamento($this->config->config, $this->config->item('empresa_matriz'));
    }
	
	
	/**
	* Metódo:		exportar_orcamentos
	* 
	* Descrição:	Função Utilizada para pegar retornar dados dos Orçamentos
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$pacote					- Utilizado para informar qual "pagina ou pacote" deve retornar
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function exportar_orcamentos($id = NULL, $pacote = NULL, $codigo_representante = NULL)
	{
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
	
		// Consulta com Pacote de Dados
		$dados = pacote_dados($this, $pacote, FALSE, $this->mapeamento['tabelas']['pedidos_dw'] . '.' . $this->mapeamento['campos']['pedidos_dw']['id_pedido'], $parametros_consulta);
		
		// Retorno Dados
		return $dados;
	}
	
	/**
	* Metódo:		consulta
	* 
	* Descrição:	Função Utilizada para construir o SQL que sera executado para retornar dados
	* 
	* Data:			18/09/2012
	* Modificação:	18/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @param		string 		$codigo_representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function consulta($dados = NULL)
	{
		$id 					= $dados['id'];
		$codigo_representante	= $dados['codigo_representante'];
		$codigo_empresa 		= $dados['codigo_empresa'];
		
		$select_pedido 				= select_all($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw'], 'pedido');
		$select_cliente 			= select_all($this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes'], 'cliente');
		$select_prospect 			= select_all($this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects'], 'prospect');
		$select_produto 			= select_all($this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos'], 'produto');
		$select_formas_pagamento 	= select_all($this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento'], 'forma_pagamento');
		$select_transportadoras 	= select_all($this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras'], 'transportadora');
		
		$select = array_merge(
			$select_pedido, 
			$select_cliente,
			$select_prospect,
			$select_produto,
			$select_formas_pagamento,
			$select_transportadoras
		);
		
		//Campo exportar foi adicionado para ser criado no banco de dados do navegador (Valor "1" = Exportado, Valor "" = Não Exportado)
		$select[] = "'1' as exportado";
		$select[] = "'0' as editado";
		$select[] = "'0' as erro";
		$select[] = "'0' as converter_pedido_orcamento";

		
		$select += formatar_euf($this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial'], $codigo_empresa);

		// Where
		//-----------------------------------------------
		
		if($id)
		{
			$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['id_pedido'] . ' >', $id);
		}
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['codigo_representante'], $codigo_representante);
		
		$this->db_cliente->where($this->_db_cliente['campos']['pedidos_dw']['tipo_venda'], '*'); // Buscar Orçamentos
		
		$this->db_cliente->where($this->_db_cliente['tabelas']['pedidos_dw'] . '.' . $this->_db_cliente['campos']['pedidos_dw']['delecao'] . ' IS NULL', null);
		
		//-----------------------------------------------
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['clientes'], 
			euf(
				$this->_db_cliente['tabelas']['clientes'], $this->_db_cliente['campos']['clientes']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['delecao'] . ' IS NULL '. ' AND ' .
			" ((".$this->_db_cliente['tabelas']['clientes'].'.'.$this->_db_cliente['campos']['clientes']['codigo'] . ' = ' . $this->_db_cliente['tabelas']['pedidos_dw'] . '.'. $this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'] . ' AND ' .
			$this->_db_cliente['tabelas']['clientes'] . '.' . $this->_db_cliente['campos']['clientes']['loja'] . ' = ' . $this->_db_cliente['tabelas']['pedidos_dw'] .'.'. $this->_db_cliente['campos']['pedidos_dw']['loja_cliente'] . ') OR ' .
			$this->_db_cliente['campos']['clientes']['cpf'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['id_prospects']. ' AND '.$this->_db_cliente['campos']['pedidos_dw']['codigo_cliente'].' IS NULL)'			
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join($this->_db_cliente['tabelas']['prospects'], 
			euf(
				$this->_db_cliente['tabelas']['prospects'], $this->_db_cliente['campos']['prospects']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['cgc'] . ' = ' . $this->_db_cliente['campos']['pedidos_dw']['id_prospects'] . ' AND ' .
			$this->_db_cliente['tabelas']['prospects'] . '.' . $this->_db_cliente['campos']['prospects']['delecao'] . " IS NULL"
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['produtos'], 
			euf(
				$this->_db_cliente['tabelas']['produtos'], $this->_db_cliente['campos']['produtos']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['produtos'] . '.'. $this->_db_cliente['campos']['produtos']['codigo'] . " = " . $this->_db_cliente['campos']['pedidos_dw']['codigo_produto'] . " AND " .
			$this->_db_cliente['tabelas']['produtos'] . "." . $this->_db_cliente['campos']['produtos']['delecao'] . " IS NULL"
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['formas_pagamento'], 
			euf(
				$this->_db_cliente['tabelas']['formas_pagamento'], $this->_db_cliente['campos']['formas_pagamento']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['formas_pagamento'] . '.' . $this->_db_cliente['campos']['formas_pagamento']['codigo'] . " = " . $this->_db_cliente['tabelas']['pedidos_dw'] . '.'.  $this->_db_cliente['campos']['pedidos_dw']['condicao_pagamento'] . " AND " .
			$this->_db_cliente['tabelas']['formas_pagamento'] . "." . $this->_db_cliente['campos']['formas_pagamento']['delecao'] . " IS NULL"
		, 'left', false);
		
		//-- Join
		$this->db_cliente->join(
			$this->_db_cliente['tabelas']['transportadoras'], 
			euf(
				$this->_db_cliente['tabelas']['transportadoras'], $this->_db_cliente['campos']['transportadoras']['filial'], 
				$this->_db_cliente['tabelas']['pedidos_dw'], $this->_db_cliente['campos']['pedidos_dw']['filial']
			) .
			$this->_db_cliente['tabelas']['transportadoras'] . '.'. $this->_db_cliente['campos']['transportadoras']['codigo'] . " = " . $this->_db_cliente['tabelas']['pedidos_dw']  . '.'. $this->_db_cliente['campos']['pedidos_dw']['codigo_transportadora'] . " AND " .
			$this->_db_cliente['tabelas']['transportadoras'] . "." . $this->_db_cliente['campos']['transportadoras']['delecao'] . " IS NULL"
		, 'left');
		
		$this->db_cliente->select($select, false)->from($this->_db_cliente['tabelas']['pedidos_dw']);
	}
	
	/**
	* Metódo:		retornar_total
	* 
	* Descrição:	Função Utilizada para retornar o número total de transportadoras
	* 
	* Data:			10/09/2012
	* Modificação:	10/09/2012
	* 
	* @access		public
	* @param		string 		$id						- Utilizado para retornar Registros a partir do ID informado
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function retornar_total($id = NULL, $codigo_representante = NULL)
	{	
		$parametros_consulta['id'] 					 = $id;
		$parametros_consulta['codigo_representante'] = $codigo_representante;
		$parametros_consulta['codigo_empresa']		 = NULL;
	
		return retornar_total($this, $parametros_consulta);
	}
	
	//--------------------------------------------------------
	
	/**
	* Metódo:		importar
	* 
	* Descrição:	Função Utilizada para inserir orçamentos no banco, e inserir LOGS
	* 
	* Data:			16/11/2012
	* Modificação:	16/11/2012
	* 
	* @access		public
	* @param		json 		$dados						- Dados dos Pedidos enviados pelo DW força de vendas
	* @param		string 		$id_usuario					- ID do usuário
	* @param		string 		$codigo_representante		- Codigo do Representante
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function importar($dados, $id_usuario, $codigo_representante)
	{
		$this->load->model('sincronizacoes_model');
		
		$id_sincronizacao = $this->sincronizacoes_model->salvar_sincronizacao('orcamentos_dw', $dados, $id_usuario, $codigo_representante);
		
		if($id_sincronizacao)
		{
			$orcamentos = json_decode($dados);
			
			foreach($orcamentos as $orcamento)
			{

				try{
				
					if($this->validar_orcamento($orcamento->pedido_id_pedido, $orcamento->pedido_codigo_produto))
					{
						$orcamento->editado = 1;
					}
				
					// Se editado == 1 - Editar o Pedidos, senao, inserir
					if($orcamento->editado == 1)
					{
						$valores = $this->obter_campos_valores($orcamento, FALSE);
					
						$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['id_pedido'], $orcamento->pedido_id_pedido);
						$this->db_cliente->where($this->mapeamento['campos']['pedidos_dw']['codigo_produto'], $orcamento->pedido_codigo_produto);
						$this->db_cliente->update($this->mapeamento['tabelas']['pedidos_dw'], $valores);
					}
					else
					{
						$valores = $this->obter_campos_valores($orcamento);
					
						$this->db_cliente->insert($this->mapeamento['tabelas']['pedidos_dw'], $valores);
					}
				}
				catch(Exception $e)
				{
					$this->sincronizacoes_model->salvar_erro($id_sincronizacao, $e->getMessage(), json_encode($orcamento)); 
				}	
			}
		}
		
		$dados_erros = $this->db->from('usuarios_sincronizacoes_erros')->where('id_sincronizacao', $id_sincronizacao)->get()->result();
			
		if($dados_erros)
		{
			$codigos_erro = array();
			
			foreach($dados_erros as $dado_erro)
			{
				$dados_json = json_decode($dado_erro->dados);

				if (!in_array($dados_json->pedido_id_pedido, $codigos_erro)) 
				{ 
					$codigos_erro[] = $dados_json->pedido_id_pedido;
					$nome_erro[] = $dados_json->pedido_id_pedido . ' - ' . ($dados_json->cliente_nome ? $dados_json->cliente_nome : $dados_json->prospect_nome);
				}
			}
			
			$erros['erro'] = $codigos_erro;
			$erros['erro_descricao'] = $nome_erro;
			
			return $erros;
		}
		else
		{
			return array('sucesso' => 'ok');
		}
		
	}
	
	
	function validar_orcamento($id_pedido, $codigo_produto)
	{
		$pedido = $this->db_cliente->select($this->mapeamento['campos']['pedidos_dw']['id_pedido'])
									->from($this->mapeamento['tabelas']['pedidos_dw'])
									->where(array(
										$this->mapeamento['campos']['pedidos_dw']['id_pedido'] 		=> $id_pedido,
										$this->mapeamento['campos']['pedidos_dw']['codigo_produto'] => $codigo_produto
									))
									->limit(1)->get()->result();
		
		return $pedido;
	}
	
	
	/**
	* Metódo:		obter_campos_valores
	* 
	* Descrição:	Função Utilizada para retornar os campo com valores
	* 
	* Data:			21/09/2012
	* Modificação:	21/09/2012
	* 
	* @access		public
	* @param		array 		$orcamento				- Dados dos Orçamentos 
	* @version		1.0
	* @author 		DevelopWeb Soluções Web
	* 
	*/
	function obter_campos_valores($orcamento, $gerar_recno = TRUE)
	{
		foreach($orcamento as $indice => $valor)
		{
			
			if($indice == 'filial')
			{
				continue;
			}

			$indice = str_replace("pedido_", "", $indice, $count);
			
			if($count > 1)
			{
				$indice = "pedido_" . $indice;
			}

			if($gerar_recno)
			{
				if($indice == 'chave') // gerar Recno
				{
					$valor = $this->gerar_recno();
				}
			}
	
			if(!empty($this->mapeamento['campos']['pedidos_dw'][$indice]))
			{
				
				if(empty($valor)) // Não podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if(strtoupper($valor) == 'UNDEFINED') // Não podemos inserir valor NULL, se for NULL inserir em branco
				{
					$valor = ' ';
				}
				
				if($indice == 'time_emissao')
				{
					$valor = (int) $valor;
				}
			
				$valores[$this->mapeamento['campos']['pedidos_dw'][$indice]] = $valor;
			}
			
		}
		
		$valores[$this->mapeamento['campos']['pedidos_dw']['delecao']] = ' ';
		
		return $valores;
	}
	
	function gerar_recno()
	{
	
		$dados = $this->db_cliente
			->select('MAX(' . $this->mapeamento['campos']['pedidos_dw']['chave'] . ')+1 AS chave')
			->from($this->mapeamento['tabelas']['pedidos_dw'])
			->get()->row_array();
			
	
		if($dados['chave'])
		{
			return $dados['chave'];
		}
		else
		{
			return 1;
		}
	
	}
	

}