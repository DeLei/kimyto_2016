<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Registro_freezer extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('registro_freezer_model');
    }

	function exportar_get()
	{
		$dados = $this->registro_freezer_model->exportar_freezer($this->input->get('id'), $this->input->get('pacote'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Eventos!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->registro_freezer_model->retornar_total($this->input->get('id'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Freezers!'), 404);
        }
	}
	
	function importar_post()
	{
	
		if($this->input->post('retorno') && $this->input->post('id_usuario'))
		{
			$retorno = $this->registro_freezer_model->importar($this->input->post('retorno'), $this->input->post('id_usuario'), $this->input->post('codigo_representante'));

			if($retorno)
			{
				$this->response($retorno, 200);
			}
			else
			{
				$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
			}
		}
		else
		{
			$this->response(array('error' => 'Não foi possível enviar Pedidos!'), 404);
		}
	}
	
}