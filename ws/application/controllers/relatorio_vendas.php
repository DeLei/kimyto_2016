<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Relatorio_Vendas extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('relatorio_vendas_model');
    }

	function exportar_get()
	{
		$dados = $this->relatorio_vendas_model->exportar_vendas($this->input->get('id'), $this->input->get('pacote'), $this->input->get('codigo_representante'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'Não foi possível buscar Relatório de Vendas!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->relatorio_vendas_model->retornar_total($this->input->get('id'), $this->input->get('codigo_representante'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total do Relatório de Vendas!'), 404);
        }
	}
	
}