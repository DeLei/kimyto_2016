<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Usuario_regiao_venda extends REST_Controller
{
	function __construct()
    {
		parent::__construct();
        $this->load->model('usuario_regiao_venda_model');
    }

	function exportar_get()
	{
		$dados = $this->usuario_regiao_venda_model->exportar_regioes($this->input->get('id'), $this->input->get('pacote'), $this->input->get('id_usuario'));
		
		if($dados)
        {
            $this->response($dados, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar produtos!'), 404);
        }
	}
	
	function total_get()
	{
		$total['total'] = $this->usuario_regiao_venda_model->retornar_total($this->input->get('id'), $this->input->get('id_usuario'));
		
		if($total)
        {
            $this->response($total, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Não foi possível buscar Total de Regiões de Vendas!'), 404);
        }
	}
	
}