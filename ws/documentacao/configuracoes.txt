------------------------------------------------------------------------------------------------------------------------------

Documento para configurar o WebService - 1.0

------------------------------------------------------------------------------------------------------------------------------

1 - Base URL

Exemplo:
	$config['base_url']	= 'http://192.168.1.150/projetos/developweb/produtos/dwfdv/protheus/trunk/ws/';

Descrição:
	Definir o caminho "LOCAL" do webservice.
	Esse caminho é utilizado parar gerar pacotes, e precisa ser local, pois pacotes somente será gerado localmente.

------------------------------------------------------------------------------------------------------------------------------

2 - Empresa Matriz

Exemplo:
	$config['empresa_matriz'] = '990'; 

Descrição:
	Definir qual o código da empresa matriz;
	O código é utilizado nas tabelas do protheus (SA1990, SB1990, ...);

------------------------------------------------------------------------------------------------------------------------------

3 - Empresas

Exemplo:
	$config['empresas'] = array (
		'010'	=> 'São Paulo',
		'020'	=> 'Santa Catarina',
	);

Descrição:
	Definir o código e a descrição das empresas utilizadas;
	O código é utilizado nas tabelas do protheus (SA1010, SB1010, SA1020, SB1020, ...);

------------------------------------------------------------------------------------------------------------------------------

4 - Empresas, Unidades e Filial

Exemplo:
	$config['euf'] = TRUE;

Descrição:
	Definir se será utilizado o campo de compartilhamento;

------------------------------------------------------------------------------------------------------------------------------

5 - Empresas EUF

Exemplo:
	$config['empresas_euf'] = array (
		'10'	=> 'São Paulo',
		'20'	=> 'Santa Catarina',
	);
	
	-- OU --
	
	$config['empresas_euf'] = NULL;

Descrição:
	Definir o código e a descrição das empresas utilizadas;
	O código é utilizado nas tabelas do protheus (SA1010, SB1010, SA1020, SB1020, ...);

------------------------------------------------------------------------------------------------------------------------------

6 - Posicao EUF

Exemplo:
	$config['posicao_euf'] = array('E', 'U', 'F');
	
Descrição:
	Organiar qual será o padrão de ordem nos campos;
		 - array('E', 'U', 'F') -> 1º Empresa, 2º Unidade, 3º Filial
		 - array('U', 'E', 'F') -> 1º Unidade, 2º Empresa, 3º Filial
		 - array('F', 'Y', 'E') -> 1º Filial, 2º Unidade, 3º Empresa
	 
------------------------------------------------------------------------------------------------------------------------------

7 - Tabela de Pedidos DW

Exemplo:
	$config['pedidos_dw'] = array (
		'tabela' 	=> 'SZW990',
		'campo'		=> 'ZW'
	);
	
Descrição:
	Definir tabela de Pedidos utilizada para salvar pedidos do Web Service 
	
------------------------------------------------------------------------------------------------------------------------------

8 - Tabela de Prospects

Exemplo:
	$config['prospects'] = array (
		'tabela' 	=> 'SZX990',
		'campo'		=> 'ZX'
	);

	
Descrição:
	Definir tabela de Prospects utilizada para salvar "pré clientes"
	
------------------------------------------------------------------------------------------------------------------------------

8 - Arquivo DBF da tabela SX2

Descrição:
	Se o Webservice "NÃO" utilizar a estrutura de tabelas compartilhadas (EUF), será necessário utilizar o arquivo DBF da tabela SX2.
	Exemplo:
		Arquivo: sx2990.dbf
		Mover arquivo na pasta dbf: dwfdv/ws/dbf/sx2990.dbf
	
	Observação:
		O arquivo dbfprecisa ser solicitado com a TI da empresa.
		
------------------------------------------------------------------------------------------------------------------------------

8 - Gerar Pacotes

Exemplo:
	$config['gerar_arquivo'] = TRUE;
	
Descrição:
	Definir se o cliente irá utiliar pacotes;
	Pacotes é uma rotina criada para salvar dados em json em determinado tempo;
	
------------------------------------------------------------------------------------------------------------------------------

9 - Quantidade por Pacote

Exemplo:
	define('POR_PACOTE', 2500);
	
Descrição:
	Definir a quantidade de registros que o webservice irá retornar por pacote.
	Exemplo:
		Tenho 7500 pedidos. O pacote irá gerar 3 pacotes de 2500 registros.
		Pacote de pedidos 1 - 2500 registors;
		Pacote de pedidos 2 - 2500 registors;
		Pacote de pedidos 3 - 2500 registors;
		
------------------------------------------------------------------------------------------------------------------------------

10 - Tempo de pacote por módulo

Exemplo:
	$config['pacotes'] = array(
		'clientes' 				=> (1 * 1 * 30 * 60),
		'historico_clientes' 	=> (1 * 1 * 30 * 60),
		'historico_prospects' 	=> (1 * 1 * 30 * 60),
		'notas_fiscais' 		=> (1 * 1 * 30 * 60),
		'orcamentos' 			=> (1 * 1 * 30 * 60),
		'pedidos_pendentes' 	=> (1 * 1 * 30 * 60),
		'pedidos_processados' 	=> (1 * 1 * 30 * 60),
		'pendencias' 			=> (1 * 1 * 30 * 60),
		'prospects' 			=> (1 * 1 * 30 * 60),
		'titulos' 				=> (1 * 1 * 30 * 60),
	);
	
Descrição:
	Esse pacote é gerado por representante;
	Definir o tempo que será gerado o pacote de cada módulo:
	Exemplo:
		- Gerar pacotes de "clientes" a cada 1 hora:
			'clientes' 	=> (1 * 60 * 60);
		- Gerar pacotes de "pedidos_processados" a cada 1 dia:
			'pedidos_processados' 	=> (1 * 24 * 60 * 60);
			
------------------------------------------------------------------------------------------------------------------------------

11 - Tempo de pacote por módulo (Únicos)

Exemplo:
	
	$config['pacotes_unicos'] = array(
		'empresas' 				=> (1 * 24 * 60 * 60),
		'eventos' 				=> (1 * 1 * 30 * 60),
		'filiais' 				=> (1 * 24 * 60 * 60),
		'formas_pagamento' 		=> (1 * 1 * 30 * 60),
		'municipios' 			=> (30 * 24 * 60 * 60),
		'noticias' 				=> (1 * 1 * 30 * 60),
		'produtos' 				=> (1 * 1 * 30 * 60),
		'regra_desconto' 		=> (1 * 1 * 30 * 60),
		'tabelas_preco' 		=> (1 * 1 * 30 * 60),
		'transportadoras' 		=> (30 * 1 * 30 * 60),
	);

Descrição:
	Esse pacote é unico;
	Definir o tempo que será gerado o pacote de cada módulo:
	Exemplo:
		- Gerar pacotes de "filiais" a cada 1 hora:
			'filiais' 	=> (1 * 60 * 60);
		- Gerar pacotes de "municipios" a cada 1 dia:
			'municipios' 	=> (1 * 24 * 60 * 60);
			
------------------------------------------------------------------------------------------------------------------------------

12 - Hora da primeira atualização 

Exemplo:
	$config['hora_atualizacao'] = '08:00'; //24horas

Descrição:
	Utilizado para definir a hora da criação dos arquivos (Apenas funciona na criação dos arquivos);
	
	
------------------------------------------------------------------------------------------------------------------------------

13 - Rotina automática de Pacotes

Descrição:
	Quando for definido que será gerado pacotes, será preciso definir uma rotina automática no windows;
	Essa rotina precisa ser chamada no mínimo a cada 1 hora (Definir com o cliente);
	A url para gerar pacotes:
		http://192.168.1.150/dwfdv/ws/pacotes/gerar

