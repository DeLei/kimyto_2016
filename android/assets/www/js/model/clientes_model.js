function obter_cliente(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa, callback){
	var cliente = [];
	db.transaction(function(x) {
		x.executeSql(
			"SELECT * FROM clientes WHERE codigo = ? AND loja = ? AND filial = ? AND empresa = ? ", [codigo_cliente.toString(), codigo_loja.toString(), codigo_filial.toString(), codigo_empresa.toString()], 
			function(x, dados) 
			{		
				
				if( dados.rows.length > 0){
					cliente = dados.rows.item(0);
				}
				
				
				callback(cliente);
			}, function(transaction, error) {
				alert("Error : " + error.message);
			}
		)
	});
	

}

function obter_cliente_freezers(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa, data, callback){
	var clienteFreezers = [];
	db.transaction(function(x) {
		x.executeSql(
			"SELECT freezer.*, registro_freezer.limpeza, registro_freezer.manutencao FROM freezer LEFT JOIN registro_freezer ON registro_freezer.codigo_freezer = freezer.codigo_freezer AND registro_freezer.empresa = freezer.empresa AND registro_freezer.data = ? WHERE freezer.codigo_cliente = ? AND freezer.empresa = ? ", [data.toString(), codigo_cliente.toString(), codigo_empresa.toString()], 
			function(x, dados) 
			{		
				if( dados.rows.length > 0){
					for(var i = 0; i < dados.rows.length; i++) {
						clienteFreezers.push(dados.rows.item(i));
					}
				}

				callback(clienteFreezers);
			}, function(transaction, error) {
				alert("Error : " + error.message);
			}
		)
	});
	

}

function obter_cliente_freezer_registro(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa, codigo_freezer, data, callback){
	var clienteFreezers = [];
	db.transaction(function(x) {
		x.executeSql(
			"SELECT freezer.modelo_freezer, registro_freezer.temperatura_freezer, registro_freezer.limpeza, registro_freezer.manutencao, registro_freezer.data FROM freezer LEFT JOIN registro_freezer ON registro_freezer.codigo_freezer = freezer.codigo_freezer AND registro_freezer.empresa = freezer.empresa AND registro_freezer.data = ? WHERE freezer.codigo_freezer = ? AND freezer.codigo_cliente = ? AND freezer.empresa = ? ", [data.toString(), codigo_freezer.toString(), codigo_cliente.toString(), codigo_empresa.toString()], 
			function(x, dados)  
			{		
				if(dados.rows.length > 0){
					callback(dados.rows.item(0));
				} else {
					callback(null);
				}
			}, function(transaction, error) {
				alert("Error : " + error.message);
			}
		)
	});
	

}


function obter_cliente_pedido(pedido, callback){
	var pedido;
	
	obter_cliente(pedido.pedido_codigo_cliente, pedido.pedido_loja_cliente, pedido.pedido_filial, pedido.empresa, function(cliente){
		
		pedido.cliente = cliente;
		callback(pedido);
		
	});
	
	
	
}


function salvar_pedido_venda_cliente(id_pedido, pedido, callback){
	var campos = [];
	var interrogacoes = [];
	var valores = [];

	
	console.log(JSON.stringify(pedido));
	
	campos.push('codigo_cliente');		
	interrogacoes.push('?');
	valores.push(pedido.codigo_cliente);

	campos.push('codigo_empresa');		
	interrogacoes.push('?');
	valores.push(pedido.dados_cliente.empresa);

	campos.push('codigo_filial');		
	interrogacoes.push('?');
	valores.push(pedido.filial);

	campos.push('codigo_vendedor');		
	interrogacoes.push('?');
	valores.push(info.cod_rep);
		
	campos.push('data');				
	interrogacoes.push('?');
	valores.push(obterDataAtual());
	
	campos.push('codigo_motivo');		
	interrogacoes.push('?');
	valores.push(null);
	
	campos.push('codigo_pedido');		
	interrogacoes.push('?');
	valores.push(id_pedido);
	
	campos.push('exportado');		
	interrogacoes.push('?');
	valores.push(null);
	
	
	campos.push('editado');		
	interrogacoes.push('?');
	valores.push(null);
	
	campos.push('erro');		
	interrogacoes.push('?');
	valores.push(null);
	
	obter_cliente(pedido.codigo_cliente, pedido.dados_cliente.loja, pedido.filial, pedido.dados_cliente.empresa, function(cliente){
		obter_regiao(cliente.codigo_regiao_venda, function(regiao){
			
		
			campos.push('regiao_venda');		
			interrogacoes.push('?');
			valores.push(cliente.codigo_regiao_venda);
		
			campos.push('regiao_data_validade');		
			interrogacoes.push('?');
			valores.push(regiao.data_validade);
			
			db.transaction(function(x) {
				x.executeSql("INSERT INTO pedido_venda (" + campos.join(', ') + ") VALUES ("+ interrogacoes.join(', ') +")", valores, 
					function() 
					{
						
						callback();				
					}, function(transaction, error) {
						
						
						alert("Error : " + error.message);
					});
			});
		});
		
	});


}


function salvar_nao_venda_cliente(codigo_cliente, loja_cliente,  filial_cliente, empresa_cliente,codigo_motivo, callback){
	var campos = [];
	var interrogacoes = [];
	var valores = [];
	
	campos.push('codigo_cliente');		
	interrogacoes.push('?');
	valores.push(codigo_cliente);
		
	campos.push('codigo_empresa');		
	interrogacoes.push('?');
	valores.push(empresa_cliente);
		
	campos.push('codigo_filial');		
	interrogacoes.push('?');
	valores.push(filial_cliente);
		
	campos.push('codigo_vendedor');		
	interrogacoes.push('?');
	valores.push(info.cod_rep);
		
	campos.push('data');				
	interrogacoes.push('?');
	valores.push(obterDataAtual());
	
	campos.push('codigo_motivo');		
	interrogacoes.push('?');
	valores.push(codigo_motivo);
	
	campos.push('codigo_pedido');		
	interrogacoes.push('?');
	valores.push(null);
	
	campos.push('exportado');		
	interrogacoes.push('?');
	valores.push(null);
	
	
	campos.push('editado');		
	interrogacoes.push('?');
	valores.push(null);
	
	campos.push('erro');		
	interrogacoes.push('?');
	valores.push(null);
	
	obter_cliente(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, function(cliente){
		obter_regiao(cliente.codigo_regiao_venda, function(regiao){
		
		
			campos.push('regiao_venda');		
			interrogacoes.push('?');
			valores.push(cliente.codigo_regiao_venda);
			
			campos.push('regiao_data_validade');		
			interrogacoes.push('?');
			valores.push(regiao.data_validade);
			
			
			console.log("INSERT INTO pedido_venda (" + campos.join(', ') + ") VALUES ("+ valores.join(', ') +")");
			console.log("VALORES: " + valores.join(', '));
			
			db.transaction(function(x) {
				doQuery(x, "INSERT INTO pedido_venda (" + campos.join(', ') + ") VALUES ("+ interrogacoes.join(', ') +")", valores, function(){

					callback();
				}, function(t, error) {
					console.log('Erro');
					alert("Error : " + error.message);
				});
			});
	
		});
	});
	
}

function salvar_registro_freezer_form(codigo_cliente, loja_cliente,  filial_cliente, empresa_cliente, codigo_freezer, temperatura_freezer, limpeza_freezer, manutencao_freezer, data, callback)
{	
	obter_cliente_freezer_registro(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, codigo_freezer, data, function(registro_freezer) 
	{
		if ((registro_freezer.data == '') || (registro_freezer.data == null)) 
		{
			var campos = [];
			var interrogacoes = [];
			var valores = [];
			
			campos.push('codigo_cliente');		
			interrogacoes.push('?');
			valores.push(codigo_cliente);
				
			campos.push('codigo_freezer');		
			interrogacoes.push('?');
			valores.push(codigo_freezer);
				
			campos.push('temperatura_freezer');		
			interrogacoes.push('?');
			valores.push(temperatura_freezer);
				
			campos.push('limpeza');		
			interrogacoes.push('?');
			valores.push(limpeza_freezer);
				
			campos.push('manutencao');				
			interrogacoes.push('?'); 
			valores.push(manutencao_freezer);
			
			campos.push('codigo');		
			interrogacoes.push('?');
			valores.push(info.id_rep);
			
			campos.push('empresa');		
			interrogacoes.push('?');
			valores.push(empresa_cliente);
			
			campos.push('importado');		
			interrogacoes.push('?');
			valores.push('N');
			
			campos.push('data');		
			interrogacoes.push('?');
			valores.push(obterDataAtual());
			
			campos.push('cod_representante');		
			interrogacoes.push('?');
			valores.push(info.cod_rep);
			
			db.transaction(function(x) {
				doQuery(x, "INSERT INTO registro_freezer (" + campos.join(', ') + ") VALUES ("+ interrogacoes.join(', ') +")", valores, function(){
					callback();
				}, function(t, error) {
					console.log('Erro');
					alert("Error : " + error.message);
				});
			});
			
		} else {
	
			var valores = [];
			
			valores.push(temperatura_freezer);
				
			valores.push(limpeza_freezer);
				
			valores.push(manutencao_freezer);

			valores.push(codigo_cliente);
			
			valores.push(codigo_freezer);
			
			valores.push(obterDataAtual());
			
			valores.push(info.id_rep);
			
			db.transaction(function(x) {
				doQuery(x, "UPDATE registro_freezer SET temperatura_freezer = ?, limpeza = ?, manutencao = ? WHERE codigo_cliente = ? AND codigo_freezer = ? AND data = ? AND codigo = ?", valores, function(){
					callback();
				}, function(t, error) {
					console.log('Erro');
					alert("Error : " + error.message);
				});
			});
		}
	});
	
}


