function  CondicaoPagamento(){
	this.codigo;
	this.percentual_desconto;
	
	
	this.obterPercentualDesconto  = function (codigo, callback){
		
		var percentual_desconto = 0;
		
		db.transaction(function(x) {
			x.executeSql('select * from formas_pagamento where codigo = ?   ' , [codigo], function(x, dados) {
				var total = dados.rows.length;
				if(total > 0){
					var dado = dados.rows.item(0);
					percentual_desconto = dado.percentual_desconto;
				}
				callback(percentual_desconto);
				
			});
		});
		
		
		
	}
	
	
}