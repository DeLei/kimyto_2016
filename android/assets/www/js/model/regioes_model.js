function Regioes_model (){
	this.id;
	this.codigo;
	this.nome;
	this.data_validade;
	this.ativa;
	this.clientes = [];
	
	
	this.addCliente = function(cliente){
		this.clientes.push(cliente);
	}	
	
	this.getClientes = function (){		
		return this.clientes;
	}
	
	
	
	this.obterClientesList = function(codigo_regiao,pagina,termo,  callback){
	
		var clientes = [];
		
		var where = 'WHERE codigo_regiao_venda = ? AND clientes.status != \'I\'  ';
		
		if(!!termo){
			termo  = strtoupper(termo);
			where += ' AND ('+
				'(clientes.codigo) like \'%'+termo+'%\' OR ' +
				'upper(clientes.nome) like \'%'+termo+'%\' OR ' +
				'upper(clientes.nome_fantasia) like \'%'+termo+'%\'  '+
			' )';
			
			
		}
		
		
		
		
		var sql = 'SELECT '+
				' clientes.*, '+
				' pedido_venda.*, '+
				' motivo_nao_venda.nome as motivo_nao_venda_descricao '+
			
			' FROM clientes ';
		
			
			sql += ' LEFT JOIN regioes_vendas ON (clientes.codigo_regiao_venda = regioes_vendas.codigo AND  regioes_vendas.ativa != \'C\' ) '	;
			sql += ' LEFT JOIN pedido_venda ON ( '+
					' pedido_venda.codigo_cliente = clientes.codigo '+
					' AND pedido_venda.codigo_empresa = clientes.empresa '+
					' AND pedido_venda.codigo_filial 	= clientes.filial '+
					' AND pedido_venda.regiao_data_validade = regioes_vendas.data_validade '+
					
			
			 ' ) ';
			
			
			
			
			sql += "	LEFT JOIN  motivo_nao_venda ON (pedido_venda.codigo_motivo = motivo_nao_venda.codigo) ";
		
		sql += where;	
			
		sql += ' ORDER BY  data,  nome asc '
		
			
	
				
			
		


		
		var totalClientes = 0;
		db.transaction(function(x) {
			x.executeSql(sql, [codigo_regiao], function(x, dados) {
				var total = dados.rows.length;
				totalClientes = total;	
				
				if(!!pagina){
					var offset = (pagina - 1) * 20;
					
					sql += ' LIMIT 20 OFFSET ' + offset;

					
					x.executeSql(sql, [codigo_regiao], function(x, dados) {
						
						var total = dados.rows.length;
						for (var i = 0; i < total; i++) {
							var dado = dados.rows.item(i);
							clientes.push(dado);
							
							
						}
						
						callback(clientes, totalClientes);
					});
					
				}else{				
					for (var i = 0; i < total; i++) {
						var dado = dados.rows.item(i);
						clientes.push(dado);						
					}	
					callback(clientes, totalClientes);
				}
				
				
			}, function(transaction, error) {
				alert("Error : " + error.message);
			});
		});
		
		
		
		
	}

};




function obter_pedidos_envio_regiao(callback){
	//Obtem total de clientes por regiao

	
	obter_total_clientes_por_regiao( function(clientes_por_regiao){
	//Obter total de pedidos por regiao
		console.log('Clientes por regiao: '+ JSON.stringify(clientes_por_regiao));
		
		
		obter_regioes_para_envio(clientes_por_regiao, function(pedidos_por_regiao){
			
			console.log('pedidos por regiao: '+ pedidos_por_regiao   );
			
			
			callback(pedidos_por_regiao);
		});		
	});			
}



function obter_regioes_para_envio(regioes_envio, callback){
	var regioes = [];
	for(var i = 0; i < regioes_envio.length; i++){
		regioes.push(regioes_envio[i].codigo);
		
	}
	callback(regioes);
	
	
}


function obter_total_clientes_por_regiao(callback){
	var regioes = [];
	
	
	var sql = 	'SELECT '+
				'	COUNT(clientes.codigo) AS total_clientes_regiao , '+
				'	regioes_vendas.codigo , '+
				'	regioes_vendas.ativa , '+
				'	('+
				'		SELECT COUNT(clientes.codigo) '+
				'			FROM regioes_vendas as rv '+
				'			LEFT JOIN clientes 		ON  (clientes.codigo_regiao_venda =  rv.codigo   AND clientes.status != \'I\') '+
				'			LEFT JOIN pedido_venda ON (  '+
				'							 pedido_venda.codigo_cliente = clientes.codigo '+ 
				'							 AND pedido_venda.codigo_empresa = clientes.empresa '+ 
				'							 AND pedido_venda.codigo_filial 	= clientes.filial '+
				'							 AND pedido_venda.regiao_data_validade = regioes_vendas.data_validade '+
				'			)	 '+
				'			WHERE 	 '+		
				'				pedido_venda.regiao_venda = regioes_vendas.codigo '+
				'			AND (regioes_vendas.ativa != \'C\')'+
				'			AND rv.codigo = regioes_vendas.codigo '+
				'	) as total_clientes_atendidos '+
				'FROM regioes_vendas  '+
				'	LEFT JOIN clientes 		ON  (clientes.codigo_regiao_venda =  regioes_vendas.codigo) '+
				'	WHERE clientes.status != \'I\' '+
				'GROUP BY clientes.codigo_regiao_venda, regioes_vendas.codigo';
	console.log(sql);
	db.transaction(function(x) {
		x.executeSql(sql, [], function(x, dados) {
			var total = dados.rows.length;
	
			for (var i = 0; i < total; i++) {
				var dado = dados.rows.item(i);
			console.log('Reg: '+ dado.codigo+' - '+dado.total_clientes_regiao +' === '+  dado.total_clientes_atendidos  );
				
				if(dado.total_clientes_regiao > 0){
					if(dado.total_clientes_regiao <=  dado.total_clientes_atendidos || dado.ativa == 'C' ){
						regioes.push(dado);
					}
				}
			}			
			callback(regioes);
			
		}, function(transaction, error) {
			//alert("Error 2 : " + error.message);
			callback(regioes);
		});
	});
}




function marcar_regiao_completa(codigo_regiao, callback){
	var sql = 	' UPDATE '+
				' regioes_vendas  '+
				' SET ativa = ? ';
	
	if(Array.isArray(codigo_regiao)){
		sql += ' WHERE codigo IN (\'' + codigo_regiao.join('\', \'') + '\')';
	} else {
		sql += ' WHERE codigo = \''+codigo_regiao+'\'';
	}
	
	db.transaction(function(x) {
		x.executeSql(sql, ['C'], function(x, dados) {
			callback();
		}, function(transaction, error) {
			alert("Erro ao sincronizar as regiões: " + error.message);
		});
	});
	
}



function obter_regiao(codigo_regiao, callback){
	var regiao = {};

	var sql = "SELECT * FROM regioes_vendas WHERE codigo ='"+codigo_regiao+"' ";
	
	db.transaction(function(xx) {
		xx.executeSql(sql, [], function(xx, dados) {
			var total = dados.rows.length;
	
			for (i = 0; i < total; i++) {
				var dado = dados.rows.item(i);
					regiao = dado;
			}
			
			callback(regiao);
			
		}, function(transaction, error) {
			callback(regiao);
			//alert("Error 2 : " + error.message);
		});
	});
	
}




