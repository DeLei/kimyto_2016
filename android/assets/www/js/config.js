//CONFIGURAÇÕES GERAIS
var config = {
	versao: 				'2.0.0.6',
	apk:					'Kimyto.apk',
	/*
	ws_url: 				'http://177.54.8.226:81/dwfdv/ws/',
	dw_fdv: 				'http://177.54.8.226:81/dwfdv/ws/',
	dw_pdr: 				'http://177.54.8.226:81/dwpdr/',
	danfe_url:				'http://177.54.8.226:81/danfe/',
	*/
	ws_url: 				'http://mtz.kimyto.com.br:81/dwfdv/ws/',
	dw_fdv: 				'http://mtz.kimyto.com.br:81/dwfdv/ws/',
	dw_pdr: 				'http://mtz.kimyto.com.br:81/dwpdr/',
	danfe_url:				'http://mtz.kimyto.com.br:81/danfe/',	
	ws_url_impostos: 		'',
	fator_calculo_imposto:	1.11205, //nao utilizado mais
	sincronizar: 			'24',
	forca_sincronizar: 		false,
	modulos_obrigatorios: 	{
		
	}
};

var prazo_medio_maximo = 21; 
var exibir_total_derivacao = ['04', '02'];