$(document).ready(function() {
	
	var codigo_dataValidade 	= parse_url(location.href).fragment;
		
	array_codigo_dataValiadade 	= codigo_dataValidade.split('|');
	codigo 			= array_codigo_dataValiadade[0];
	data_validade 	= array_codigo_dataValiadade[1];
	
	$('#filtrar').click(function() {
		sessionStorage['pedidos_regiao_pagina_atual'] = 1;
		
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			console.log(name +' - '+ $(this).val());
			sessionStorage['pedidos_regiao_' + name] = $(this).val();
		});
		
		obter_clientes_regiao(codigo, data_validade,1);
	});

	
	$('#limpar_filtros').click(function() {
		sessionStorage['pedidos_regiao_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['pedidos_regiao_' + name] = '';
		});
		
		obter_clientes_regiao(codigo, data_validade,1);
	});
	
	//CUSTOM E150/2015
	//	Registrar nova venda
	$('.registrar_nova_venda').live('click',function(){
		
		var codigo_cliente 	= $(this).data('codigo_cliente');
		var loja_cliente 	= $(this).data('loja_cliente');
		var filial_cliente 	= $(this).data('filial_cliente');
		var empresa_cliente = $(this).data('empresa_cliente');
		
		novo_pedido(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, 'N');
	});
	//Registrar não venda
	$('.registrar_nao_venda').live('tap, click',function(){
		
		
		
		var codigo_cliente 	= $(this).data('codigo_cliente');
		var loja_cliente 	= $(this).data('loja_cliente');
		var filial_cliente 	= $(this).data('filial_cliente');
		var empresa_cliente = $(this).data('empresa_cliente');
		
		form_motivo_nao_venda(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente);
	});
	//Registrar Freezer
	$('.registrar_freezer').live('tap, click',function(){

		var codigo_cliente 	= $(this).data('codigo_cliente');
		var loja_cliente 	= $(this).data('loja_cliente');
		var filial_cliente 	= $(this).data('filial_cliente');
		var empresa_cliente = $(this).data('empresa_cliente');
		var codigo_freezer = $(this).data('codigo_freezer');
		
		form_registrar_freezer(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, codigo_freezer, obterDataAtual());
		
		return false;
	});
	//Listar Freezer
	$('.listar_freezers').live('tap, click',function(e){

		var codigo_cliente 	= $(this).data('codigo_cliente');
		var loja_cliente 	= $(this).data('loja_cliente');
		var filial_cliente 	= $(this).data('filial_cliente');
		var empresa_cliente = $(this).data('empresa_cliente');
		
		form_listar_freezers(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, obterDataAtual());
		
		return false;
	});
	
	var html = '';
	var data_atual = protheus_data2data_normal(obterDataAtual());

	$("#data_atual").html(data_atual);

	obter_clientes_regiao(codigo, data_validade,1);
	
});

/**
 * Obter regiões com clientes vinculados, onde a região nõ encontre-se expirada.
 * 
 * 
 */


function obter_clientes_regiao(codigo, data_validade, pagina, callback){
	
	var mensagem_loading = '<img src="img/ajax-loader.gif" width="15" weight="15"> Aguarde, o sistema está carregando os dados...';
	var html = '';
	var data_atual = protheus_data2data_normal(obterDataAtual());

	$("#data_atual").html(data_atual);


	$('.rodape_grid').hide();
	obter_cliente_regiao(codigo, data_validade, pagina, function(regioes) {
		var pagina = (sessionStorage['pedidos_regiao_pagina_atual']) ;
		// calcular offset
		var offset = (sessionStorage['pedidos_regiao_pagina_atual'] - 1) * 20;
		// gerar filtros
		

		var total = regioes.length; 
		if (total > 0) {
			for (var i = 0; i < total; i++) {
				var regiao = regioes[i];
				html += obter_regiao_html(regiao);

				// calcular totais			
				$('#total').text(number_format(regiao.totalClientes, 0, ',', '.'));

				// paginação
				$('#paginacao').html('');
				
				var total = ceil(regiao.totalClientes / 20);

				if (total > 1) {
					if (sessionStorage['pedidos_regiao_pagina_atual'] > 6) {
						$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(\''+regiao.codigo+'\','+regiao.data_validade+' , \'1\' ); return false;">Primeira Página</a>&nbsp;&nbsp;');
					}

					if (sessionStorage['pedidos_regiao_pagina_atual'] > 1) {
						$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(\''+regiao.codigo+'\','+regiao.data_validade+ ',\''+  (intval(sessionStorage['pedidos_regiao_pagina_atual']) - 1)  + '\'); return false;">&lt;</a> ');
					}

					for (i = intval(sessionStorage['pedidos_regiao_pagina_atual']) - 6; i <= intval(sessionStorage['pedidos_regiao_pagina_atual']) + 5; i++) {
						if (i <= 0 || i > total) {
							continue;
						}

						if (i == sessionStorage['pedidos_regiao_pagina_atual']) {
							$('#paginacao').append('<strong>' + i + '</strong> ');
						} else {
							$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(\''+regiao.codigo+'\','+regiao.data_validade+' ,'+ i + '); return false;">' + i + '</a> ');
						}
					}

					if (sessionStorage['pedidos_regiao_pagina_atual'] < total) {
						$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(\''+regiao.codigo+'\','+regiao.data_validade+' ,\'' + (intval(sessionStorage['pedidos_regiao_pagina_atual']) + 1) + '\'); return false;">&gt;</a> ');
					}
					
					if (sessionStorage['pedidos_regiao_pagina_atual'] <= total - 6) {
						$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_clientes_regiao(\''+regiao.codigo+'\','+regiao.data_validade+' ,\'' + total + '\'); return false;">Última Página</a> ');
					}
				}
			
				
			}
		} else {
			html = '<div>Nenhuma região disponível.</div>';

		}		
		$("#dados_html").html(html);
		$('.rodape_grid').show();
		
		if(!!callback){		
			
			callback();
			
		}

	});
	
}

function obter_cliente_regiao(codigo, data_validade, pagina,  callback) {

	pagina = pagina ? pagina : 1;
	sessionStorage['pedidos_regiao_pagina_atual'] = pagina;
	var termo = (sessionStorage['pedidos_regiao_termo']);
	
	
	
	obter_regioes_vendas(codigo, data_validade,pagina, termo, function(regioes) {
		//console.log('Regioes: '+ JSON.stringify(regioes));
		callback(regioes);
	});

}

function obter_regioes_vendas(codigo, data_validade, pagina, termo, callback) {
	var data_atual = obterDataAtual();
	var regioes = [];

	var where = " WHERE   ativa = 'A' AND codigo = '"+codigo+"' AND data_validade = '"+data_validade+"' ";
	
	var order_by = ' ORDER BY data_validade ASC';
	/**
	 * Obter Regiões
	 * */

	var sql = 'SELECT * FROM regioes_vendas  ' + where + order_by;
	
	db.transaction(function(x) {
		x.executeSql(sql, [   ], function(x, dados) {
			var total = dados.rows.length;
			var item = 0;
			if (total > 0) {
				interacaoRegiao(pagina, termo, dados, item, total, regioes, function(regioes) {
					callback(regioes);
				});
			} else {
				callback(regioes);
			}

		}, function(transaction, error) {
			alert("Error : " + error.message);
		});
	});

} 

function interacaoRegiao(pagina, termo, dados, item, total, regioes, callback) {
	
	if (item == total) {
		callback(regioes);
	} else {
		var dado = dados.rows.item(item);
		var regiao = new Regioes_model();
		regiao.id = dado.id;
		regiao.codigo = dado.codigo;
		regiao.nome = dado.nome;
		regiao.ativa = dado.ativa;
		regiao.data_validade = dado.data_validade;

		regiao.obterClientesList(regiao.codigo,pagina, termo, function(clientes, totalClientes) {
			
			regiao.clientes = clientes;
			regiao.totalClientes = totalClientes;
			regioes.push(regiao);
			interacaoRegiao(pagina, termo, dados, item + 1, total, regioes, callback);

		});
	}
}



function obter_regiao_html(regiao) {
	
	var html = '';	
	html += '<div id="' + regiao.id + '"  class="regiao_venda_box" >' + '<p>Região: ' + regiao.nome+ ' - Atender até: '+ protheus_data2data_normal(regiao.data_validade) + '</p>' +
			'<div> ' ;
		html +='<div style="display:none;" class="nenhum_cliente_encontrado">Nenhum cliente encontrado.</div>';
			 html+= obter_cliente_html(regiao);
				'</div>' +
			'</div>';
	return html;
}


function obter_cliente_html(regiao){
	var html = '';
	for (var i = 0; i < regiao.clientes.length; i++) {

		html+= '<div class="cliente_regiao" data-nome="'+regiao.clientes[i].nome_fantasia+'" data-razao_social="'+regiao.clientes[i].nome+'" data-codigo="'+regiao.clientes[i].codigo+'"    >';
		html+= '	<div class="" style="width: 100%; display: table;">';
		html+= '		<div>'+regiao.clientes[i].nome+'('+regiao.clientes[i].nome_fantasia+') - '+regiao.clientes[i].codigo +  '</div>';
		html+= '		<div>';
		/* Se houver registro de venda ou não venda*/
		
		
		if(!is_null(regiao.clientes[i].codigo_motivo)){
			html += '<div class="text_danger">'+
				'Não Vendido: '+ regiao.clientes[i].motivo_nao_venda_descricao + 
			'</div>';
		
			
		}else if(!is_null(regiao.clientes[i].codigo_pedido)){
			html += '<div class="text_success">'+
				'Vendido: Pedido ' + regiao.clientes[i].codigo_pedido +
			'</div>';
	
			
			
		}else{
			
			
			if(regiao.data_validade >= obterDataAtual()){
								
				html+= '			<div style="margin-right: 20px; margin-top: 6px; margin-bottom: 6px; float: left;">';
				html+=		'     		<a class="btn btn-success btn-primary registrar_nova_venda"  data-codigo_cliente="'+regiao.clientes[i].codigo+'"  data-loja_cliente="'+regiao.clientes[i].loja+'"  data-filial_cliente="'+regiao.clientes[i].filial+'" data-empresa_cliente="'+regiao.clientes[i].empresa+'" href="#" >Registrar Venda</a>';
				html+=		'    	</div>';
					
			}
			
			html+='				<div style="margin-right: 20px; margin-top: 6px; margin-bottom: 6px; float: left;">';
			html+='					<a class="btn btn-warning btn-primary  registrar_nao_venda"  data-codigo_cliente="'+regiao.clientes[i].codigo+'"  data-loja_cliente="'+regiao.clientes[i].loja+'"  data-filial_cliente="'+regiao.clientes[i].filial+'" data-empresa_cliente="'+regiao.clientes[i].empresa+'" href="#" >Registrar Não Venda</a>';
			html+='				</div>';
			
		}	
					
				
		html+='		</div>';			
		html+='	</div>';
		html+='	<div class="col col30">';
		html+='		<div>Limite de Crédito: '+formatarMoeda(regiao.clientes[i].limite_credito)+'</div>';
		html+='		<div>Total de Títulos em aberto: '+formatarMoeda(regiao.clientes[i].total_titulos_aberto)+'</div>';		
		html+='	</div>';
		html+='	<div class="col col30">';
		html+='		<div>Limite de Crédito Disponível: '+formatarMoeda(regiao.clientes[i].limite_credito - regiao.clientes[i].total_titulos_aberto)+'</div>';
		html+='	</div>';
		html+='	<div class="clear"></div>';
		html+= '			<div style="margin-right: 20px; margin-top: 6px; margin-bottom: 6px; float: left;">';
		html+=		'     		<a class="btn btn-info btn-primary listar_freezers"  data-codigo_cliente="'+regiao.clientes[i].codigo+'"  data-loja_cliente="'+regiao.clientes[i].loja+'"  data-filial_cliente="'+regiao.clientes[i].filial+'" data-empresa_cliente="'+regiao.clientes[i].empresa+'" href="#" >Freezers</a>';
		html+=		'    		<div id="freezer_cliente_' + regiao.clientes[i].codigo + '"></div>';
		html+=		'    	</div>';	
		html+='	<div class="clear"></div>';
		html+='</div>';
		
		
	}
	
	return html;
}


function novo_pedido(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa, tipo)
{
	obter_cliente(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa, function(cliente){
				//Cabeçalho pedido
				var dados_pedido = [];
							
				dados_pedido['cliente_prospect'] 				= 'C';
				dados_pedido['empresa'] 						= cliente.empresa;
				dados_pedido['filial'] 							= cliente.filial;
				dados_pedido['codigo_cliente'] 					= cliente.codigo;
				dados_pedido['loja_cliente']					= cliente.loja;
				dados_pedido['descricao_cliente']				= cliente.codigo + '/' + cliente.loja + ' - ' + cliente.nome + ' - ' + cliente.cpf;
				dados_pedido['desconto_cliente']				= cliente.desconto;
				dados_pedido['condicao_pagamento'] 				= cliente.condicao_pagamento;
				dados_pedido['tipo_pedido']						= tipo;
				dados_pedido['dados_cliente']					= cliente;
				dados_pedido['tabela_precos']					= cliente.tabela_preco;
				dados_pedido['codigo_tabela_preco_promo']		= cliente.codigo_tabela_preco_promo;
				dados_pedido['codigo_icms_st_cliente']			= cliente.codigo_icms_st_cliente;
				
				
				if(tipo == 'N')
				{	
					sessionStorage['sessao_tipo'] = 'sessao_pedido';
					sessionStorage['sessao_pedido'] = serialize(dados_pedido);
					window.location = "pedidos_adicionar.html";
				}
				else
				{
					sessionStorage['sessao_tipo'] = 'sessao_orcamento';
					sessionStorage['sessao_orcamento'] = serialize(dados_pedido);
					window.location = "pedidos_adicionar.html#O";
				}

			
	});
}

function form_motivo_nao_venda(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente){
	$("#motivo_nao_venda_motivo_error").hide();
	obter_cliente(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, function(cliente){
		$("#motivo_nao_venda_cliente").html(cliente.nome+ '( '+cliente.nome_fantasia+')');
		$("#motivo_nao_venda_data").html(protheus_data2data_normal(obterDataAtual()));
		$("#motivo_nao_venda_motivo").html('<option value="">Selecione</option>');
		
		obter_motivos_nao_venda(function(motivos_nao_venda){
			for(var i = 0; i < motivos_nao_venda.length; i++){				
				$("#motivo_nao_venda_motivo").append('<option value="'+motivos_nao_venda[i].codigo+'">'+motivos_nao_venda[i].nome+'</option>');
			}
			
			exibirMensagem2('formulario_nao_venda','', "salvar_motivo_nao_venda('"+codigo_cliente+"', '"+loja_cliente+"', '"+filial_cliente+"', '"+empresa_cliente+"')", "Registro de Não Venda");
			
		});

	});
}

function form_registrar_freezer(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, codigo_freezer, data)
{
	obter_cliente(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, function(cliente)
	{
		obter_cliente_freezer_registro(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, codigo_freezer, data, function(cliente_freezer)
		{
			
			$("#freezer_nome").html(cliente_freezer.modelo_freezer);
			$("#temperatura_freezer").val(cliente_freezer.temperatura_freezer);
			$("#limpeza_freezer").val(cliente_freezer.limpeza);
			$("#manutencao_freezer").val(cliente_freezer.manutencao);
			$("#registro_freezer_data").html(protheus_data2data_normal(obterDataAtual()));

			exibirMensagem2('formulario_registrar_freezer','', "salvar_registro_freezer('"+codigo_cliente+"', '"+loja_cliente+"', '"+filial_cliente+"', '"+empresa_cliente+"', '" + codigo_freezer +"')", "Leitura de Freezer");
		});
	});
}

function form_listar_freezers(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, data)
{
	obter_cliente(codigo_cliente, loja_cliente, 1, 1, function(cliente)
	{
		obter_cliente_freezers(codigo_cliente, loja_cliente, 1, 1, data, function(cliente_freezers)
		{
			var html = '<br />';
			html += '<table style="font-size: 16px">';
			if (cliente_freezers.length) {
				for(var i in cliente_freezers) {
					html += '<tr>';
						html += '<td style="width:490px">' + cliente_freezers[i].codigo_freezer + ' - ' + cliente_freezers[i].modelo_freezer + '</td>';
						html += '<td style="width:120px">Limpeza [' + (cliente_freezers[i].limpeza == null || cliente_freezers[i].limpeza.trim() == '' ? 'OK' : 'X') + ']</td>';
						html += '<td style="width:140px">Manutenção [' + (cliente_freezers[i].manutencao == null || cliente_freezers[i].manutencao.trim() == '' ? 'OK' : 'X') + ']</td>';
						html += '<td style="width:90px">' + '<a style="margin-left: 4px" class="btn btn-info btn-primary registrar_freezer"  data-codigo_cliente="'+codigo_cliente+'" data-codigo_freezer="'+cliente_freezers[i].codigo_freezer+'" data-loja_cliente="'+loja_cliente+'" data-filial_cliente="1" data-empresa_cliente="1" href="#" >Leitura</a>' + '</td>';
					html += '</tr>';
				};
			} else {
				html += '<tr><td><strong>Não há freezers neste cliente</strong></td></tr>';
			}
			html += '</table>';
			$('#freezer_cliente_' + codigo_cliente).html(html);
		});
	});
}

function obter_motivos_nao_venda(callback){
	var sql = 'SELECT * FROM motivo_nao_venda '+
		' ORDER BY nome';
	var motivos_nao_venda = [];	
	
	db.transaction(function(x) {
		x.executeSql(sql, [], function(x, dados) {
			var total = dados.rows.length;
			var item = 0;
			if (total > 0) {
				for(var i = 0; i < total; i++ ){
					var dado = dados.rows.item(i);
					motivos_nao_venda.push(dado);
				}
				callback(motivos_nao_venda);
			} else {
				mensagem('Nenhum motivo de não venda encontrado. Por favor sincronize o módulo "Motivo de Não Venda".Se o problema persistir por favor entre em contato com seu Gestor Comercial.');
			}
		}, function(transaction, error) {
			alert("Error : " + error.message);
		});
	});
}

function salvar_registro_freezer(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, codigo_freezer)
{	
	
	salvar_registro_freezer_form(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, codigo_freezer, $("#temperatura_freezer").val(), $("#limpeza_freezer").val(), $("#manutencao_freezer").val(), obterDataAtual(), function(){
		alert('Registro do Freezer Salvo com Sucesso');
		form_listar_freezers(codigo_cliente, loja_cliente, filial_cliente, empresa_cliente, obterDataAtual());
		$('#formulario_registrar_freezer').removeClass("disabled");
		$('#formulario_registrar_freezer').dialog('close');
	});
	
}

function salvar_motivo_nao_venda(codigo_cliente, loja_cliente,  filial_cliente, empresa_cliente){
	var codigo_motivo = $("#motivo_nao_venda_motivo").val();
	console.log('Cód: '+codigo+ 'Dat: '+data_validade );
	//alert('clicado'+ $(".ui-dialog-buttonpane").html());
	console.log($('#formulario_nao_venda').attr('disabled'));
	
	if(codigo_motivo.length > 0){
		salvar_nao_venda_cliente(codigo_cliente, loja_cliente,  filial_cliente, empresa_cliente,codigo_motivo, function(){
			console.log('Motivo de  nao venda salva com sucesso');
			//location.href="pedidos_adicionar_cliente_regiao.html";
			obter_clientes_regiao(codigo, data_validade, 1,function(){		
				$('#formulario_nao_venda').removeClass('disabled');
				$("#formulario_nao_venda").dialog('close');
			});
		});
	}else{
		
		$('#formulario_nao_venda').removeClass('disabled');
		$("#motivo_nao_venda_motivo_error").show();
		$("#motivo_nao_venda_motivo_error").html('<p class="error">Nenhum motivo Selecionado.</p>');
	}
	
}