$(document).ready(function(){
	
	$('.somenteNumero').live('keyup cut copy paste focusout', function (event) {
		var conteudo = $(this).val().toUpperCase();
		var validConteudo = conteudo.replace(/[^0-9]/g, '');
		$(this).val(validConteudo);
	});	
	
	$('.promocao').live('click',function(){
		
		var codigo_produto 				= $(this).data('codigo_produto');
		var derivacao_codigo_derivacao 	= $(this).data('derivacao_codigo_derivacao');
		var indice 						= $(this).data('indice'); 
		var tabela_precos				= obter_valor_sessao('tabela_precos');
		
		
		
		obter_dados_produto(codigo_produto, derivacao_codigo_derivacao, tabela_precos, function(produto){		
			
			console.log("i: "+indice);
			console.log(JSON.stringify(produto));
			var html = "<div>"+
							"<p><strong>Produto:</strong> "+produto.produto_codigo+" - "+produto.produto_descricao+"</p>"+
							"<p><strong>Preço: </strong>"  +'R$ ' + number_format(produto.ptp_preco, 2, ',', '.') +"</p>"+
							"<p><strong>Preço Promocional: </strong>"  +'R$ ' + number_format(produto.produto_promocional_preco, 2, ',', '.') +"</p>"+
							
							"Aplicar preço promocional?"+
						"</div>";
			
			
			exibirMensagemSimNao(null, html, "aplicar_promocao('"+codigo_produto+"', '"+derivacao_codigo_derivacao+"', '"+indice+"')","remover_promocao('"+codigo_produto+"', '"+derivacao_codigo_derivacao+"', '"+indice+"')", 'PROMOÇÃO');
			
			
		});
		
	});
	
	
	
	
	
	/**
	 * Realiza o calculo de total do item de derivação
	 */
	$('.quantidade_derivacao').live('keyup', function(){
		
		
		
		var codigo_produto		= $(this).data('produto');
		var codigo_derivacao	= $(this).data('derivacao');
		
		
		var aplicada_promocao 	= $(this).data('aplicada_promocao');	
		
		var quantidade		= converter_decimal($(this).val());
		var preco_unitario	= converter_decimal($("input[name='preco_unitario[" + codigo_produto + "][" + codigo_derivacao + "]']").val());
		var preco_unitario_promocional =  converter_decimal($("input[name='preco_unitario_promocional[" + codigo_produto + "][" + codigo_derivacao + "]']").val());
		
		
		var preco_venda = converter_decimal($("input[name='preco_venda[" + codigo_produto + "][" + codigo_derivacao + "]']").val());
		
		
		valor_total = quantidade * preco_venda;
			
		
		
		
		
		
		$("input[name='preco_total[" + codigo_produto + "][" + codigo_derivacao + "]']").val(number_format(valor_total, 2, ',', '.'));
		$("#label_valor_total_item_" + codigo_produto + "_" + codigo_derivacao).html('R$ ' + number_format(valor_total, 2, ',', '.'));
		
		obterValorTotalDerivacao();
	});
	
	/**
	 * Monta a tela de listagem de produtos
	 */
	$('#visualizar_produtos').click(function(){
		var produtos = obter_produtos_sessao();
		var valor_total = 0;
		var valor_total_com_impostos = 0;
		
		var total_por_derivacao = {};
		
		var html = '<table width="100%" class="normal hnordt">';
				html += '<thead>';
					html += '<tr>';
						html += '<th>Produto</th>';
						html += '<th>Derivação</th>';
						html += '<th>Quant.</th>';
						html += '<th>Preço (R$)</th>';
						html += '<th>Preço c/ Imp. (R$)</th>';
						html += '<th>Total (R$)</th>';
						html += '<th>Total c/ Imp. (R$)</th>';
						html += '<th>Opções</th>'
					html += '</tr>';
				html += '</thead>';
				html += '<tbody>';
				
					var produtosOrdenadosClasse = [];
					
					$.each(produtos, function(key, produto){
						$.each(produto, function(k, derivacao){
							if (typeof derivacao !== 'undefined') {
								if (typeof produtosOrdenadosClasse[derivacao.codigo_classe] === 'undefined') {
									produtosOrdenadosClasse[derivacao.codigo_classe] = new Array();
								}
								produtosOrdenadosClasse[derivacao.codigo_classe].push(derivacao);
							}
						});
					});
					
					
					var condicaoPagamentoModel = new CondicaoPagamento();
					
					condicaoPagamentoModel.obterPercentualDesconto( obter_valor_sessao('condicao_pagamento'), function(percentual_desconto){
					
					
						$.each(produtosOrdenadosClasse, function(key, produto){
							if (typeof produto !== 'undefined') {
								$.each(produto, function(k, derivacao){								
									if( isset(total_por_derivacao[derivacao.codigo_derivacao])){
										total_por_derivacao[derivacao.codigo_derivacao]  += derivacao.quantidade;
									}else{
										total_por_derivacao[derivacao.codigo_derivacao] = derivacao.quantidade;
									}
									
									var quantidade = derivacao.quantidade;
									var preco_venda = number_format(derivacao.preco_venda, 2);
									var preco_venda_com_impostos = calcular_impostos_produtos(derivacao.preco_venda, percentual_desconto); 
									
									html += '<tr>';
										html += '<td>' + derivacao.codigo_produto + ' - ' + derivacao.descricao_produto + '</td>';
										html += '<td>' + derivacao.codigo_derivacao + ' - ' + derivacao.descricao_derivacao + ' <br />(' + derivacao.descricao_classe + ')</td>';
										html += '<td align="center">' + quantidade + '</td>';
										html += '<td align="right">' + number_format(preco_venda, 2, ',', '.') + '</td>';
										html += '<td align="right">' + number_format(preco_venda_com_impostos, 2, ',', '.') + '</td>';
										html += '<td align="right">' + number_format(quantidade * preco_venda, 2, ',', '.') + '</td>';
										html += '<td align="right">' + number_format(quantidade * preco_venda_com_impostos, 2, ',', '.') + '</td>';
										html += '<td align="center"><a class="excluir_item botao_c_grid" data-descricao_produto="' + derivacao.descricao_produto + '" href="' + derivacao.codigo_produto + '|' + derivacao.codigo_derivacao + '">Excluir</a></td>';
									html += '</tr>';
									
									valor_total += quantidade * preco_venda;
									valor_total_com_impostos += quantidade * preco_venda_com_impostos;
								});
							}
						});
						
						////////////
						var quantidade_potes = '';
						
						
						var qtd_2_litros = 0;
						var qtd_1_litro = 0;
						$.each(total_por_derivacao, function(codigo_derivacao, total){
							if(in_array(codigo_derivacao, exibir_total_derivacao)){
								
								if(codigo_derivacao == '04'){
									qtd_1_litro = total;
									
								}else if(codigo_derivacao == '02'){
									qtd_2_litros = total; 
								}
							}
							
						});
						quantidade_potes += '<div class="qtd_embalagem_total"> <strong>Quantidade potes 1 litro:</strong> '+qtd_1_litro+'.</div>';
						quantidade_potes += '<div class="qtd_embalagem_total"> <strong>Quantidade potes 2 litros:</strong> '+qtd_2_litros+'.</div>';
					
						
						//////
						
						html += '<tr class="">';
						html += '<td colspan="8">'+quantidade_potes+'</td>';
						
						html += '</tr>';
						
						
						html += '<tr class="novo_grid_rodape">';
							html += '<td colspan="5">Total do Pedido</td>';
							html += '<td align="right">' + number_format(valor_total, 2, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(valor_total_com_impostos, 2, ',', '.') + '</td>';
							html += '<td></td>';
						html += '</tr>';
					html += '</tbody>';
				html += '</html><br/>';
				
			
			//Oculta as telas padrões
			$('.barra').hide();
			$('#conteudo').hide();
			
			//Exibe a listagem de produtos
			$('.barra_listagem_produtos').html('<div id="voltar"><a href="#" id="voltar_inclusao_pedido"><img src="img/btn_voltar.gif" width="86" height="46"></a></div>');
			$('.barra_listagem_produtos').show();
			$('#lista_produtos').html(html);
			
			$('#lista_produtos').show();
			
					});
	});
	
	/**
	 * Remove a listagem de produtos da tela e exibe a inclusão de itens
	 */
	$('#voltar_inclusao_pedido').live('click', function(){
		$('.barra_listagem_produtos').hide();
		$('#lista_produtos').html('');
		
		$('.barra').show();
		$('#conteudo').show();
	});
	
	$('.changeValue').click(function(){
		$(this).select();
	});

	// Desativar todos os campos quando clicar em id_informacao_cliente
	$('#id_adicionar_produtos').click(function(){
		
		$('#id_outras_informacoes').addClass('tab_desativada');
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',1);
		
		ativar_tabs();
	});
	
	/**
	* Limpa os dados do produto setados nos campos input
	*/
	$('#trocar_produto').live('click', function(){
		limpar_dados_produtos();
	});
	
	/**
	* 
	* ID:			adicionar_produto
	*
	* Descrição:	Utilizado para adicionar um produto e seus valores na sessao
	*
	*/
	$('#adicionar_produto').live('click', function(e){
		e.preventDefault();
		
		var produtos = validarProduto();
		
		if(!produtos) {
			alert('erro');
		} else {
			var produtos = serialize(produtos);
			
			salvar_produto_sessao(produtos);
			
			limpar_dados_produtos();
			
			$('[name=produto]').focus();
			
			obterTotaisPedido();
		}
	});
	
	/**
	* 
	* ID:			avancar_passo_2
	*
	* Descrição:	Utilizado para salvar os dados editados e passar para o proximo passo
	*
	*/
	$('#avancar_passo_2').live('click', function(e){
	
		e.preventDefault();
	
		$('.erro_itens').remove();
		
		var sessao_produtos = obter_produtos_sessao();
		if(sessao_produtos) {
			var total_produtos 		= Object.keys(sessao_produtos).length;
			
		} else {
			var total_produtos 		= 0;
			
		}
		
		if(total_produtos > 0) {
			
			// Ativando Botao "Adicionar Produtos"
			$('#id_outras_informacoes').removeClass('tab_desativada');
			
			ativar_tabs();
			
			//Chamando as funções do outras_informacoes
			outras_informacoes();
			
			// Acionando (Click) botao "Adicionar Produtos"
			$("#id_outras_informacoes").click();

		} else {
			
			var descricao_tipo_pedido = obter_descricao_pedido('lower');
			mensagem('Adicione um produto no ' + descricao_tipo_pedido + '.');
			
		}
	});
	
	
	
	/**
	* 
	* CLASSE:			excluir_item
	*
	* Descrição:	Utilizado para excluir um item na sessao
	*
	*/
	$('.excluir_item').live('click', function(e){
		e.preventDefault();
		
		var excluir = $(this).attr('href');
			excluir = explode('|', excluir);
			
		var codigo_produto = remover_zero_esquerda(excluir[0]);
		var codigo_derivacao = remover_zero_esquerda(excluir[1]);
		
		
		var descricao_produto = $(this).data('descricao_produto');
		
		confirmar('Deseja excluir o item <b>' + codigo_produto + ' - ' + descricao_produto + '</b>?', 
			function () {
				$(this).dialog('close');
				
				var sessao_produtos = obter_produtos_sessao();
				
				delete sessao_produtos[codigo_produto][codigo_derivacao];
				
				salvar_produtos_excluidos(serialize(sessao_produtos));
				
				obterTotaisPedido();
				
				$("#confirmar_dialog").remove();
				
				$('#visualizar_produtos').trigger('click');
			}
		);
	});
	
});

/*
function recalcular_precos_produtos(callback){
	var produtos_sessao = obter_produtos_sessao();
	
	
	
	if(produtos_sessao) {
		$(produtos_sessao).each(function ( codigo_derivacao){
			console.log(codigo_derivacao);
			//.log(JSON.stringify(produtos_sessao[codigo_produto][codigo_derivacao]));
		});
		
		
		if(produtos_sessao[codigo_produto]) {
			if(produtos_sessao[codigo_produto][derivacao]) {
				
				console.log(JSON.stringify(produtos_sessao[codigo_produto][derivacao]));
				
				quantidade_sessao = produtos_sessao[codigo_produto][derivacao]['quantidade'];
				aplicada_promocao = produtos_sessao[codigo_produto][derivacao]['aplicada_promocao'];
				preco_venda 	  = produtos_sessao[codigo_produto][derivacao]['preco_venda'];
			}
		}
	}
	
	callback();
	
}
*/


function adicionar_produtos() {
	
	// Exibir valores do cabeçalho
	exibir_cabecalho();
	
	// Obter Produtos (Autocomplete)
	
//	recalcular_precos_produtos(function(){
	
		obter_produtos();
		obterTotaisPedido();
//	});
	
	
	
	alterarCabecalhoTabelaConteudo();
}

/**
* Metódo:		limpar_dados_produtos
* 
* Descrição:	Função Utilizada para apagar todos os campos ligados ao produto
* 
* Data:			19/10/2013
* Modificação:	23/10/2013
* 
* @access		public

* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function limpar_dados_produtos()
{
	$('input[name="classe_produto"]').val("");
	$('input[name="codigo_classe"]').val("");
	$('input[name="descricao_classe"]').val("");
	
	$('#trocar_produto').hide();
	$('input[name="classe_produto"]').removeAttr('disabled');
	$('#erro_item').hide();
	
	$('.derivacoes_produto').html('');
	$('#lista_derivacoes').hide();
}

/**
* Metódo:		erro_itens
* 
* Descrição:	Função Utilizada para exibir os erros 
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @param		string 			codigo_produto 	- codigo do produto usado para identificar qual linha da coluna sera inserido o erro
* @param		string 			mensagem_erro	- mensagem do erro
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function erro_itens(mensagem_erro, codigo_produto)
{
	var icone = '<img src="img/warning.png" style="vertical-align: middle" /> ';

	if(codigo_produto)
	{
		$('<tr class="erro_itens" style="background-color: #F0798E"><td colspan="10">' + icone + mensagem_erro + '</td></tr>').insertAfter($('.produto_' + codigo_produto));
	
		$('.produto_' + codigo_produto).css('background-color', '#F0798E');
	}
	else
	{
	
		$('#erro_item').html(icone + mensagem_erro);
		$('#erro_item').show('fast');
	}
}


/**
* Metódo:		obter_produtos_sessao
* 
* Descrição:	Função Utilizada para obter os itens inseridos na sessão
* 
* Data:			19/10/2013
* Modificação:	19/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos_sessao() {
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}

	if(sessao_pedido['produtos']) {	
		return sessao_pedido['produtos'];
	} else {
		return false;
	}
}


/**
* Metódo:		obter_produtos_removidos_sessao
* 
* Descrição:	Função Utilizada para obter os itens removidos na sessão
* 
* Data:			22/10/2013
* Modificação:	22/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos_removidos_sessao() {
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_removidos']) {
		return sessao_pedido['produtos_removidos'];
	} else {
		return false;
	}
}

/**
* Metódo:		obter_produtos
* 
* Descrição:	Função Utilizada para buscar os produtos na tabela de produtos onde a tabela de preços for igual a selecionada
* 
* Data:			17/10/2013
* Modificação:	17/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_produtos() {
	$('#erro_item').hide();
	$('#carregando_produtos').show();
	
	sessionStorage['sessao_tipo'] = 'sessao_pedido';
	
	var sessao_pedido  = obter_dados_pedido(); 
	
	
	
	
	var where = " where produto_empresa = '"+sessao_pedido.empresa+"' ";
	
	
	if (obter_valor_sessao('filial') && obter_valor_sessao('filial') != 'undefined') {
		var wheres = " ";
	}
	
	db.transaction(function(x) {
		doQuery(x, 'SELECT DISTINCT derivacao_classe_codigo_classe, derivacao_classe_descricao_classe FROM produtos WHERE tb_codigo = ?' + wheres, [obter_valor_sessao('tabela_precos')], function(x, dados) {
			if (dados.rows.length) {
				var classe = [];
				
				for (i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					classe.push({ label: dado.derivacao_classe_codigo_classe + ' - ' + dado.derivacao_classe_descricao_classe, codigo: dado.derivacao_classe_codigo_classe, descricao: dado.derivacao_classe_descricao_classe});
				}
				
				$('#carregando_produtos').hide();
				
				buscar_produtos(classe);

			} else {
				$('#erro_item').text('Nenhuma classe de produtos encontrada.');
				$('#carregando_produtos').hide();
				$('#erro_item').show();
			}
		});
	});
	
}

/**
* Metódo:		buscar_produtos
* 
* Descrição:	Função Utilizada para pesquisar os produtos pelo autocomplete e adicionar os valores nos campso quando o produto for selecionado
* 
* Data:			17/10/2013
* Modificação:	17/10/2013
* 
* @access		public
* @param		array 			var produtos		- Todos os produtos
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_produtos(classes) {
	$('input[name=classe_produto]').autocomplete({
		minLength: 3,
		source: classes,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {

			$('input[name=classe_produto]').val(ui.item.label);
			$('input[name=codigo_classe]').val(ui.item.codigo);
			$('input[name=descricao_classe]').val(ui.item.descricao);
			
			// Bloquear campo quando selecionar cliente
			$('input[name=classe_produto]').attr('disabled', 'disabled');
			$('#trocar_produto').show();
			
			
			
			obterProdutosClasse(ui.item.codigo, percentual_desconto);
			
			
			
			
			return false;
		}
	});
	/* Montagem das opções de classes de produtos para selecionar */
	var html = '<div>';
	for(var i in classes) {
		html += '<a href="#" id="classe_' + classes[i].codigo + '_produto" data-codigo_classe="'+classes[i].codigo+'" class="btn btn_selecionar_classe_produto" onclick="selecionar_produto_classe(\'' +  classes[i].codigo + '\', \'' + classes[i].descricao + '\'); return false;" >' + classes[i].descricao + '</a>';
	}
	html += '</div>';
	$('#classe_produto_selecionar_opcao').html(html);
}

/**
* Metódo:		selecionar_produto_classe
* 
* Descrição:	Função Utilizada para selecionar uma classe de produto
* 
* Data:			29/08/2014
* Modificação:	29/08/2014
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function selecionar_produto_classe(codigo_classe, descricao_classe) {
	$("input[name='codigo_classe_produtos']").val(codigo_classe);
	$("input[name='descricao_classe_produtos']").val(descricao_classe);
	
	
	obterProdutosClasse(codigo_classe);
	
	
	
}


/**
* Metódo:		marcar_classe_atendida
* 
* Descrição:	Função Utilizada para marcar as classes com produtos adicionados
* 
* Data:			29/08/2014
* Modificação:	29/08/2014
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function marcar_classe_atendida() {
	var produtos = obter_produtos_sessao();
	var total_por_derivacao = {};
	
	if(produtos) {
		
		
		$.each(produtos, function(key, produto){
			$.each(produto, function(k, derivacao){
				if( isset(total_por_derivacao[derivacao.codigo_derivacao])){					
					total_por_derivacao[derivacao.codigo_derivacao]  += derivacao.quantidade;
				}else{
					total_por_derivacao[derivacao.codigo_derivacao] = derivacao.quantidade;
				}
				
			})
		});
	}
	
	$(".btn_selecionar_classe_produto").removeClass('classe_atendida');
	$.each(total_por_derivacao, function(codigo_derivacao, total){
		if(total > 0){
			$(".btn_selecionar_classe_produto").addClass('classe_atendida');
		//	$(".btn_selecionar_classe_produto[data-codigo_classe='"+codigo_derivacao+"']").addClass('classe_atendida');	
		}else{
			
			$(".btn_selecionar_classe_produto[data-codigo_classe='"+codigo_derivacao+"']").removeClass('classe_atendida');
		}
	});
	
	
	
	

}

/**
* Metódo:		exibir_cabecalho
* 
* Descrição:	Função Utilizada para exibir os valores do cabeçalho
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_cabecalho() {
	
	// Obter descrição da FILIAL e adicionar no cabeçalho
	obter_descricao('filiais', 'razao_social', 'codigo', obter_valor_sessao('filial'), '.filial' , obter_valor_sessao('empresa'));
	
	//Tipo de pedido
	$('.tipo_pedido').html('VENDA NORMAL');
	
	// Obter descrição do CLIENTE e adicionar no cabeçalho
	if(obter_valor_sessao('descricao_cliente')) {
		$('.cabecalho_cliente_prospect').html('Cliente');
		$('.cliente').html(obter_valor_sessao('descricao_cliente'));
	}
	
	// Obter descrição da TABELA DE PREÇOS e adicionar no cabeçalho
	obter_descricao('tabelas_preco', 'descricao', 'codigo', obter_valor_sessao('tabela_precos'), '.tabela_precos');
	
	// Obter descrição da CONDIÇÃO DE PAGAMENTO e adicionar no cabeçalho
	obter_descricao('formas_pagamento', 'descricao', 'codigo', obter_valor_sessao('condicao_pagamento'), '.condicao_pagamento');
}

/**
* Metódo:		obter_descricao
* 
* Descrição:	Função Utilizada para obter a descrição e codigo de um valor na sessão
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					tabela				- A tabela do navegador que será utilizada para realizar o "SELECT" do retorna de descrição
* @param		string 					campo_descricao		- O campo "Descrição" da "tabela" passada no primeiro parametro
* @param		string 					campo_codigo		- O campo "Codigo" da "tabela" passada no primeiro parametro
* @param		string 					codigo				- O Codigo que será usado para comparação no "WHERE"
* @param		string 					classe				- Classe html (.classe) que será adiciona o valor do retorno no html
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_descricao(tabela, campo_descricao, campo_codigo, codigo, classe, empresa) {
	var where  = '';
	if(!!empresa){
		where += ' AND empresa  = '+ empresa + ' ';
		
	}
	
	db.transaction(function(x) {	
		x.executeSql('SELECT ' + campo_descricao + ' AS descricao, ' + campo_codigo + ' AS codigo FROM ' + tabela + ' WHERE ' + campo_codigo + ' = ?' + where, [codigo], function(x, dados) {
			
			if(dados.rows.length) {
				var dado = dados.rows.item(0);
			
				$(classe).html(dado.codigo + ' - ' + dado.descricao);
			} else {
				$(classe).html('N/A');
			}
			
		});
	});
	
}

/**
* Metódo:		salvar_produto_sessao
* 
* Descrição:	Função Utilizada para salvar produtos adicionados
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produto_sessao(produtos_paramentro) {
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos']) {
		var sessao_produtos = sessao_pedido['produtos'];
	
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
		
	} else {
		var produtos = produtos_paramentro;
		
	}
	
	salvar_sessao('produtos', produtos);

}

/**
* Metódo:		salvar_produto_sessao
* 
* Descrição:	Função Utilizada para salvar produtos removidos
* 
* Data:			16/10/2013
* Modificação:	16/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produto_removido_sessao(produtos_paramentro) {
	var produtos_paramentro = unserialize(produtos_paramentro);
	
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao_pedido['produtos_removidos']) {
		var sessao_produtos = sessao_pedido['produtos_removidos'];
	
		var produtos = merge_options(sessao_produtos, produtos_paramentro);
		
	} else {
		var produtos = produtos_paramentro;
		
	}
	
	salvar_sessao('produtos_removidos', produtos);

}


/**
* Metódo:		salvar_produtos_excluidos
* 
* Descrição:	Função Utilizada para salvar produtos (Sem os removidos)
* 
* Data:			20/10/2013
* Modificação:	20/10/2013
* 
* @access		public
* @param		string 					produtos_paramentro		- produto serializado
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_produtos_excluidos(produtos_paramentro) {
	var produtos_paramentro = unserialize(produtos_paramentro);

	var produtos = produtos_paramentro;
	
	salvar_sessao('produtos', produtos);
}






/**
 * Obtem as derivações do produto e monta a listagem para preenchimento das quantidades
 * 
 * @param codigo_produto - Código do produto
 */
function obterProdutosClasse(codigo_classe) {
	var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	
	var where = '';
	var filial = obter_valor_sessao('filial');
	
	if(sessao_pedido.empresa) {
		where += " AND produtos.tb_empresa = '" + sessao_pedido.empresa + "'";
	}
	
	if (filial && filial != 'undefined') {
		where += " ";
	}
	
	$('.derivacoes_produto').html('');
	$('#lista_derivacoes').hide();
	$('#carregando_lista_derivacoes').show();
	
	var sql = 	"SELECT "+ 
				"	produtos.* , " +
				"	produtos_promocao.tb_promocional as produto_promocional, " +
				"	produtos_promocao.ptp_preco		 as produto_promocional_preco "	 +
				"FROM produtos " +
				"LEFT JOIN produtos AS produtos_promocao "  +
				"	ON ( " +
				"		produtos_promocao.tb_promocional = 'S' " +
				"		AND produtos_promocao.derivacao_classe_codigo_classe = produtos.derivacao_classe_codigo_classe "  +
				"		AND produtos_promocao.tb_filial = produtos.tb_filial "  +
				"		AND produtos_promocao.produto_codigo = produtos.produto_codigo  "  +
				") " +
				" WHERE     produtos.tb_codigo = ? AND produtos.derivacao_classe_codigo_classe = ? " + where + 
				
				" AND produtos.tb_promocional = 'N' " +
				
				" ORDER BY produto_descricao, derivacao_descricao_derivacao";
	
	
	
	db.transaction(function(x) {
		doQuery(x, sql, [obter_valor_sessao('tabela_precos'), codigo_classe], function(x, dados) {
			if (dados.rows.length) {
				var html = '';
				
				
				var total_geral = 0;
				for (i = 0; i < dados.rows.length; i++) {
					
					var aplicada_promocao = false;
					var dado = dados.rows.item(i);
					var codigo_produto = dado.produto_codigo;
					var quantidade_sessao = '';
					
					var preco_venda = 0;
					preco_venda = dado.ptp_preco;
					
					var produtos_sessao = obter_produtos_sessao();
					var derivacao = remover_zero_esquerda(dado.derivacao_codigo_derivacao);
					if(produtos_sessao) {
						if(produtos_sessao[codigo_produto]) {
							if(produtos_sessao[codigo_produto][derivacao]) {
								
								console.log(JSON.stringify(produtos_sessao[codigo_produto][derivacao]));
								
								quantidade_sessao = produtos_sessao[codigo_produto][derivacao]['quantidade'];
								aplicada_promocao = produtos_sessao[codigo_produto][derivacao]['aplicada_promocao'];
								preco_venda 	  = produtos_sessao[codigo_produto][derivacao]['preco_venda'];
							}
						}
					}
					
						html += '<tr style="font-size:16px">';
						html += '<td colspan="2" onClick="$(\'#produto_selecao_simple_'+i+'\').toggle();$(\'#produto_selecao_form_'+i+'\').toggle();">' + dado.produto_codigo + ' - ' + dado.produto_descricao +  obter_promocao_produto(i,dado, aplicada_promocao) +  '</td>';
						html += '<td colspan="2" onClick="$(\'#produto_selecao_simple_'+i+'\').toggle();$(\'#produto_selecao_form_'+i+'\').toggle();">' + dado.derivacao_codigo_derivacao + ' - ' + dado.derivacao_descricao_derivacao + '</td>';
						html += '<td>';
							html += '<div onClick="$(\'#produto_selecao_simple_'+i+'\').toggle();$(\'#produto_selecao_form_'+i+'\').toggle();" id="produto_selecao_simple_'+i+'" >';
								html += '<br /><strong><div style="font-size:18px; text-align: center;" id="label_valor_total_item_'+codigo_produto+'_'+dado.derivacao_codigo_derivacao+'"></div></strong><br />';
							html += '</div>';
							
							html += '<div id="produto_selecao_form_'+i+'" style="display:none; text-align: center" >';		
								html += '<strong>Quantidade</strong><br /><input style="width: 200px; text-align: center" type="number" class="quantidade_derivacao somenteNumero changeValue" name="quantidade[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" data-produto="' + codigo_produto + '" data-derivacao="' + dado.derivacao_codigo_derivacao + '" data-aplicada_promocao="' + aplicada_promocao + '"  value="' + quantidade_sessao + '"><br />';
								
								html += '<strong>Preço Venda</strong><br /><input type="text" class="preco_unitario_venda" name="preco_venda[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" disabled="disabled" value="' + number_format(preco_venda, 2, ',', '.') + '" style="width: 200px; text-align: center;" /><br />';
								//CUSTOM E150/2015
								html += '<input type="hidden" class="preco_unitario_derivacao" name="preco_unitario[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" disabled="disabled" value="' + number_format(dado.ptp_preco, 2, ',', '.') + '" style="width: 200px; text-align: center;" />';
								html += '<input type="hidden" class="preco_unitario_promocional_derivacao" name="preco_unitario_promocional[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" disabled="disabled" value="' + number_format(dado.produto_promocional_preco, 2, ',', '.') + '" style="width: 200px; text-align: center;" />';
								//CUSTOM E150/2015
								html += '<strong>Total</strong><br /><input type="text" class="total_derivacao" name="preco_total[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" disabled="disabled" value="0,00" style="width: 200px; text-align: center;" />';
								html += '<input type="hidden" name="descricao_produto[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" value="' + dado.produto_descricao + '" />';
								html += '<input type="hidden" name="descricao_derivacao[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" value="' + dado.derivacao_descricao_derivacao + '" />';
								
							html += '</div>';
						
						html += '</td>';
					html += '</tr>';
					
					/*
					html += '<tr id="xproduto_selecao_simple_'+i+'" style="display:none; font-size:14px">';
						html += '<td colspan="2">Quantidade</td>';
						html += '<td colspan="2">Preço Unitário</td>';
						html += '<td>Total</td>';
					html += '</tr>';
					
					html += '<tr id="xproduto_selecao_data_'+i+'" style="display:none; font-size:14px">';
						html += '<td colspan="2"><input type="number" class="quantidade_derivacao changeValue" name="quantidade[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" data-produto="' + codigo_produto + '" data-derivacao="' + dado.derivacao_codigo_derivacao + '" size="8" value="' + quantidade_sessao + '"></td>';
						html += '<td colspan="2"><input type="text" class="preco_unitario_derivacao" name="preco_unitario[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" disabled="disabled" value="' + number_format(dado.ptp_preco, 2, ',', '.') + '" style="width: 30%; text-align: right;" /></td>';
						html += '<td>';
							html += '<input type="text" class="total_derivacao" name="preco_total[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" disabled="disabled" value="0,00" style="width: 40%; text-align: right;" />';
							html += '<input type="hidden" name="descricao_produto[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" value="' + dado.produto_descricao + '" />';
							html += '<input type="hidden" name="descricao_derivacao[' + codigo_produto + '][' + dado.derivacao_codigo_derivacao + ']" value="' + dado.derivacao_descricao_derivacao + '" />';
						html += '</td>';
					html += '</tr>';
					*/
				}
				
				//Rodape com os totais
				html += '<tr class="novo_grid_rodape">';
					html += '<td colspan="4">TOTAL DO PRODUTO</td>';
					html += '<td align="right" id="valor_total_derivacao">' + number_format(total_geral, 2, ',', '.') + '</td>';
				html += '</tr>';
				
				
				//Monta a lista de derivações do produto
				$('.derivacoes_produto').html(html);
				
				$('#carregando_lista_derivacoes').hide();
				
				//Exibe o grid para preenchimento das quantidades
				$('#lista_derivacoes').show();
				
				$('.quantidade_derivacao').trigger('keyup');
				
			} else {
				var html = '<span class="class_info">Nenhuma derivação de produto encontrada.<span>';
				$("#carregando_lista_derivacoes").hide();
				$('.derivacoes_produto').html(html);
				
				$('#lista_derivacoes').show();
			}
			
		},
		function(tx, error) {
             console.log("Query Error: " + error.message); 
        }
		
		);
	});
}



/**
 * Verifica se o produto é uma promoção
 * 
 * */

function obter_promocao_produto(i, produto, aplicada_promocao){
	var promocao = '';
	var promocao_aplicada = '';
	
	console.log('Cod: '+ produto.produto_codigo+' - '+typeof(aplicada_promocao)  +' - '+ aplicada_promocao);
	
	if(aplicada_promocao === true  || aplicada_promocao === "true" ){
		promocao_aplicada += ' ativa ';		
		
	}
	
	if(produto.produto_promocional === 'S'){
		promocao += '<span   data-indice="'+i+'"  data-codigo_produto="'+produto.produto_codigo+'" data-derivacao_codigo_derivacao="'+produto.derivacao_codigo_derivacao+'"   class="produto_icone promocao '+promocao_aplicada+'"></span>'; 
		console.log(promocao);
	}
	
	return promocao;
	
}





/**
 * Obtem o valor total do produto de acordo com as quantidades de derivação
 */
function obterValorTotalDerivacao() {
	var valor_total_derivacao = 0;
	$.each($('.total_derivacao'), function(key, obj){
		var valor_total_item = converter_decimal($(obj).val());
			valor_total_item = parseFloat(valor_total_item);
			
		valor_total_derivacao += valor_total_item;
	});
	
	if (valor_total_derivacao == 0) {
		$('#btn_add_produtos').html('<button type="button" class="btn btn-default btn-large" disabled="disabled">Adicionar Produtos</button>');
	} else {
		$('#btn_add_produtos').html('<a id="adicionar_produto" class="btn btn-default btn-large">Adicionar Produtos</a>');
	}
	
	$('#valor_total_derivacao').html(number_format(valor_total_derivacao, 2, ',', '.'));
}

/**
 * Valida os itens que possuem quantidade e retona o array para gravar na sessão
 */
function validarProduto() {
	
	var produtos = obter_produtos_sessao();
	
	if(!produtos) {
		produtos = new Array();
	}
	
	$.each($('.quantidade_derivacao'), function(key, obj){
		var classe_com_itens = false;
		
		var quantidade = parseInt($(obj).val());
		
		var codigo_produto   = $(obj).data('produto');
		var codigo_derivacao = $(obj).data('derivacao');
		console.log(quantidade);
		
		if(!isNaN(quantidade) && quantidade > 0) { 
			
	        var aplicada_promocao= $(obj).data('aplicada_promocao');
			
			var preco_venda	= converter_decimal($("input[name='preco_venda[" + codigo_produto + "][" + codigo_derivacao + "]']").val());
			var preco_unitario	= converter_decimal($("input[name='preco_unitario[" + codigo_produto + "][" + codigo_derivacao + "]']").val());
						
			var descricao_produto	= $("input[name='descricao_produto[" + codigo_produto + "][" + codigo_derivacao + "]']").val();
			var descricao_derivacao = $("input[name='descricao_derivacao[" + codigo_produto + "][" + codigo_derivacao + "]']").val();
			
			var codigo_classe	= $("input[name='codigo_classe_produtos']").val();
			var descricao_classe = $("input[name='descricao_classe_produtos']").val();
			
			//Verifica se existe preço de tabela
			if(preco_venda <= 0) {
				produtos = false;				
			} else {
				if(!produtos[codigo_produto]){
					produtos[codigo_produto] = new Array();
				}
				produtos[codigo_produto][codigo_derivacao] = new Array();
				
				produtos[codigo_produto][codigo_derivacao]['descricao_produto']		= descricao_produto;
				produtos[codigo_produto][codigo_derivacao]['descricao_derivacao']	= descricao_derivacao;
				produtos[codigo_produto][codigo_derivacao]['codigo_classe']			= codigo_classe;
				produtos[codigo_produto][codigo_derivacao]['descricao_classe']		= descricao_classe;
				produtos[codigo_produto][codigo_derivacao]['codigo_produto']		= codigo_produto.toString();
				produtos[codigo_produto][codigo_derivacao]['codigo_derivacao']		= codigo_derivacao.toString();
				produtos[codigo_produto][codigo_derivacao]['quantidade']			= quantidade;				
				produtos[codigo_produto][codigo_derivacao]['preco_venda']			= parseFloat(preco_venda);
				produtos[codigo_produto][codigo_derivacao]['preco_unitario']		= parseFloat(preco_unitario);
				produtos[codigo_produto][codigo_derivacao]['aplicada_promocao']		= aplicada_promocao;
				
				classe_com_itens = true;
			}
		}else{
			
			if(produtos[remover_zero_esquerda(codigo_produto)]){
				
				delete (produtos[remover_zero_esquerda(codigo_produto)][remover_zero_esquerda(codigo_derivacao)]);
				
				
			}
		}
		
		if(classe_com_itens){
			$("#classe_" + codigo_classe + "_produto").addClass('btn-info');
		}
	});
	
	return produtos;
}

/**
 * Exibe os totais do pedido
 */
function obterTotaisPedido() {
	var valor_total = 0;
	var desconto_total = 0;
	var valor_total_com_impostos = 0;
	var quantidade_total = 0;
	var produtos = obter_produtos_sessao();
	var total_por_derivacao = {};
	
	
	var condicaoPagamentoModel = new CondicaoPagamento();
	
	condicaoPagamentoModel.obterPercentualDesconto( obter_valor_sessao('condicao_pagamento'), function(percentual_desconto){
	
		
		$.each(produtos, function(key, produto){
			$.each(produto, function(k, derivacao){
				if( isset(total_por_derivacao[derivacao.codigo_derivacao])){
					total_por_derivacao[derivacao.codigo_derivacao]  += derivacao.quantidade;
					
				}else{
					total_por_derivacao[derivacao.codigo_derivacao] = derivacao.quantidade;
					
				}
				var quantidade = derivacao.quantidade;
				var preco_unitario = derivacao.preco_venda;
				
				var preco_unitario_com_impostos = calcular_impostos_produtos(derivacao.preco_venda , percentual_desconto);
				//preco_unitario_com_impostos = (preco_unitario_com_impostos - (preco_unitario_com_impostos * 3 /100));
				quantidade_total++;
				valor_total += quantidade * preco_unitario;
				desconto_total +=  ((quantidade * preco_unitario) * percentual_desconto /100);
				valor_total_com_impostos += quantidade * preco_unitario_com_impostos;
			});
		});
		
		if(quantidade_total > 0) {
			
			var quantidade_potes = '';
			/*  Obter dinamicamente se necessário
			sessionStorage['sessao_tipo'] = 'sessao_pedido';
			
			var sessao_pedido  = obter_dados_pedido(); 
			var where = " where produto_empresa = '"+sessao_pedido.empresa+"' ";
			
			
			if (obter_valor_sessao('filial') && obter_valor_sessao('filial') != 'undefined') {
				var wheres = " ";
			}
			
			db.transaction(function(x) {
				doQuery(x, 'SELECT DISTINCT derivacao_classe_codigo_classe, derivacao_classe_descricao_classe FROM produtos WHERE tb_codigo = ?' + wheres, [obter_valor_sessao('tabela_precos')], function(x, dados) {
					var classe_descricao = [];
					for (var i = 0; i < dados.rows.length; i++) {
						var dado = dados.rows.item(i);
						classe_descricao[str_pad(dado.derivacao_classe_codigo_classe,2,'0','STR_PAD_LEFT')] = dado.derivacao_classe_descricao_classe;
					}
					
					
					
					
				})
			});*/
			var qtd_2_litros = 0;
			var qtd_1_litro = 0;
			$.each(total_por_derivacao, function(codigo_derivacao, total){
				if(in_array(codigo_derivacao, exibir_total_derivacao)){
					
					if(codigo_derivacao == '04'){
						qtd_1_litro = total;
						
					}else if(codigo_derivacao == '02'){
						qtd_2_litros = total; 
					}
				}
				
			});
			quantidade_potes += '<div class="qtd_embalagem_total"> <strong>Quantidade potes 1 litro:</strong> '+qtd_1_litro+'.</div>';
			quantidade_potes += '<div class="qtd_embalagem_total"> <strong>Quantidade potes 2 litros:</strong> '+qtd_2_litros+'.</div>';
			
			$('#quantidade_potes').html(quantidade_potes );	
			var html_totais_adicionados = '<h2>Valor Total do Pedido R$ ' + number_format(valor_total, 2, ',', '.') + '</h2>';
			html_totais_adicionados += '<br />';
			if(percentual_desconto > 0){
				html_totais_adicionados += '<h2>Desconto condição ('+number_format(percentual_desconto,2,',','.')+'%) R$ ' + number_format(desconto_total, 2, ',', '.') + '</h2>';
			}
			
			html_totais_adicionados += '<br />';
			if(obter_valor_sessao('empresa') == 1){
				html_totais_adicionados += '<h2>Valor Total do Pedido c/ Impostos R$ ' + number_format(valor_total_com_impostos, 2, ',', '.') + '</h2>';
			}else{
				html_totais_adicionados += '<h2>Valor Financeiro R$ ' + number_format(valor_total_com_impostos, 2, ',', '.') + '</h2>';
			}
			$('#quantidade_itens_pedido').html(quantidade_total + ' Produto(s) Adicionados.');
			
			$('#valor_total_pedido').html(html_totais_adicionados);
			
			$('#totais').show();
			
			marcar_classe_atendida();
		}
	});
}