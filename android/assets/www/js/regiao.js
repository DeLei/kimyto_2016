$(document).ready(function() {
	
	
	
	
	
	
	
	
	var html = '';
	var data_atual = protheus_data2data_normal(obterDataAtual());

	$("#data_atual").html(data_atual);

	
	
	obter_cliente_regiao( function(regioes) {
		var total = regioes.length; 
		if (total > 0) {
			for (var i = 0; i < total; i++) {
				html += obter_regiao_html(regioes[i]);
			}
		} else {
			html = '<div>Nenhuma região disponível.</div>';

		}
		
		$("#dados_html").html(html);

	});

});

/**
 * Obter regiões com clientes vinculados, onde a região nõ encontre-se expirada.
 * 
 * 
 */

function obter_cliente_regiao(  callback) {

	obter_regioes_vendas(  function(regioes) {
		//console.log('Regioes: '+ JSON.stringify(regioes));
		callback(regioes);

	});

}

function obter_regioes_vendas(  callback) {
	var data_atual = obterDataAtual();
	var regioes = [];

	var where = " WHERE   ativa = 'A' ";
	
	var order_by = ' ORDER BY data_validade ASC';
	/**
	 * Obter Regiões
	 * */
	var whereRegioesPendentes = "(SELECT count(*) FROM clientes WHERE clientes.codigo_regiao_venda = regioes_vendas.codigo AND clientes.status != 'I')";
	whereRegioesPendentes += "<>";
	whereRegioesPendentes += "(SELECT count(*) FROM pedido_venda WHERE pedido_venda.regiao_venda = regioes_vendas.codigo AND pedido_venda.regiao_data_validade = regioes_vendas.data_validade)";

	where += ' AND ' + whereRegioesPendentes;

	var sql = 'SELECT * FROM regioes_vendas  ' + where + order_by;
	console.log(sql);
	db.transaction(function(x) {
		x.executeSql(sql, [], function(x, dados) {
			
			
			var total = dados.rows.length;
			var item = 0;
		
			
			if (total > 0) {
				interacaoRegiao(dados, item, total, regioes, function(regioes) {
					callback(regioes);
				});
			} else {
				callback(regioes);
			}

		}, function(transaction, error) {
			alert("Error : " + error.message);
		});
	});

} 

function interacaoRegiao(dados, item, total, regioes, callback) {

	if (item == total) {
		callback(regioes);
	} else {

		var dado = dados.rows.item(item);
		var regiao = new Regioes_model();
		regiao.id = dado.id;
		regiao.codigo = dado.codigo;
		regiao.nome = dado.nome;
		regiao.ativa = dado.ativa;
		regiao.data_validade = dado.data_validade;

		regioes.push(regiao);

		interacaoRegiao(dados, item + 1, total, regioes, callback);
	}
}



function obter_regiao_html(regiao) {
	
	var html = '';
	
	html += '<a class="regiao_link" href="pedidos_adicionar_cliente_regiao.html#'+regiao.codigo+'|'+regiao.data_validade+'">'
	html += '<div id="' + regiao.id + '"  class="regiao_venda_box_a" >' + '<p>Região: ' + regiao.nome+ ' - Atender até: '+ protheus_data2data_normal(regiao.data_validade) + '</p>' +
			 
	
				
			'</div></a>';
	return html;
}



