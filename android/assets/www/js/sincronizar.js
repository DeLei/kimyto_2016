//------------------------------------------------------------------------//
//------------------------------ MÉTODOS ---------------------------------//
//------------------------------------------------------------------------//


/**
* Metódo:		sincronizar_modulo
* 
* Descrição:	Função Utilizada para chamar o modulo que será sincronizado
* 
* Data:			17/09/2013
* Modificação:	17/09/2013
* 
* @access		public
* @param		string 		$modulo					- Modulo que sera sincronizado
* @param		string 		$descricao_modulo		- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_modulo			- Proximo modulo que ser asincronizado quando finalizar o primeiro
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_modulo(modulo, descricao_modulo, proxima_modulo, parametros)
{
	
	// Salvando informações das sincronizações
	db.transaction(function(x) {
		x.executeSql('INSERT INTO sincronizacoes (timestamp, modulo, sucesso) VALUES (?, ?, ?)', [time(), modulo, 0]);
	});

	// Carregando
	$('#carregando').show();
	$('#btnCancelar').show();
	$('#btnSincronizar').hide();
	$('#carregando').find('span:first').html('Sincronizando ' + descricao_modulo + '…');

	//----------------------------------------------------------------------------------------------
	// Chamando o total de pacotes via ajax, e depois sincronizar dados do pacote
	//---------------------------------------------------------
	var ajax = $.ajax({
		url: config.ws_url + modulo + '/total',
		success: function(dados) {

			var total_pacotes = dados.total;
			//-----------
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM ' + modulo, [],
				function(x, dados){

					// A tabela existe, entao apaga os dados e contia a sincronização

					//1º Tentamos deletar os dados exportados das tabelas (Como somente as tabelas de exportação tem os campos "exportado", ira ocorrer um erro de sql)
					//2ª Se ocorrer um erro de SQL, vamos deletar os dados das tabelas que não existem os campos "exportado" 

					var where = '';

					if(modulo == 'pedidos_pendentes') {
						//Exclui somente os pedidos que já estão na SC5
						where = ' AND pedido_data_emissao <= "' + date('Ymd', strtotime('-15 days')) + '"';
					}

					x.executeSql('DELETE FROM ' + modulo + ' WHERE exportado = \'1\'' + where, [],
					function(){
						console.log('--==-- Apagando dados antigos do modulo ' + modulo + ' (EXPORTADO)');
						sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
					},
					function(){
						x.executeSql('DELETE FROM ' + modulo, [], function(){
							console.log('--==-- Apagando dados antigos do modulo ' + modulo);
							sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
						});
					});

				},
				function(){ // Erro de SQL, irá continuar a sincronização normalmente
					sincronizar_dados(modulo, 1, total_pacotes, proxima_modulo, parametros);
				});
			});

		}

	});

	//----------------------------------------------------------
	//----------------------------------------------------------------------------------------------
}

/**
* Metódo:		verificar_campos
* 
* Descrição: 		Verifica se os campos retornados pelo WS existem no banco de dados local
* 					se não existir gerar SQL ALTER e cria os campos todos para tipo TEXT.
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir)
{
	db.transaction(function(x) {

		var total_itens = dados.length;
		var item_atual 	= 0;
		var campos 		= [];

		//--------------------
		$.each(dados, function(i, objeto) {


			//--------------------
			$.each(objeto, function(indice, valor) {

				if(indice != 'rownum') 
				{
					campos.push(indice);
				}

			});
			//--------------------

			return false;
		});


		console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
		console.log('TABELA: '+ tabela);
		console.log('CAMPOS: '+ campos.join(', '));
		console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');


		x.executeSql('SELECT ' + campos.join(', ') + ' FROM ' + tabela, [], function(){
			if(inserir)
			{
				inserir_dados(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao);
			}
			else
			{
				finalizar_sincronizacao(tabela, proxima_funcao);
			}

		},
		function(xe, erro)
		{
			if(erro.code == 5)
			{
				var mensagem_erro = erro.message;
				var retorno = mensagem_erro.split(":");
				var campo = trim(retorno[1]);

				if(campo)
				{

					campo = str_replace(')','',campo);
					console.log(' -- ERRO COLUNA -- ');
					console.log('Coluna não existe: ' + campo);
					console.log('ALTER TABLE ' + tabela + ' ADD ' + campo + ' TEXT');
					x.executeSql('ALTER TABLE ' + tabela + ' ADD ' + campo + ' TEXT', [], function () {
						console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Tabela alterada com sucesso.');

						verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir);


					}, function(xy, error){
						console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Não foi possível alterar: ' + error.message);
					});

					console.log(' -- FIM -- ');

				}
				else
				{
					console.log(erro);
				}
			}

		});

	});
}

/**
* Metódo:		inserir_dados
* 
* Descrição:	Função Utilizada para inserir os dados json na tabela SQLLITE
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function inserir_dados(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao)
{


	var total_itens = dados.length;
	var item_atual = 0;

	db.transaction(function(x) {
		$.each(dados, function(i, objeto) {
			var campos 			= [];
			var interrogacoes  	= [];
			var valores		 	= [];

			$.each(objeto, function(indice, valor) {

				if(indice != 'rownum') //o campo rownum, vem automatico por conta do LIMIT no sqlsrv, entao por padrao, vamos remover esse retorno
				{
					campos.push(indice);
					interrogacoes.push('?');
					valores.push(valor);
				}

			});
			
			x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(a1, a2){
				item_atual++;
				var percontagem_item = percentagem_pacotes / total_itens;

				$( "#progressbar" ).progressbar({value: parseFloat($('#progressbar').attr('aria-valuenow')) + percontagem_item});

				if(total_itens == item_atual)
				{
					console.log('Pacote ' + pacote + ' foi finalizado!');

					$( "#progressbar" ).progressbar({value: round(percentagem_pacotes * pacote)});

					if(pacote < total_pacotes)
					{
						pacote++;
						sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao);
					}
					else
					{
						finalizar_sincronizacao(tabela, proxima_funcao);
					}
				}

			},
			function(x, erro){
				console.log(' ----- ');
				console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
				console.log(erro);
				console.log('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ") VALUES ('" + valores.join("', '") + "');");
				console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
				console.log(' ----- ');
			});

		});
	});


}


/**
* Metódo:		sincronizar_dados
* 
* Descrição:	Função Utilizada para salvar dados do web service no banco do navegador
* 
* Data:			17/09/2013
* Modificação:	17/09/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao, parametros)
{

	var percentagem_pacotes = 100 / total_pacotes; //Calculo de porcentagem por pacote
	if(pacote == 1)
	{
		$( "#progressbar" ).progressbar({value: 0});
	}
	
	//Mensagem no console
	console.log('Sincronizando ' + tabela + ' | Pacote = ' + pacote + ' | Total de Pacotes = ' + total_pacotes);
	console.log(' - - - - - PARAMETROS - - - - - ');
		
	if(typeof(parametros) === 'object'){
		parametros.pacote = pacote;
		
	}else{
		parametros = {};
		parametros.pacote = pacote;
	}	
	
	var ajax = $.ajax({
		url: config.ws_url + tabela + '/exportar' ,
		data: parametros,
		success: function(dados) {
			
			// Criar Tabela
			criar_tabela(tabela, dados, true);
			
			// Salvar Dados
			if(verificar_dados(dados))
			{
				
				verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, true);
			
				
			}
			else
			{
				verificar_campos(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, false);				
			}
			
		}
	});
	
	ajax.fail(function () {
		apprise('A sincronização não foi totalmente concluída. Você deseja tentar novamente?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				window.location = 'sincronizar.html';
			}
			else
			{
				window.location = 'index.html';
			}
		});
	});
	
}

/**
* Metódo:		finalizar_sincronizacao
* 
* Descrição:	Função Utilizada para finalizar um pacote, e chamar a proxima funcao
* 
* Data:			22/09/2013
* Modificação:	22/09/2013
* 
* @access		public
* @param		string 		$tabela							- Tabela que esta sendo sincronizada para ser exibida no console
* @param		string 		$proxima_funcao					- Proxima funcao que sera chamada quando finalziar a sincronização de um pacote
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function finalizar_sincronizacao(tabela, proxima_funcao)
{

	var modulos = new Object();
		modulos['empresas'] 							= 'Importação de Empresas';
		modulos['filiais'] 								= 'Importação de Filiais';
		modulos['clientes'] 							= 'Importação de Clientes';
		modulos['exportacao_historico_clientes'] 		= 'Exportação de Históricos de Clientes';
		modulos['historico_clientes'] 					= 'Importação de Históricos de Clientes';
		modulos['tabelas_preco'] 						= 'Importação de Tabelas de Preço';
		modulos['produtos'] 							= 'Importação de Produtos';
		modulos['formas_pagamento'] 					= 'Importação de Condições de Pagamento';
		modulos['municipios'] 							= 'Importação de Municípios';
		modulos['pedidos_processados'] 					= 'Importação de Pedidos Processados';
		modulos['pedidos_pendentes'] 					= 'Importação de Pedidos Pendentes';
		modulos['exportacao_pedidos_pendentes'] 		= 'Exportação de Pedidos';
		modulos['noticias'] 							= 'Importação de Notícias';
		modulos['derivacoes_produtos'] 					= 'Importação de Derivação de Produtos';
		
		//--------------------------
		// Pendências
		//--------------------------
		modulos['exportacao_pendencias_mensagens'] 		= 'Exportação de Pendências Mensagens';
		modulos['exportacao_pendencias'] 				= 'Exportação de Pendências';
		modulos['pendencias'] 							= 'Importação de Pendências';
		modulos['pendencias_usuarios'] 					= 'Importação de Pendências Destinatários';
		modulos['pendencias_mensagens'] 				= 'Importação de Pendências Mensagens';
		
		//--------------------------
		// Agenda
		//--------------------------
		modulos['exportacao_agenda'] 					= 'Exportação de Agenda';
		modulos['agenda'] 								= 'Importação de Agenda';
		
		modulos['regioes_vendas'] 						= 'Regiões Venda';
		modulos['motivo_nao_venda'] 					= 'Motivo Não Venda';
		modulos['exportacao_pedido_venda'] 				= 'Exportação de Pedido de Venda';
		modulos['pedido_venda'] 						= 'Pedido de Venda';
		modulos['notas_fiscais'] 						= 'Notas Fiscais';
		modulos['freezer'] 								= 'Freezer';
		modulos['registro_freezer'] 					= 'Registro de Freezers';
		
		
		
	//Salvando Informações da sincronização
	db.transaction(function(x) {
		x.executeSql('UPDATE sincronizacoes SET sucesso = ? WHERE modulo = ?', [1, tabela]);
	});

	//Mensagem para o usuário
	$('#'+tabela).after('<span class="ui-icon-white ui-icon-check"></span> ');
	console.log(tabela + ' - ' + modulos[tabela]);
	$('#modulos-sincronizados').prepend('<li><b>Sucesso.</b> - '+modulos[tabela]+'</li>').show();
	
	//Mensagem no console	
	console.log('A sincronização de ' + tabela + ' foi finalizada!');
	console.log('-------------------------------------------------');
	
	//Proxima funcao
	if(proxima_funcao)
	{
		eval(proxima_funcao);
	}
	else
	{
		mensagem('Sincronização concluída com sucesso.', "window.location = 'index.html';");																			
	}

}

//-- -- -- -- -- -- -- -- -- -- -- --

/**
* Metódo:		exportar_dados
* 
* Descrição:	Função Utilizada para enviar dados d eum modulo do DWFORÇA DE VENDAS para o WEBSERVICE
* 
* Data:			24/09/2013
* Modificação:	25/09/2013
* 
* @param		string 		$codigo_modulo					- ID do modulo (Campo Chave) para identificar a chave
* @param		string 		$modulo							- Modulo que sera sincronizado (nome da tabela no WS)
* @param		string 		$descricao_modulo				- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_funcao					- Proxima funcao que sera chamada quando finalziar a sincronização de um pacote
* @param		array 		$codigo							- Array utilizado para especificar quais dados serão enviados (EXP: vamos enviar somento o prospect "54321" => codigos[0] = '54321';)
*
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function exportar_dados(codigo_modulo, modulo, descricao_modulo, proxima_funcao, exportar_codigos)
{
	// Carregando
	$('#carregando').show();
	$('#btnCancelar').show();
	$('#btnSincronizar').hide();
	$('#carregando').find('span:first').html('Enviando ' + descricao_modulo + '…');
	$( "#progressbar" ).progressbar({value: 30});
	
	if(modulo == 'pedidos_pendentes'){
		var params = {
			codigo_modulo: codigo_modulo,
			modulo: modulo,
			descricao_modulo: descricao_modulo,
			proxima_funcao: proxima_funcao,
			exportar_codigos: exportar_codigos
		}
		
		obterPedidosEnviar(params, function(pedidos){
			
			enviarPedidos(params, pedidos, 0, function(){
				
				finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
				
			});
			
		});
	} else {
		db.transaction(function(x) {
			
			var where = '';
			
			if(modulo != 'pedidos_pendentes') {
				where += ' WHERE exportado IS NULL';
			}
	
			if(exportar_codigos && exportar_codigos.length > 0) {
				if(where == '') {
					where += ' WHERE ';
				} else {
					where += ' AND ';
				}
				where += codigo_modulo + " IN ('" + implode('\', \'', exportar_codigos) + "')";
			}
			
			x.executeSql('SELECT * FROM ' + modulo + where, [], function(x, dados) {
				
				var total_registros = dados.rows.length;
				
				console.log('Total = ' + total_registros);
				
				if (total_registros)
				{
					
					var retorno_json = new Array();
					for(registro = 0; registro < total_registros; registro++)
					{
						retorno_json[registro] = dados.rows.item(registro);
					}
					 
					var ajax = $.ajax({
						url: config.ws_url + modulo + '/importar',
						type: 'POST',
						data: {
							retorno: json_encode(retorno_json)
						},
						success: function(dados) {
							if(dados.sucesso == 'ok')
							{
								//Apagando os dados com erro na sessao
								localStorage.setItem('erros_' + modulo, '');
								
								if(exportar_codigos && exportar_codigos.length > 0)
								{
									// Marcando os dados especificados no "exportar_codigos" como exportados
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\')', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											
											if(modulo == 'pedido_venda'){
												
												console.log('Marcar codigos: '+ exportar_codigos );
												
												
												marcar_regiao_completa(exportar_codigos, function(){
													finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
												});
											} else {
												finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
											}
										});
									});
								}
								else
								{
									// Marcando todos os dados como exportados
									db.transaction(function(x) {
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL', [], function(){
											$( "#progressbar" ).progressbar({value: 100});
											finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										});
									});
								}
	
							} else if(dados.erro) {
								
								var codigos_sessao = [];
								var codigos = [];
								$.each(dados.erro, function(i, codigo) {
									codigos.push("'" + codigo + "'");
									
									if(is_numeric(codigo))
									{
										codigos_sessao.push(codigo);
									}
									
								});
								
								//Gravando na sessao dos dados com erro
								localStorage.setItem('erros_' + modulo, codigos_sessao);
								
								
								var descricoes = [];
								$.each(dados.erro_descricao, function(i, descricao) {
									descricoes.push(descricao);
								});
							
								db.transaction(function(x) {
								
									//Precisamos marcar quais dados estao com erro, e depois apagar todos os dados que não estao com erro
									x.executeSql('UPDATE ' + modulo + ' SET erro = \'1\' WHERE ' + codigo_modulo + ' IN (' + codigos.join(', ') + ')');
									
									if(exportar_codigos && exportar_codigos.length > 0)
									{
										// Marcando os dados especificados no "exportar_codigos" como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\') AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									}
									else
									{
										// Marcando todos os dados como exportados, exceto os pedidos em que ocorreu erro
										x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
									}
									
									
									apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível sincronizar:<b><br />(' + descricoes.join(', ') + ').</b><br />Entre em contato com TI da sua empresa.', {
										'textYes': 'OK'
									}, function (dado) {
									
										$( "#progressbar" ).progressbar({value: 100});
										finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
										
									});
	
									
								});
								
							}
						}
					});
					
					ajax.fail(function () {
						apprise('Não foi possível enviar os dados do módulo ' + descricao_modulo + '. Você deseja tentar novamente?', {
							'verify': true,
							'textYes': 'Sim',
							'textNo': 'Não'
						}, function (dado) {
							if (dado)
							{
								window.location = 'sincronizar.html';
							}
							else
							{
								window.location = 'index.html';
							}
						});
					});
					
					
				}
				else
				{
					db.transaction(function(x) {
						var where = '';
						
						if(modulo == 'pedidos_pendentes') {
							//Exclui somente os pedidos que já estão na SC5
							where = ' AND pedido_data_emissao <= "' + date('Ymd', strtotime('-15 days')) + '"';
						}
						
						x.executeSql('DELETE FROM ' + modulo + where);
						
						$( "#progressbar" ).progressbar({value: 100});
						finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
					});
				}
				
			},
			function (){ // Se a tabela não existir entrar nessa condição
				finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
			});
			
		});
	}
}

function obterPedidosEnviar(params, callback){
	
	var where = ' WHERE (exportado IS NULL OR erro = \'1\' OR editado = \'1\')';
	
	if(params.exportar_codigos && params.exportar_codigos.length > 0) {
		where += " AND " + params.codigo_modulo + " IN ('" + implode('\', \'', params.exportar_codigos) + "')";
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT ' + params.codigo_modulo + ' FROM pedidos_pendentes ' + where + ' GROUP BY ' + params.codigo_modulo, [], function(x, dados) {
			var pedidos = new Array();
			if(dados.rows.length){
				for(var i = 0; i < dados.rows.length; i++){
					var pedido = dados.rows.item(i);
					
					pedidos.push(pedido[params.codigo_modulo]);
				}
			}
			
			callback(pedidos);
		}, function(){
			callback();
		});
	});
	
}

function enviarPedidos(params, pedidos, iteracao, callback){
	
	if((pedidos) && (iteracao <= pedidos.length - 1)){
		$('#carregando').find('span:first').html('Enviando o pedido ' + pedidos[iteracao]+ '…');
		$("#progressbar").progressbar({value: 30});
		
		var where = ' WHERE (exportado IS NULL OR erro = \'1\' OR editado = \'1\')';
			where += " AND " + params.codigo_modulo + " = '" + pedidos[iteracao] + "'";
		
		console.log('Enviando pedido ' + pedidos[iteracao]);
		
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM ' + params.modulo + where, [], function(x, dados) {
				if(dados.rows.length){
					
					var dadosPedido = new Array();
					
					for(var i = 0; i < dados.rows.length; i++){
						var pedido = dados.rows.item(i);
						
						dadosPedido.push(pedido);
					}
					
					var ajax = $.ajax({
						url: config.ws_url + params.modulo + '/importar',
						type: 'POST',
						data: {
							retorno: json_encode(dadosPedido)
						},
						beforeSend: function(){
							$("#progressbar").progressbar({value: 66});
						},
						success: function(dados) {
							$("#progressbar").progressbar({value: 100});
							
							if(dados.sucesso == 'ok'){
								
								db.transaction(function(x) {
										x.executeSql('UPDATE ' + params.modulo + ' SET exportado = \'1\', erro = null, editado = null WHERE ' + params.codigo_modulo + ' = \'' + pedidos[iteracao] + '\'', [], function(){
											
											enviarPedidos(params, pedidos, iteracao + 1, callback);
											
										}, function(q, e){
											
											apprise('Erro ao atualizar o stauts do pedido <b>' + pedidos[iteracao] + '</b>.<br/><br/>Erro: ' + e.code + ' - ' + e.message, {
												'textYes': 'OK'
											}, function (dado) {
												enviarPedidos(params, pedidos, iteracao + 1, callback);
											});
											
										});
									
								});
								
							} else if(dados.erro) {
								
								apprise('Erro ao enviar o pedido <b>' + pedidos[iteracao] + '</b>.<br/><br/>' + dados.erro_descricao, {
									'textYes': 'OK'
								}, function (dado) {
									db.transaction(function(x) {
										x.executeSql('UPDATE pedido_venda SET erro = \'1\', exportado = null, editado = null WHERE codigo_pedido = \'' + pedidos[iteracao] + '\'', [], function(){
											x.executeSql('UPDATE ' + params.modulo + ' SET erro = \'1\', exportado = null WHERE ' + params.codigo_modulo + ' = \'' + pedidos[iteracao] + '\'', [], function(){
												
												enviarPedidos(params, pedidos, iteracao + 1, callback);
												
											}, function(q, e){
												
												apprise('Erro ao atualizar o stauts do pedido <b>' + pedidos[iteracao] + '</b>.<br/></br>Erro: ' + e.code + ' - ' + e.message, {
													'textYes': 'OK'
												}, function (dado) {
													enviarPedidos(params, pedidos, iteracao + 1, callback);
												});
												
											});
										}, function(q, e){
											
											apprise('Erro ao atualizar o stauts da venda para o pedido <b>' + pedidos[iteracao] + '</b>.<br><br>Erro: ' + e.code + ' - ' + e.message, {
												'textYes': 'OK'
											}, function (dado) {
												enviarPedidos(params, pedidos, iteracao + 1, callback);
											});
											
										});
									});
								});
								
							}
						},
						error: function(e){
							
							apprise('Erro ao conectar no servidor para realizar o envio do pedido <b>' + pedidos[iteracao] + '</b>.<br/><br/>Verifique sua <b>conexão</b> e tente novamente.<br/><br/><p>Erro: ' + e.responseText + '</p>', {
								'textYes': 'OK'
							}, function (dado) {
								enviarPedidos(params, pedidos, iteracao + 1, callback);
							});
							
						}
					});
				}
			});
		});
	} else {
		callback();
	}
	
}
