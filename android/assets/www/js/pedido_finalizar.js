$(document).ready(function(){

	//----------------------------
	// BOTÃO FINALIZAR PEDIDO
	//----------------------------	
	$('#botao_finalizar').click(function(e){
		e.preventDefault();
		
		var descricao_tipo_pedido = obter_descricao_pedido();
		
		confirmar('Tem certeza que deseja FINALIZAR o ' + descricao_tipo_pedido + ' ?', 
			function () {
				$(this).dialog('close');
				
				salvar_pedido();
				
				$("#confirmar_dialog").remove();
			}
		);
	
	});

});

function finalizar() {

	exibir_produtos_finalizar();
	
	exibir_outras_informacoes();
	
}

function exibir_produtos_finalizar() {
	
	var sessao_produtos = obter_produtos_sessao();
	
	if(sessao_produtos) {
		
		$('.itens_pedido_finalizar').empty();
		
		var total = 0;
		var total_com_impostos = 0;
		var html = '';

		var produtosOrdenadosClasse = [];
		
		
		
		$.each(sessao_produtos, function(key, produto){
			$.each(produto, function(k, derivacao){
				if (typeof derivacao !== 'undefined') {
					if (typeof produtosOrdenadosClasse[derivacao.codigo_classe] === 'undefined') {
						produtosOrdenadosClasse[derivacao.codigo_classe] = new Array();
					}
					produtosOrdenadosClasse[derivacao.codigo_classe].push(derivacao);
				}
			});
		});
		
		
		
		var condicaoPagamentoModel = new CondicaoPagamento();
		
		condicaoPagamentoModel.obterPercentualDesconto( obter_valor_sessao('condicao_pagamento'), function(percentual_desconto){
		
			$.each(produtosOrdenadosClasse, function(key, produto){
				if (typeof produto !== 'undefined') {
					$.each(produto, function(k, derivacao) {
						var preco_venda = number_format(derivacao.preco_venda, 2);
						var quantidade	= derivacao.quantidade;
						var preco_com_impostos = calcular_impostos_produtos(derivacao.preco_venda, percentual_desconto);
						html += '<tr>';
							html += '<td>' + derivacao.codigo_produto + ' - ' + derivacao.descricao_produto + '</td>';
							html += '<td>' + derivacao.codigo_derivacao + ' - ' + derivacao.descricao_derivacao + ' <br />(' + derivacao.descricao_classe + ')</td>';
							html += '<td align="center">' + quantidade + '</td>';
							html += '<td align="right">' + number_format(preco_venda, 2, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(preco_venda * quantidade, 2, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(preco_com_impostos, 2, ',', '.') + '</td>';
						html += '</tr>';
						
						total += preco_venda * quantidade;
						total_com_impostos += preco_com_impostos * quantidade;
					});
				}
			});
			
			html += '<tr class="novo_grid_rodape">';
				html += '<td colspan="4">TOTAL DO PEDIDO</td>';
				html += '<td align="right">' + number_format(total, 2, ',', '.') + '</td>';
				html += '<td align="right">' + number_format(total_com_impostos, 2, ',', '.') + '</td>';
			html += '</tr>';
			
			$('.itens_pedido_finalizar').html(html);
			
			alterarCabecalhoListagem('#itens_pedido_finalizar_tabela');
		});	
	}
}

function exibir_outras_informacoes() {
	
	var sessao_pedido = [];
	
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	$('.pedido_cliente').html(sessao_pedido.pedido_cliente ? sessao_pedido.pedido_cliente : 'N/A');
	$('.tipo_frete').html('CIF'); //Fixo como CIF
	$('.cliente_entrega').html(sessao_pedido.descricao_cliente_entrega ? sessao_pedido.descricao_cliente_entrega : 'N/A');
	$('.observacao_comercial').html(sessao_pedido.observacao_comercial ? sessao_pedido.observacao_comercial : 'N/A');
}

function salvar_pedido() {

	var sessao_pedido = [];

	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	var tabela 	= 'pedidos_pendentes';
	var tipo 	= 'P';
	
	var sessao_produtos = sessao_pedido.produtos;
	
	if(sessao_produtos) {
		var total_produtos 		= obterQuantidadeItens(sessao_produtos);
	} else {
		var total_produtos 		= 0;
	}
	
	
	//-----------------
	var dados_cliente 			= sessao_pedido.dados_cliente;
	var dados_forma_pagamento	= sessao_pedido.dados_forma_pagamento;
	
	if(total_produtos > 0) {
	
		var editar = sessao_pedido.editar;
		
		if(editar) {
			var id_pedido 		= sessao_pedido.id_pedido;
		} else {
			var id_pedido 		= uniqid();
		}
		
		var data_emissao 	= date('Ymd');
		var time_emissao 	= time();
		var latitude		= localStorage.getItem('gps_latitude');
		var longitude		= localStorage.getItem('gps_longitude');
		var produtos		= new Array();
		var derivacoes		= new Array();
		
		var numero_item = 1;
		var pedido_orginal_removido = false;
		$.each(sessao_produtos, function(key, item){
			$.each(item, function(k, produto) {
				var campos 			= [];
				var interrogacoes  	= [];
				var valores		 	= [];
				
				//----------------------------------- Campos -----------------------------------//
				
				/**
				 * Cabeçalho
				 */
				campos.push('empresa');										interrogacoes.push('?');
				campos.push('filial');										interrogacoes.push('?');
				campos.push('pedido_codigo_empresa');						interrogacoes.push('?');
				campos.push('pedido_filial');								interrogacoes.push('?');
				campos.push('pedido_id_pedido'); 							interrogacoes.push('?');
				campos.push('pedido_data_emissao'); 						interrogacoes.push('?');
				campos.push('pedido_time_emissao'); 						interrogacoes.push('?');
				campos.push('pedido_id_usuario'); 							interrogacoes.push('?');
				campos.push('pedido_codigo_representante'); 				interrogacoes.push('?');
				campos.push('pedido_codigo_cliente'); 						interrogacoes.push('?');
				campos.push('pedido_loja_cliente');							interrogacoes.push('?');
				campos.push('pedido_condicao_pagamento');					interrogacoes.push('?');
				campos.push('pedido_tabela_precos');						interrogacoes.push('?');
				campos.push('pedido_tipo_frete');							interrogacoes.push('?');
				campos.push('pedido_tipo_venda');							interrogacoes.push('?');
				campos.push('pedido_pedido_cliente');						interrogacoes.push('?');
				campos.push('pedido_observacao_comercial');					interrogacoes.push('?');
				campos.push('pedido_cliente_entrega');						interrogacoes.push('?');
				campos.push('pedido_loja_entrega');							interrogacoes.push('?');
				campos.push('pedido_latitude');								interrogacoes.push('?');
				campos.push('pedido_longitude');							interrogacoes.push('?');
				campos.push('pedido_versao');								interrogacoes.push('?');
				campos.push('pedido_status');								interrogacoes.push('?');
				
				/**
				 * Dados do cliente
				 */
				campos.push('cliente_codigo');								interrogacoes.push('?');				
				campos.push('cliente_loja');								interrogacoes.push('?');
				campos.push('cliente_nome');								interrogacoes.push('?');
				campos.push('cliente_cpf');									interrogacoes.push('?');
				campos.push('cliente_pessoa_contato');						interrogacoes.push('?');
				campos.push('cliente_endereco');							interrogacoes.push('?');
				campos.push('cliente_bairro');								interrogacoes.push('?');
				campos.push('cliente_cep');									interrogacoes.push('?');
				campos.push('cliente_cidade');								interrogacoes.push('?');
				campos.push('cliente_estado');								interrogacoes.push('?');
				campos.push('cliente_telefone');							interrogacoes.push('?');
				campos.push('cliente_email');								interrogacoes.push('?');
				
				/**
				 * Dados da condição de pagamento
				 */
				campos.push('forma_pagamento_codigo');						interrogacoes.push('?');
				campos.push('forma_pagamento_descricao');					interrogacoes.push('?');
				campos.push('forma_pagamento_percentual_desconto');			interrogacoes.push('?');
				
				
				/**
				 * Produtos
				 */
				campos.push('pedido_numero_item');							interrogacoes.push('?');
				campos.push('produto_descricao');							interrogacoes.push('?');
				campos.push('pedido_codigo_produto');						interrogacoes.push('?');
				campos.push('pedido_derivacao');							interrogacoes.push('?');
				campos.push('derivacao_descricao_derivacao');				interrogacoes.push('?');
				campos.push('pedido_preco_venda');							interrogacoes.push('?');
				campos.push('pedido_preco_unitario');						interrogacoes.push('?');
				campos.push('pedido_quantidade');							interrogacoes.push('?');
				campos.push('classe_codigo_classe');						interrogacoes.push('?');
				campos.push('classe_descricao_classe');						interrogacoes.push('?');
				campos.push('pedido_aplicada_promocao');					interrogacoes.push('?');
				
				
				//----------------------------------- Valores -----------------------------------//
				
				/**
				 * Cabeçalho
				 */
				
				
				
				valores.push(dados_cliente.empresa);
				valores.push(sessao_pedido.filial);
				valores.push(dados_cliente.empresa);
				valores.push(sessao_pedido.filial);
				valores.push(id_pedido);
				valores.push(data_emissao);
				valores.push(time_emissao);
				valores.push(info.id_rep);
				valores.push(info.cod_rep);
				valores.push(sessao_pedido.codigo_cliente);
				valores.push(sessao_pedido.loja_cliente);
				valores.push(sessao_pedido.condicao_pagamento);
				valores.push(sessao_pedido.tabela_precos);
				valores.push('C');
				valores.push('N');
				valores.push(sessao_pedido.pedido_cliente);
				valores.push(sessao_pedido.observacao_comercial);
				valores.push(sessao_pedido.codigo_cliente_entrega);
				valores.push(sessao_pedido.loja_cliente_entrega);
				valores.push(latitude);
				valores.push(longitude);
				valores.push(config.versao);
				valores.push('L');
				
				/**
				 * Dados do cliente
				 */
				valores.push(dados_cliente.codigo);
				valores.push(dados_cliente.loja);
				valores.push(dados_cliente.nome);
				valores.push(dados_cliente.cpf);
				valores.push(dados_cliente.pessoa_contato);
				valores.push(dados_cliente.endereco);
				valores.push(dados_cliente.bairro);
				valores.push(dados_cliente.cep);
				valores.push(dados_cliente.cidade);
				valores.push(dados_cliente.estado);
				valores.push(dados_cliente.telefone);
				valores.push(dados_cliente.email);
				
				/**
				 * Dados da condição de pagamento
				 */
				valores.push(dados_forma_pagamento.codigo);
				valores.push(dados_forma_pagamento.descricao);
				valores.push(dados_forma_pagamento.percentual_desconto);
				
				
				
				/**
				 * Produtos
				 */
				valores.push(numero_item);
				valores.push(produto.descricao_produto);
				valores.push(produto.codigo_produto);
				valores.push(produto.codigo_derivacao);
				valores.push(produto.descricao_derivacao);
				valores.push(produto.preco_venda);
				valores.push(produto.preco_unitario);
				valores.push(produto.quantidade);
				valores.push(produto.codigo_classe);
				valores.push(produto.descricao_classe);
			
				
				
				/**
				 * Utilizado na validação para pedidos editados
				 */
				produtos.push(produto.codigo_produto);
				derivacoes.push(produto.codigo_derivacao);
				valores.push(produto.aplicada_promocao);
				
				
				db.transaction(function(x) {
					
					//Remove o pedido antigo na primeira vez que passar no foreach e reinsere os dados
				 	if(editar && !pedido_orginal_removido) {
				 		pedido_orginal_removido = true;
						x.executeSql('DELETE FROM pedidos_pendentes WHERE pedido_id_pedido = "' + id_pedido + '"');
						x.executeSql('DELETE FROM pedido_venda WHERE codigo_pedido = "' + id_pedido + '"');
					}
				
					x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(){
						total_produtos--;
						
						if(total_produtos == 0) {
							/* Custom E150/2015*/
							//Salvando dados do pedido na tabela pedido venda
							salvar_pedido_venda_cliente(id_pedido, sessao_pedido,function(){
								console.log('Id: '+ id_pedido);
								// Apagando pedido da sessao
								location.href="pedidos_espelho.html#" + id_pedido + '|' + sessao_pedido.filial + '|' + tipo;
								sessionStorage[sessionStorage['sessao_tipo']] = '';
							
							});
						}
					}, function (e, t) {
						console.log(JSON.stringify(e));
						console.log(JSON.stringify(t));
					});
				});
				
				numero_item++;
			});
		});
	}
}

function finalizar_pedido_editado(id_pedido, filial, produtos, derivacoes) {

	//Definindo Tabela de pedidos ou or�amentos
	if(sessionStorage['sessao_tipo'] == 'sessao_orcamento') {
		var tabela = 'orcamentos';
		var tipo = 'O';
	} else {
		var tabela = 'pedidos_pendentes';
		var tipo = 'P';
	}
	
	console.log('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto NOT IN ("' + produtos.join('", "') + '" AND pedido_derivacao NOT IN ("' + derivacoes.join('", "') + '")');
	return false;
	db.transaction(function(x) {
		x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + id_pedido + '" AND pedido_codigo_produto NOT IN ("' + produtos.join('", "') + '")', [], function(){
			// Apagando pedido da sessao
			location.href="pedidos_espelho.html#" + id_pedido + '|' + filial + '|' + tipo;
			sessionStorage[sessionStorage['sessao_tipo']] = '';
		});
	});
}

function obterQuantidadeItens(sessao_produtos){
	var total = 0;
	$.each(sessao_produtos, function(codigo_produto, produto) {
		total += Object.keys(produto).length; //Obtem a quantidade de derivações
	});
	
	return total;
}