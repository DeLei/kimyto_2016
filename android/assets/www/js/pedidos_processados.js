$(document).ready(function() {
	
	var codigo_e_loja = parse_url(location.href).fragment;
	
	if(codigo_e_loja) {
		codigo_e_loja = codigo_e_loja.split('_');
		sessionStorage['ped_pro_cliente'] = codigo_e_loja[0];
	}
	
	// obter pedidos
	obter_pedidos(sessionStorage['ped_pro_pagina_atual'], sessionStorage['ped_pro_ordem_atual'], sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
	
	// ordenação
	$('table thead tr th a').click(function() {
		$('.ordem').empty();
		
		if ($(this).data('ordem') == 'ASC') {
			obter_pedidos(sessionStorage['ped_pro_pagina_atual'], $(this).data('campo') + ' DESC', sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
			
			$(this).data('ordem', 'DESC');
			
			$(this).find('.ordem').text('▼');
			
		} else {
			
			obter_pedidos(sessionStorage['ped_pro_pagina_atual'], $(this).data('campo') + ' ASC', sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
			
			$(this).data('ordem', 'ASC');
			
			$(this).find('.ordem').text('▲');
		}
		
		return false;
	});
	
	// filtros
	
	// obter filiais
	db.transaction(function(x) {	
		x.executeSql('SELECT * FROM filiais WHERE empresa = ?', [info.empresa], function(x, dados) {
				
			var total = dados.rows.length;
			
			$('select[name=filial]').append('<option value="">Todos</option>');
			
			for(i = 0; i < total; i++) {
				var dado = dados.rows.item(i);
				$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
			}
		});
	});
	
	
	$('select[name=situacao]').val(sessionStorage['ped_pro_situacao']);
	$('input[name=cliente]').val(sessionStorage['ped_pro_cliente']);
	$('input[name=codigo]').val(sessionStorage['ped_pro_codigo']);
	$('input[name=dt_inicial]').val(sessionStorage['ped_pro_dt_inicial']);
	$('input[name=dt_final]').val(sessionStorage['ped_pro_dt_final']);
	
	$('#filtrar').click(function() {
		sessionStorage['ped_pro_pagina_atual'] = 1;
		sessionStorage['ped_pro_filial'] = $('select[name=filial]').val();
		sessionStorage['ped_pro_situacao'] = $('select[name=situacao]').val();
		sessionStorage['ped_pro_cliente'] = $('input[name=cliente]').val();
		sessionStorage['ped_pro_codigo'] = $('input[name=codigo]').val();
		sessionStorage['ped_pro_dt_inicial'] = $('input[name=dt_inicial]').val();
		sessionStorage['ped_pro_dt_final'] = $('input[name=dt_final]').val();
		
		obter_pedidos(sessionStorage['ped_pro_pagina_atual'], sessionStorage['ped_pro_ordem_atual'], sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['ped_pro_pagina_atual'] = 1;
		sessionStorage['ped_pro_situacao'] = '';
		sessionStorage['ped_pro_filial'] = '';
		sessionStorage['ped_pro_cliente'] = '';
		sessionStorage['ped_pro_codigo'] = '';
		sessionStorage['ped_pro_dt_inicial'] = '';
		sessionStorage['ped_pro_dt_final'] = '';

		$('select[name=situacao]').val(sessionStorage['ped_pro_situacao']);
		$('select[name=filial]').val(sessionStorage['ped_pro_filial']);
		$('input[name=cliente]').val(sessionStorage['ped_pro_cliente']);
		$('input[name=codigo]').val(sessionStorage['ped_pro_codigo']);
		$('input[name=dt_inicial]').val(sessionStorage['ped_pro_dt_inicial']);
		$('input[name=dt_final]').val(sessionStorage['ped_pro_dt_final']);
		
		obter_pedidos(sessionStorage['ped_pro_pagina_atual'], sessionStorage['ped_pro_ordem_atual'], sessionStorage['ped_pro_situacao'], sessionStorage['ped_pro_cliente'], sessionStorage['ped_pro_codigo'], sessionStorage['ped_pro_dt_inicial'], sessionStorage['ped_pro_dt_final'], sessionStorage['ped_pro_filial']);
	});
});

function obter_pedidos(pagina, ordem, situacao, cliente, codigo, dt_inicial, dt_final, filial) {
	pagina = pagina ? pagina : 1;
	ordem = ordem ? ordem : 'pedido_data_emissao DESC';
	situacao = situacao ? situacao : '';
	filial = filial ? filial : '';
	cliente = cliente ? cliente : '';
	codigo = codigo ? codigo : '';
	dt_inicial = dt_inicial ? dt_inicial : '';
	dt_final = dt_final ? dt_final : '';
	
	// setar pagina e ordem atual
	sessionStorage['ped_pro_pagina_atual'] = pagina;
	sessionStorage['ped_pro_ordem_atual'] = ordem;
	sessionStorage['ped_pro_situacao'] = situacao;
	sessionStorage['ped_pro_filial'] = filial;
	sessionStorage['ped_pro_cliente'] = cliente;
	sessionStorage['ped_pro_codigo'] = codigo;
	sessionStorage['ped_pro_dt_inicial'] = dt_inicial;
	sessionStorage['ped_pro_dt_final'] = dt_final;
	
	// definir seta
	var ordem_atual = explode(' ', sessionStorage['ped_pro_ordem_atual']);
	
	if (ordem_atual[1] == 'ASC') {
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'ASC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▲');
	
	} else {
		
		$('a[data-campo=' + ordem_atual[0] + ']').data('ordem', 'DESC');
		
		$('a[data-campo=' + ordem_atual[0] + ']').find('.ordem').text('▼');
	}
	
	// calcular offset
	var offset = (sessionStorage['ped_pro_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	var wheres = '';
	
	if (situacao) {
		wheres += 'AND status LIKE "%' + situacao + '%"';
	} else {
		wheres += 'AND status IS NOT NULL';
	}
	
	if (filial > 0) {
		//wheres += ' AND pedido_filial = "' + filial + '"';
		wheres += ' AND (pedido_filial = "' + filial + '" OR pedido_filial = " ")';
	}
	
	if (cliente) {
		wheres += ' AND cliente_nome LIKE "%' + cliente + '%" OR '+' cliente_codigo LIKE "%' + cliente + '%"';
	}
	
	if (codigo) {
		wheres += ' AND pedido_codigo LIKE "%' + codigo + '%"';
	}
	
	if (dt_inicial && dt_inicial != 'undefined') {	
		var ex = explode('/', dt_inicial);
		
		wheres += ' AND pedido_data_emissao >= "' + ex[2] + ex[1] + ex[0] + '"';
	}

	if (dt_final && dt_final != 'undefined') {
		var ex = explode('/', dt_final);
	
		wheres += ' AND pedido_data_emissao <= "' + ex[2] + ex[1] + ex[0] + '"';
	}
	
	if (info.empresa) {
		wheres += " AND empresa = '" + info.empresa + "'";
	}
	
	var consulta_pedidos_na_empresa = 'SELECT pedido_codigo, SUM(ip_valor_total_item) AS valor_total, SUM(ip_preco_produto * ip_quantidade_faturada_produto) AS valor_faturado, SUM(ip_quantidade_vendida_produto) AS quantidade_vendida, SUM(ip_quantidade_faturada_produto) AS quantidade_faturada, * '+
	' FROM pedidos_processados ' +
	'JOIN regioes_vendas ON (regioes_vendas.codigo  = cliente_codigo_regiao_venda ) '+
	' WHERE pedido_codigo > 0 ' + wheres + ' GROUP BY pedido_codigo ORDER BY ' + sessionStorage['ped_pro_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset;
	
	console.log(consulta_pedidos_na_empresa);
	
	// exibir pedido e calcular totais
	db.transaction(function(x) {
		x.executeSql(consulta_pedidos_na_empresa, [], function(x, dados) {
			if (dados.rows.length) {
				$('table tbody').empty();
				
				for (i = 0; i < dados.rows.length; i++) {
					
					var pedido = dados.rows.item(i);
					
					var pedidos = [];
					pedidos.push('<img src="img/' + pedido.status + '.png">');
					pedidos.push(pedido.pedido_codigo + '/' + pedido.pedido_filial);
					pedidos.push(pedido.cliente_nome);
					pedidos.push(protheus_data2data_normal(pedido.pedido_data_emissao));
					pedidos.push(number_format(pedido.quantidade_vendida, 0, ',', '.'));
					pedidos.push(number_format(pedido.quantidade_faturada, 0, ',', '.'));
					pedidos.push(number_format(pedido.valor_faturado, 2, ',', '.'));
					pedidos.push(number_format(pedido.valor_total, 2, ',', '.'));
					
					pedidos.push('<a href="pedidos_espelho.html#' + pedido.pedido_codigo + '|' + pedido.pedido_filial + '" class="btn btn-large btn-primary">+ Opções</a>');
					
					var html = concatenar_html('<td>', '</td>', pedidos);
				
					$('table tbody').append('<tr>' + html + '</tr>');
					
					alterarCabecalhoTabelaResolucao();
				}
				
			} else {
				$('table tbody').html('<tr><td colspan="10" style="color: #900; padding: 10px;"><strong>Nenhum pedido encontrado.</strong></td></tr>');
			}
		});
		
		// calcular totais
		
		var consulta_pedidos_na_empresa_total = 'SELECT COUNT(DISTINCT pedido_codigo) AS total, SUM(ip_valor_total_item) AS valor_total '+
		' FROM pedidos_processados '+
		' JOIN regioes_vendas ON (regioes_vendas.codigo  = cliente_codigo_regiao_venda ) '+ 
		' WHERE pedido_codigo != "" ' + wheres;
		
		x.executeSql(consulta_pedidos_na_empresa_total, [], function(x, dados) {
			var dado = dados.rows.item(0);
			
			$('#total').text(number_format(dado.total, 0, ',', '.'));
			$('#valor_total').text(number_format(dado.valor_total, 2, ',', '.'));
			
			// paginação
			
			$('#paginacao').html('');
			
			var total = ceil(dado.total / 20);
			
			if (sessionStorage['ped_pro_pagina_atual'] > 6) {
				$('#paginacao').append('<a href="#" onclick="obter_pedidos(1, \'' + sessionStorage['ped_pro_ordem_atual'] + '\', \'' + sessionStorage['ped_pro_situacao'] + '\', \'' + sessionStorage['ped_pro_cliente'] + '\', \'' + sessionStorage['ped_pro_codigo'] + '\', \'' + sessionStorage['ped_pro_dt_inicial'] + '\', \'' + sessionStorage['ped_pro_dt_final'] + '\', \'' + sessionStorage['ped_pro_filial'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
			}
			
			if (sessionStorage['ped_pro_pagina_atual'] > 1) {
				$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + (intval(sessionStorage['ped_pro_pagina_atual']) - 1) + ', \'' + sessionStorage['ped_pro_ordem_atual'] + '\', \'' + sessionStorage['ped_pro_situacao'] + '\', \'' + sessionStorage['ped_pro_cliente'] + '\', \'' + sessionStorage['ped_pro_codigo'] + '\', \'' + sessionStorage['ped_pro_dt_inicial'] + '\', \'' + sessionStorage['ped_pro_dt_final'] + '\', \'' + sessionStorage['ped_pro_filial'] + '\'); return false;">&lt;</a> ');
			}
			
			for (i = intval(sessionStorage['ped_pro_pagina_atual']) - 6; i <= intval(sessionStorage['ped_pro_pagina_atual']) + 5; i++) {
				if (i <= 0 || i > total) {
					continue;
				}
				
				if (i == sessionStorage['ped_pro_pagina_atual']) {
					$('#paginacao').append('<strong>' + i + '</strong> ');
				} else {
					$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + i + ', \'' + sessionStorage['ped_pro_ordem_atual'] + '\', \'' + sessionStorage['ped_pro_situacao'] + '\', \'' + sessionStorage['ped_pro_cliente'] + '\', \'' + sessionStorage['ped_pro_codigo'] + '\', \'' + sessionStorage['ped_pro_dt_inicial'] + '\', \'' + sessionStorage['ped_pro_dt_final'] + '\', \'' + sessionStorage['ped_pro_filial'] + '\'); return false;">' + i + '</a> ');
				}
			}
			
			if (sessionStorage['ped_pro_pagina_atual'] < total) {
				$('#paginacao').append('<a href="#" onclick="obter_pedidos(' + (intval(sessionStorage['ped_pro_pagina_atual']) + 1) + ', \'' + sessionStorage['ped_pro_ordem_atual'] + '\', \'' + sessionStorage['ped_pro_situacao'] + '\', \'' + sessionStorage['ped_pro_cliente'] + '\', \'' + sessionStorage['ped_pro_codigo'] + '\', \'' + sessionStorage['ped_pro_dt_inicial'] + '\', \'' + sessionStorage['ped_pro_dt_final'] + '\', \'' + sessionStorage['ped_pro_filial'] + '\'); return false;">&gt;</a> ');
			}
			
			if (sessionStorage['ped_pro_pagina_atual'] <= total - 6) {
				$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_pedidos(' + total + ', \'' + sessionStorage['ped_pro_ordem_atual'] + '\', \'' + sessionStorage['ped_pro_situacao'] + '\', \'' + sessionStorage['ped_pro_cliente'] + '\', \'' + sessionStorage['ped_pro_codigo'] + '\', \'' + sessionStorage['ped_pro_dt_inicial'] + '\', \'' + sessionStorage['ped_pro_dt_final'] + '\', \'' + sessionStorage['ped_pro_filial'] + '\'); return false;">Última Página</a> ');
			}
		});
	});
}
