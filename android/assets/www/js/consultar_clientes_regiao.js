$(document).ready(function() {
	
	// obter produtos
	obter_clientes_regiao(sessionStorage['pedidos_regiao_pagina_atual'], sessionStorage['pedidos_regiao_ordem_atual']);
	
	// filtros
	
	$('[name]').each(function () {
		var name = $(this).attr('name');
		
		$(this).val(sessionStorage['pedidos_regiao_' + name] != 'undefined' ? sessionStorage['pedidos_regiao_' + name] : '');
	});
	
	$('#filtrar').click(function() {
		sessionStorage['pedidos_regiao_pagina_atual'] = 1;
		
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			console.log(name +' - '+ $(this).val());
			sessionStorage['pedidos_regiao_' + name] = $(this).val();
		});
		
		obter_clientes_regiao(sessionStorage['pedidos_regiao_pagina_atual'], sessionStorage['pedidos_regiao_ordem_atual']);
	});
	
	$('#limpar_filtros').click(function() {
		sessionStorage['pedidos_regiao_pagina_atual'] = 1;
		
		$('[name]').each(function () {
			var name = $(this).attr('name');
			
			$(this).val('');
			
			sessionStorage['pedidos_regiao_' + name] = '';
		});
		
		obter_clientes_regiao(sessionStorage['pedidos_regiao_pagina_atual'], sessionStorage['pedidos_regiao_ordem_atual']);
	});
});

function obter_clientes_regiao(pagina, ordem) {
	pagina = pagina ? pagina : 1;
	
	
	// setar pagina 
	sessionStorage['pedidos_regiao_pagina_atual'] = pagina;
	
	// calcular offset
	var offset = (sessionStorage['pedidos_regiao_pagina_atual'] - 1) * 20;
	
	// gerar filtros
	
	
	termo = sessionStorage['pedidos_regiao_termo'];
	
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM produtos WHERE produto_codigo > 0 ' + wheres + ' ORDER BY ' + sessionStorage['pedidos_regiao_ordem_atual'] + ' LIMIT 20 OFFSET ' + offset, [], function(x, dados) {
			
			if (dados.rows.length) {
				
				$('table tbody').empty();
		
				for (i = 0; i < dados.rows.length; i++) {
					var item = dados.rows.item(i);
					
					var itens = [];
					
					itens.push(item.produto_filial);
					itens.push(item.produto_codigo);
					itens.push(item.produto_descricao);
					itens.push(item.tb_codigo);
					itens.push(number_format(item.ptp_preco, 3, ',', '.'));
					itens.push(number_format(item.produto_ipi, 3, ',', '.'));
					
					var html = concatenar_html('<td>', '</td>', itens);
					
					$('table tbody').append('<tr>' + html + '</tr>');
				}
				
				alterarCabecalhoTabelaResolucao();
			
			} else {
				$('table tbody').html('<tr><td colspan="7" style="color: #900; padding: 10px;"><strong>Nenhum produto encontrado.</strong></td></tr>');
			}
		});
		
		// calcular totais
		x.executeSql('SELECT COUNT(produto_codigo) AS total FROM produtos WHERE produto_codigo > 0 ' + wheres, [], function(x, dados) {
			
			var dado = dados.rows.item(0);
			
			$('#total').text(number_format(dado.total, 0, ',', '.'));
			
			// paginação
			$('#paginacao').html('');
			
			var total = ceil(dado.total / 20);
			
			if (total > 1) {
				if (sessionStorage['pedidos_regiao_pagina_atual'] > 6) {
					$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(1, \'' + sessionStorage['pedidos_regiao_ordem_atual'] + '\'); return false;">Primeira Página</a>&nbsp;&nbsp;');
				}
				
				if (sessionStorage['pedidos_regiao_pagina_atual'] > 1) {
					$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(' + (intval(sessionStorage['pedidos_regiao_pagina_atual']) - 1) + ', \'' + sessionStorage['pedidos_regiao_ordem_atual'] + '\'); return false;">&lt;</a> ');
				}
				
				for (i = intval(sessionStorage['pedidos_regiao_pagina_atual']) - 6; i <= intval(sessionStorage['pedidos_regiao_pagina_atual']) + 5; i++) {
					if (i <= 0 || i > total) {
						continue;
					}
					
					if (i == sessionStorage['pedidos_regiao_pagina_atual']) {
						$('#paginacao').append('<strong>' + i + '</strong> ');
					} else {
						$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(' + i + ', \'' + sessionStorage['pedidos_regiao_ordem_atual'] + '\'); return false;">' + i + '</a> ');
					}
				}
				
				if (sessionStorage['pedidos_regiao_pagina_atual'] < total) {
					$('#paginacao').append('<a href="#" onclick="obter_clientes_regiao(' + (intval(sessionStorage['pedidos_regiao_pagina_atual']) + 1) + ', \'' + sessionStorage['pedidos_regiao_ordem_atual'] + '\'); return false;">&gt;</a> ');
				}
				
				if (sessionStorage['pedidos_regiao_pagina_atual'] <= total - 6) {
					$('#paginacao').append('&nbsp;&nbsp;<a href="#" onclick="obter_clientes_regiao(' + total + ', \'' + sessionStorage['pedidos_regiao_ordem_atual'] + '\'); return false;">Última Página</a> ');
				}
			}
		});
	});
}
