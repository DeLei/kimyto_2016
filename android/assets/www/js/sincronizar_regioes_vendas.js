/**
* Metódo:		sincronizar_modulo_por_registro
* 
* Descrição:	Função Utilizada para chamar o modulo que será sincronizado
* 
* Data:			31/03/2015
* Modificação:	31/03/2015
* 
* @access		public
* @param		string 		$modulo					- Modulo que sera sincronizado
* @param		string 		$descricao_modulo		- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_modulo			- Proximo modulo que ser asincronizado quando finalizar o primeiro
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_modulo_por_registro(modulo, descricao_modulo, proxima_modulo, parametros)
{
	
	// Salvando informações das sincronizações
	db.transaction(function(x) {
		x.executeSql('INSERT INTO sincronizacoes (timestamp, modulo, sucesso) VALUES (?, ?, ?)', [time(), modulo, 0]);
	});

	// Carregando
	$('#carregando').show();
	$('#btnCancelar').show();
	$('#btnSincronizar').hide();
	$('#carregando').find('span:first').html('Sincronizando ' + descricao_modulo + '…');

	//----------------------------------------------------------------------------------------------
	// Chamando o total de pacotes via ajax, e depois sincronizar dados do pacote
	//---------------------------------------------------------
	var ajax = $.ajax({
		url: config.ws_url + modulo + '/total',
		success: function(dados) {
			
			var total_pacotes = dados.total;
			
			//-----------
			
			db.transaction(function(x) {
				x.executeSql('SELECT * FROM ' + modulo, [],
				function(x, dados){
				
					// A tabela existe, entao apaga os dados e contia a sincronização			
					
					//1º Tentamos deletar os dados exportados das tabelas (Como somente as tabelas de exportação tem os campos "exportado", ira ocorrer um erro de sql)
					//2ª Se ocorrer um erro de SQL, vamos deletar os dados das tabelas que não existem os campos "exportado" 
					
					var where = '';
					
					
					x.executeSql('DELETE FROM ' + modulo + ' WHERE exportado = \'1\'' + where, [],
					function(){
						console.log('--==-- Apagando dados antigos do modulo ' + modulo + ' (EXPORTADO)');
						sincronizar_dados_por_registro(modulo, 1, total_pacotes, proxima_modulo, parametros);
					},
					function(){
						sincronizar_dados_por_registro(modulo, 1, total_pacotes, proxima_modulo, parametros);
					});
					
				},
				function(){ // Erro de SQL, irá continuar a sincronização normalmente
					sincronizar_dados_por_registro(modulo, 1, total_pacotes, proxima_modulo, parametros);
				});
			});
			
		}
	
	});
	
	//----------------------------------------------------------
	//----------------------------------------------------------------------------------------------
}



/**
* Metódo:		sincronizar_dados
* 
* Descrição:	Função Utilizada para salvar dados do web service no banco do navegador
* 
* Data:			17/09/2013
* Modificação:	17/09/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		array 		$parametros				- array para customização no ws
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function sincronizar_dados_por_registro(tabela, pacote, total_pacotes, proxima_funcao, parametros)
{

	var percentagem_pacotes = 100 / total_pacotes; //Calculo de porcentagem por pacote
	if(pacote == 1)
	{
		$( "#progressbar" ).progressbar({value: 0});
	}
	
	//Mensagem no console
	console.log('Sincronizando ' + tabela + ' | Pacote = ' + pacote + ' | Total de Pacotes = ' + total_pacotes);
	
	if(typeof(parametros) === 'object'){
		parametros.pacote = pacote;
		
	}else{
		parametros = {};
		parametros.pacote = pacote;
	}	
	
	var ajax = $.ajax({
		url: config.ws_url + tabela + '/exportar',
		data: parametros,
		success: function(dados) {
			
			// Criar Tabela
			criar_tabela(tabela, dados, true);
			
			// Salvar Dados
			if(verificar_dados(dados))
			{
				verificar_campos_por_registro(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, true);
			}
			else
			{
				verificar_campos_por_registro(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, false);				
			}			
		}
	});
	
	ajax.fail(function () {
		apprise('A sincronização não foi totalmente concluída. Você deseja tentar novamente?', {
			'verify': true,
			'textYes': 'Sim',
			'textNo': 'Não'
		}, function (dado) {
			if (dado)
			{
				window.location = 'sincronizar.html';
			}
			else
			{
				window.location = 'index.html';
			}
		});
	});
	
}



/**
* Metódo:		verificar_campos
* 
* Descrição: 		Verifica se os campos retornados pelo WS existem no banco de dados local
* 					se não existir gerar SQL ALTER e cria os campos todos para tipo TEXT.
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function verificar_campos_por_registro(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir)
{
	db.transaction(function(x) {
		
		var total_itens = dados.length;
		var item_atual 	= 0;
		var campos 		= [];
		
		//--------------------
		$.each(dados, function(i, objeto) {
			//--------------------
			$.each(objeto, function(indice, valor) {
				
				if(indice != 'rownum') 
				{
					campos.push(indice);
				}
				
			});
			//--------------------
			return false;
		});
		
		
		console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
		console.log('TABELA: '+ tabela);
		console.log('CAMPOS: '+ campos.join(', '));
		console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
		
		
		x.executeSql('SELECT ' + campos.join(', ') + ' FROM ' + tabela, [], function(){
			if(inserir)
			{
				inserir_dados_por_registro(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao);
			}
			else
			{
				finalizar_sincronizacao(tabela, proxima_funcao);
			}
			
		},
		function(xe, erro)
		{
			if(erro.code == 5)
			{
				var mensagem_erro = erro.message;
				var retorno = mensagem_erro.split(":");
				var campo = trim(retorno[1]);
				
				if(campo)
				{
					
					campo = str_replace(')','',campo);
					console.log(' -- ERRO COLUNA -- ');
					console.log('Coluna não existe: ' + campo);
					console.log('ALTER TABLE ' + tabela + ' ADD ' + campo + ' TEXT');
					x.executeSql('ALTER TABLE ' + tabela + ' ADD ' + campo + ' TEXT', [], function () {
						console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Tabela alterada com sucesso.');
						
						verificar_campos_por_registro(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao, inserir);

						
					}, function(xy, error){
						console.log('Tabela: ' + tabela + ' SQL: ' + campo + ' Não foi possível alterar: ' + error.message);
					});
					
					console.log(' -- FIM -- ');
					
				}
				else
				{
					console.log(erro);
				}
			}
			
		});

	});
}

/**
* Metódo:		inserir_dados
* 
* Descrição:	Função Utilizada para inserir os dados json na tabela SQLLITE
* 
* Data:			16/04/2013
* Modificação:	16/04/2013
* 
* @access		public
* @param		string 		$tabela					- Utilizado descrição da tabela do web service e do banco do navegador
* @param		json 		$dados					- dados retornados do json
* @param		number 		$pacote					- Número de pacote de dados que esta sendo sincronizado
* @param		number 		$total_pacotes			- Número total de pacotes de dados
* @param		number 		$percentagem_pacotes	- percentagem total de pacotes
* @param		string 		$proxima_funcao			- funcao que será executada quando finalizar
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function inserir_dados_por_registro(tabela, dados, pacote, total_pacotes, percentagem_pacotes, proxima_funcao)
{
	var total_itens = dados.length;
	var item_atual = 0;
	
	obter_pedidos_envio_regiao(function(regioes_completas){	
			console.log(JSON.stringify(regioes_completas));
		
		
			$.each(dados, function(i, objeto) {
				var campos 			= [];
				var interrogacoes  	= [];
				var valores		 	= [];
			
				$.each(objeto, function(indice, valor) {
					
					if(indice != 'rownum') //o campo rownum, vem automatico por conta do LIMIT no sqlsrv, entao por padrao, vamos remover esse retorno
					{
						campos.push(indice);
						interrogacoes.push('?');
						valores.push(valor);
					}
					
				});
				
				//Se regiao estiver concluida em com todos os pedido_venda enviados, será apagada para que seja atualizada
				if(in_array(objeto.codigo, regioes_completas)){
					console.log('regiao será atualizada atualizada');
					
					marcar_regiao_completa(objeto.codigo, function(){
						
						obter_regiao(objeto.codigo,function(dados_regiao){	
							
							var objeto_antigo = objeto.ativa  = 'C';
							if(objeto.codigo == dados_regiao.codigo && objeto.data_validade == dados_regiao.data_validade){ //((isset(dados_regiao.codigo)  && dados_regiao.ativa != 'C')  || identicos(dados_regiao, objeto_antigo)){
								
								console.log('ocorreu algum erro durante o envio dos pedido_venda registro não atualizado');
								
								item_atual++;
							
								var percontagem_item = percentagem_pacotes / total_itens;
								
								$( "#progressbar" ).progressbar({value: parseFloat($('#progressbar').attr('aria-valuenow')) + percontagem_item});
				
								if(total_itens == item_atual)
								{
									console.log('Pacote ' + pacote + ' foi finalizado!');
									
									$( "#progressbar" ).progressbar({value: round(percentagem_pacotes * pacote)});
								
									if(pacote < total_pacotes)
									{
										pacote++;
										sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao);
									}
									else
									{
										finalizar_sincronizacao(tabela, proxima_funcao);
									}
								}
							}else{
								
								console.log('Dados Regiao no tablet: ' + JSON.stringify(dados_regiao));
								db.transaction(function(x) {
									x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(a1, a2){
										item_atual++;

										var percontagem_item = percentagem_pacotes / total_itens;								
										$( "#progressbar" ).progressbar({value: parseFloat($('#progressbar').attr('aria-valuenow')) + percontagem_item});

										if(total_itens == item_atual)
										{
											console.log('Pacote ' + pacote + ' foi finalizado!');									
											$( "#progressbar" ).progressbar({value: round(percentagem_pacotes * pacote)});

											if(pacote < total_pacotes)
											{
												pacote++;
												sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao);
											}
											else
											{
												finalizar_sincronizacao(tabela, proxima_funcao);
											}
										}								
									},
									function(x, erro){
										console.log(' ----- ');
										console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
										console.log(erro);
										console.log('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ") VALUES ('" + valores.join("', '") + "');");
										console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
										console.log(' ----- ');
									});

								});
								
							
							}
						});
						
						
		
						
					});
					
				}else{
					
					obter_regiao(objeto.codigo,function(dados_regiao){
						
						if(isset(dados_regiao.codigo)){
							console.log('regiao_encontrada_nao_atualizada')
							
							item_atual++;
						
							var percontagem_item = percentagem_pacotes / total_itens;
							
							$( "#progressbar" ).progressbar({value: parseFloat($('#progressbar').attr('aria-valuenow')) + percontagem_item});
			
							if(total_itens == item_atual)
							{
								console.log('Pacote ' + pacote + ' foi finalizado!');
								
								$( "#progressbar" ).progressbar({value: round(percentagem_pacotes * pacote)});
							
								if(pacote < total_pacotes)
								{
									pacote++;
									sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao);
								}
								else
								{
									finalizar_sincronizacao(tabela, proxima_funcao);
								}
							}
						}else{
							console.log('Dados Regiao no tablet: ' + JSON.stringify(dados_regiao));
							db.transaction(function(x) {
								x.executeSql('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ') VALUES (' + interrogacoes.join(', ') + ')', valores, function(a1, a2){
									item_atual++;

									var percontagem_item = percentagem_pacotes / total_itens;								
									$( "#progressbar" ).progressbar({value: parseFloat($('#progressbar').attr('aria-valuenow')) + percontagem_item});

									if(total_itens == item_atual)
									{
										console.log('Pacote ' + pacote + ' foi finalizado!');									
										$( "#progressbar" ).progressbar({value: round(percentagem_pacotes * pacote)});

										if(pacote < total_pacotes)
										{
											pacote++;
											sincronizar_dados(tabela, pacote, total_pacotes, proxima_funcao);
										}
										else
										{
											finalizar_sincronizacao(tabela, proxima_funcao);
										}
									}								
								},
								function(x, erro){
									console.log(' ----- ');
									console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
									console.log(erro);
									console.log('INSERT INTO ' + tabela + ' (' + campos.join(', ') + ") VALUES ('" + valores.join("', '") + "');");
									console.log(' ----- ERRO NO INSERT DA TABELA ' + tabela + ' ----- ');
									console.log(' ----- ');
								});

							});
						}
					});
					
					
					
				}

				
			});
	});
}




//-- -- -- -- -- -- -- -- -- -- -- --

/**
* Metódo:		exportar_dados
* 
* Descrição:	Função Utilizada para enviar dados d eum modulo do DWFORÇA DE VENDAS para o WEBSERVICE
* 
* Data:			24/09/2013
* Modificação:	25/09/2013
* 
* @param		string 		$codigo_modulo					- ID do modulo (Campo Chave) para identificar a chave
* @param		string 		$modulo							- Modulo que sera sincronizado (nome da tabela no WS)
* @param		string 		$descricao_modulo				- Descrição do modulo que será exibida no carregando
* @param		string 		$proxima_funcao					- Proxima funcao que sera chamada quando finalziar a sincronização de um pacote
* @param		array 		$codigo							- Array utilizado para especificar quais dados serão enviados (EXP: vamos enviar somento o prospect "54321" => codigos[0] = '54321';)
*
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function exportar_dados_regioes_vendas(codigo_modulo, modulo, descricao_modulo, proxima_funcao, exportar_codigos)
{
	
	
	
	// Carregando
	$('#carregando').show();
	$('#btnCancelar').show();
	$('#btnSincronizar').hide();
	$('#carregando').find('span:first').html('Enviando ' + descricao_modulo + '…');
	$( "#progressbar" ).progressbar({value: 30});
	
	
	
	
	
	db.transaction(function(x) {
		
		var where = '';
		
		if(modulo != 'pedidos_pendentes') {
			where += ' WHERE exportado IS NULL';
		}

		if(exportar_codigos && exportar_codigos.length > 0) {
			if(where == '') {
				where += ' WHERE ';
			} else {
				where += ' AND ';
			}
			where += codigo_modulo + " IN ('" + implode('\', \'', exportar_codigos) + "')";
		}
		mensagem(where);
		x.executeSql('SELECT * FROM ' + modulo + where, [], function(x, dados) {
			var regiao_venda = new Regioes_model();
			
			
			
			/*
			
			var total_registros = dados.rows.length;
			
			console.log('Total = ' + total_registros);
			
			if (total_registros)
			{
				
				var retorno_json = new Array();
				for(registro = 0; registro < total_registros; registro++)
				{
					retorno_json[registro] = dados.rows.item(registro);
				}
				
				var ajax = $.ajax({
					url: config.ws_url + modulo + '/importar',
					type: 'POST',
					data: {
						retorno: json_encode(retorno_json)
					},
					success: function(dados) {
						if(dados.sucesso == 'ok')
						{
							//Apagando os dados com erro na sessao
							localStorage.setItem('erros_' + modulo, '');
							
							if(exportar_codigos && exportar_codigos.length > 0)
							{
								// Marcando os dados especificados no "exportar_codigos" como exportados
								db.transaction(function(x) {
									x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\')', [], function(){
										$( "#progressbar" ).progressbar({value: 100});
										finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
									});
								});
							}
							else
							{
								// Marcando todos os dados como exportados
								db.transaction(function(x) {
									x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL', [], function(){
										$( "#progressbar" ).progressbar({value: 100});
										finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
									});
								});
							}

						} else if(dados.erro) {
							
							var codigos_sessao = [];
							var codigos = [];
							$.each(dados.erro, function(i, codigo) {
								codigos.push("'" + codigo + "'");
								
								if(is_numeric(codigo))
								{
									codigos_sessao.push(codigo);
								}
								
							});
							
							//Gravando na sessao dos dados com erro
							localStorage.setItem('erros_' + modulo, codigos_sessao);
							
							
							var descricoes = [];
							$.each(dados.erro_descricao, function(i, descricao) {
								descricoes.push(descricao);
							});
						
							db.transaction(function(x) {
							
								//Precisamos marcar quais dados estao com erro, e depois apagar todos os dados que não estao com erro
								x.executeSql('UPDATE ' + modulo + ' SET erro = \'1\' WHERE ' + codigo_modulo + ' IN (' + codigos.join(', ') + ')');
								
								if(exportar_codigos && exportar_codigos.length > 0)
								{
									// Marcando os dados especificados no "exportar_codigos" como exportados, exceto os pedidos em que ocorreu erro
									x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE ' + codigo_modulo + ' IN (\'' + exportar_codigos.join('\', \'') + '\') AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
								}
								else
								{
									// Marcando todos os dados como exportados, exceto os pedidos em que ocorreu erro
									x.executeSql('UPDATE ' + modulo + ' SET exportado = \'1\' WHERE exportado IS NULL AND ' + codigo_modulo + ' NOT IN (' + codigos.join(', ') + ')');
								}
								
								
								apprise('<b>ERRO - ' + descricao_modulo + '!</b> <br />Não foi possível sincronizar:<b><br />(' + descricoes.join(', ') + ').</b><br />Entre em contato com TI da sua empresa.', {
									'textYes': 'OK'
								}, function (dado) {
								
									$( "#progressbar" ).progressbar({value: 100});
									finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
									
								});

								
							});
							
						}
					}
				});
				
				
			}
			else
			{
				db.transaction(function(x) {
					var where = '';
					
					if(modulo == 'pedidos_pendentes') {
						//Exclui somente os pedidos que já estão na SC5
						where = ' AND pedido_data_emissao <= "' + date('Ymd', strtotime('-15 days')) + '"';
					}
					
					x.executeSql('DELETE FROM ' + modulo + where);
					
					$( "#progressbar" ).progressbar({value: 100});
					finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
				});
			}
		*/
			
			
			
		},
		function (){ // Se a tabela não existir entrar nessa condição
			finalizar_sincronizacao('exportacao_' + modulo, proxima_funcao);
		});
		
	});
}
