if (typeof PhoneGap !== "undefined") {

	/**
	 * Empty constructor
	 */
	var Version = function() {};
	
	
	Version.prototype.getVersionCode = function(sucesso, falhou) 
	{	
		PhoneGap.exec(sucesso, falhou, "Version", "GetVersionCode",  new Array());		
	};
	
	PhoneGap.addConstructor(function() {
		
		if (!window.plugins) 
		{
			window.plugins = {};
		}
		
		window.plugins.version = new Version();
		
	});
}