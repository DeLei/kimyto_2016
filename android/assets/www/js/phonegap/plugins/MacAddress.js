if (typeof PhoneGap !== "undefined") {

	/**
	 * Empty constructor
	 */
	var MacAddressPlugin = function() {
	};
	
	
	MacAddressPlugin.prototype.getMacAddress = function(sucesso, falhou) 
	{	
		PhoneGap.exec(sucesso, falhou, "MacAddressPlugin", "getMacAddress",  new Array());		
	};
	
	PhoneGap.addConstructor(function() {
		
		if (!window.plugins) 
		{
			window.plugins = {};
		}
		
		window.plugins.macaddress = new MacAddressPlugin();
		
	});
}