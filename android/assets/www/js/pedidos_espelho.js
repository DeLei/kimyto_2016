$(document).ready(function() {
	
	var codigo_e_empresa 	= parse_url(location.href).fragment;
	array_codigo_e_empresa 	= codigo_e_empresa.split('|');
	var codigo_do_pedido 	= array_codigo_e_empresa[0];
	var codigo_da_empresa 	= array_codigo_e_empresa[1];
	var tipo_pedido 		= array_codigo_e_empresa[2];
	
	var pedido_exportado	= false;
	
	
	var tabela = 'pedidos_pendentes';
	var descricao = 'pedido';
	
	$('#enviar_pedido').click(function(e) {
		e.preventDefault();
		if(pedido_exportado) {
			confirmar('Este ' + descricao + ' já foi enviado anteriormente.<br/><br/>Deseja enviar novamente?', function () {
				$(this).dialog('close');
				
				location.href = "sincronizar.html?codigo_pedido=" + codigo_do_pedido;
				
				$("#confirmar_dialog").remove();
			});
		} else {
			confirmar('Deseja enviar este ' + descricao + '?', function () {
				$(this).dialog('close');
				
				location.href = "sincronizar.html?codigo_pedido=" + codigo_do_pedido;
				
				$("#confirmar_dialog").remove();
			});
		}
	});
	
	// Função utilizada para verificar se o cliente pertence ao representante
	function verificar_cliente_representante(cpfOuCnpj, callback) {
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM clientes WHERE cpf = ?', [cpfOuCnpj], function(x, dados) {
				if (dados.rows.length) {
					callback();
				} else {
					mensagem('O Pedido não pode ser copiado porque o cliente (' + formatar_cpfCNPJ(cpfOuCnpj) + ') não pertence ao representante.');
				}
			});
		});
	}
	
	// Obter DANFE pedido
	$('#btnObterDANFE').click(function(e) {
		e.preventDefault();
		
		if(window.navigator.onLine)
		{
			
			
			aguarde('Obtendo DANFE. Está ação poderá demorar alguns minutos.');
			
			obter_numero_nota_fiscal_pedido(codigo_do_pedido, codigo_da_empresa, function(dados_pedido){
			
				
				
				var codigo_empresa 	= dados_pedido.pedido_empresa ;
				var codigo_filial	= dados_pedido.pedido_filial;
				var codigo_NFE		= dados_pedido.pedido_codigo_nota_fiscal;
				
				
				
				
				console.log('Iniciada geração da DANFE');
				gerar_danfe(codigo_empresa, codigo_filial, codigo_NFE,function(nome_arquivo){
						var html = '<table cellspacing="5">';
						html += '<tr><td><h2> DANFE</h2></td></tr>';						
						html += '<tr><td><div class="aviso">Se você tentar abrir um arquivo PDF, ele será aberto normalmente pelo leitor padrão. Se isso não acontecer, você poderá definir o leitor para esse tipo de arquivo.</div></td></tr>';  
						html += '</table>';
						
						file_path = ('file:/'+localStorage.getItem('caminho_local')+nome_arquivo+'.pdf' );
						//console.log(file_path);
						
						//(window.open(file_path, '_system', 'location=no'));
					//	alert('A');
						
					//	window.open((file_path), '_system', 'location=no');
						
						comando = "abrirPDF('"+file_path+"')";
						
						
						
						console.log(comando);
					//	eval(comando);
					//	alert('B');
						$("#aguarge_dialog").remove();
						//$.colorbox({ html: html, maxWidth: '995px' });
						//exibirMensagem('dialog_catalogos', html, "window.open(encodeURI("+file_path+"), '_system', 'location=yes')", 'Detalhes');
						//exibirMensagem('dialog_catalogos', html, "window.open('"+file_path+"', '_system')", 'Detalhes');
						exibirMensagem('dialog_catalogos', html, comando, 'Detalhes');
					
				});
			
			});
		}
		else
		{
			mensagem('Não é possível obter a DANFE. Sem conexão à internet.');
		}
		
		
	});
	
	
	
	
	
	
	// Copiar pedido
	/*
	
	$('#copiar').click(function(e) {
		e.preventDefault();
		
		var copiar_pedido_orcamento = function() {
			confirmar('Deseja copiar este ' + descricao + '?', 
				function () {
					$(this).dialog('close');
					
					//Copiar pedido processado
					if(!tipo_pedido) {
						copiar_pedido_processado(codigo_do_pedido, codigo_da_empresa);
					} else { //Copiar pedido pendente
						copiar_pedido(codigo_do_pedido, codigo_da_empresa, false, tabela);
					}
					
					$("#confirmar_dialog").remove();
				}
			);
		}
		
		verificar_cliente_representante($('#cpfOuCnpj').val(), copiar_pedido_orcamento);
	});
	
	*/
	
	// Editar pedido
	$('#editar').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja editar este ' + descricao + '?', function () {
			$(this).dialog('close');
			
			copiar_pedido(codigo_do_pedido, codigo_da_empresa, true, tabela);
			
			$("#confirmar_dialog").remove();
			
		});
	});
	
	// Editar pedido
	$('#excluir').click(function(e) {
		e.preventDefault();
		
		confirmar('Deseja excluir este ' + descricao + '?', function () {
			$(this).dialog('close');
			
			db.transaction(function(x) {
				x.executeSql('DELETE FROM ' + tabela + ' WHERE pedido_id_pedido = "' + codigo_do_pedido + '"', [], function(){
					if(tipo_pedido == 'O') {
						// Apagando pedido da sessao
						location.href="orcamentos_aguardando.html";
					} else {
						// Apagando pedido da sessao
						location.href="pedidos_aguardando.html";
					}
				});
			});
			
			$("#confirmar_dialog").remove();
		});
	});
	
	
	// Se for "P" (Pedidos Pendentes) entra no IF, se não entra no IF dos Pedidos Processados
	if(tipo_pedido == 'P' || tipo_pedido == 'O') {
	
		// Obter Pedido Pendentes
		db.transaction(function(x) {
			x.executeSql('SELECT clientes.codigo_icms_st_cliente, ' + tabela + '.* FROM ' + tabela + ' JOIN clientes ON clientes.codigo = ' + tabela +'.cliente_codigo AND clientes.loja = ' + tabela +'.cliente_loja  WHERE pedido_id_pedido = ? AND pedido_filial = ? ORDER BY CAST(classe_codigo_classe AS INTEGER)', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
				if (dados.rows.length) {
					var pedido = dados.rows.item(0);
					
					if(!pedido.exportado) {
						$('#editar').show();
						$('#excluir').show();
					} else {
						pedido_exportado = true;
					}
					/*$('#enviar_pedido').show();*/
					
					// informações gerais
					$('.codigo_e_representante').text(info.cod_rep + ' / ' + info.nome_representante);
					$('.numero_pedido').text(pedido.pedido_id_pedido);
					$('.filial').text(pedido.pedido_filial);
					$('.data_emissao').text(protheus_data2data_normal(pedido.pedido_data_emissao));
					$('.condicao_pagamento').text(pedido.forma_pagamento_descricao);
					$('.tabela_preco').text(pedido.pedido_tabela_precos);
					
					/**
					 * Dados do cliente
					 */
					if(!isEmpty(pedido.cliente_codigo)) {
						$('.nome_cliente').text(pedido.cliente_nome);
						$('.codigo_loja_cliente').text(pedido.cliente_codigo + ' / ' + pedido.cliente_loja);
						$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.cliente_cpf));
						$('#cpfOuCnpj').val(pedido.cliente_cpf);
						$('.contato').text(pedido.cliente_pessoa_contato ? pedido.cliente_pessoa_contato : 'N/A');
						
						/*Tratamento telefone*/
						var telefone = '';
						if(pedido.cliente_ddd) {
							telefone = pedido.cliente_ddd + pedido.cliente_telefone;
						} else {
							if(pedido.cliente_telefone) {
								telefone = pedido.cliente_telefone;
							}						
						}
						/*Tratamento telefone*/
						$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
						$('.email').text(pedido.cliente_email ? pedido.cliente_email : 'N/A');
						$('.cidade').text(pedido.cliente_cidade ? pedido.cliente_cidade : 'N/A');
						$('.estado').text(pedido.cliente_estado ? pedido.cliente_estado : 'N/A');
						$('.bairro').text(pedido.cliente_bairro ? pedido.cliente_bairro : 'N/A');
						$('.endereco').text(pedido.cliente_endereco ? pedido.cliente_endereco : 'N/A');
					}
					
					// Informações do Pedido
					$('.tipo_frete').text(obter_tipo_frete(pedido.pedido_tipo_frete));
					$('.pedido_cliente').text(pedido.pedido_pedido_cliente ? pedido.pedido_pedido_cliente : 'N/A');
					$('.observacao_comercial').text(pedido.pedido_observacao_comercial ? pedido.pedido_observacao_comercial : 'N/A');
					
					/**
					 * Cliente de Entrega
					 */
					$('.cliente_entrega').empty();
					$('.cliente_entrega').append('<b>Cliente:</b> ' + pedido.cliente_nome + '<br />');
					$('.cliente_entrega').append('<b>Endereço:</b> ' + pedido.cliente_endereco + '<br />');
					$('.cliente_entrega').append('<b>Bairro:</b> ' + pedido.cliente_bairro + '<br />');
					$('.cliente_entrega').append('<b>CEP:</b> ' + formatar_cep(pedido.cliente_cep) + '<br />');
					$('.cliente_entrega').append('<b>Cidade:</b> ' + pedido.cliente_cidade + '<br />');
					$('.cliente_entrega').append('<b>Estado:</b> ' + pedido.cliente_estado + '<br />');
					
					/**
					 * Produtos
					 */
					var total_itens = dados.rows.length;
					
					if(total_itens > 0) {
						
						var total = 0;
						var total_com_impostos = 0;
						var html = '';
						for(i = 0; i < total_itens; i ++) {
							var item_pedido = dados.rows.item(i);
							
							var quantidade					= parseInt(item_pedido.pedido_quantidade);
							var preco_unitario				= number_format(item_pedido.pedido_preco_unitario, 2);
							var preco_venda					= number_format(item_pedido.pedido_preco_venda, 2);
							var preco_venda_com_impostos 	= calcular_impostos_produtos_espelho(item_pedido.empresa, pedido.pedido_filial, pedido.codigo_icms_st_cliente, preco_venda, pedido.forma_pagamento_percentual_desconto);
							html += '<tr>';
								html += '<td align="center">' +  (i + 1) + '</td>';
								html += '<td>' + item_pedido.pedido_codigo_produto + ' - ' + item_pedido.produto_descricao + '</td>';
								html += '<td>' + item_pedido.pedido_derivacao + ' - ' + item_pedido.derivacao_descricao_derivacao + ' <br/>(' + item_pedido.classe_descricao_classe + ')</td>';
								html += '<td align="center">' + quantidade + '</td>';
								html += '<td align="right">' + number_format(preco_unitario, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(preco_venda, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(preco_venda_com_impostos, 3, ',', '.') + '</td>';
								html += '<td align="right">' + number_format(quantidade * preco_venda, 3, ',', '.') +'</td>';
								html += '<td align="right">' + number_format(quantidade * preco_venda_com_impostos, 3, ',', '.') +'</td>';
							html += '</tr>';
							
							total += preco_venda * quantidade;
							total_com_impostos += quantidade * preco_venda_com_impostos;
						}
						
						html += '<tr class="novo_grid_rodape">';
							html += '<td colspan="7">TOTAL DO PEDIDO</td>';
							html += '<td align="right">' + number_format(total, 2, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(total_com_impostos, 2, ',', '.') + '</td>';
						html += '</tr>';
						$('.itens_pendentes').html(html);
						$('#itens_pendentes_tabela').show();
					}
					
					alterarCabecalhoListagem('#itens_pendentes_tabela');
					alterarCabecalhoTabelaConteudo();
				}
			});
		});
		
	} else {
	
		
		
		
		//$('#copiar').hide();
		
		// Obter Pedido Processado
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ? ORDER BY ip_codigo_item DESC', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
				if (dados.rows.length) {
					
					$('#atencao_impostos').hide();
					
					var pedido = dados.rows.item(0);
					
					
					//Projeto 
					//E150/2015 - Danfe
					console.log(JSON.stringify(pedido));
					
						if(pedido.status == "faturado"  &&  !is_null(pedido.pedido_codigo_nota_fiscal)  ){
							$("#obterDANFE").show();
						}
					
					
					
					// informações gerais
					$('.codigo_e_representante').text(info.cod_rep + ' / ' + info.nome_representante);
					$('.numero_pedido').text(pedido.pedido_codigo);
					$('.filial').text(pedido.pedido_filial);
					$('.data_emissao').text(protheus_data2data_normal(pedido.pedido_data_emissao));
					$('.condicao_pagamento').text(pedido.forma_pagamento_descricao);
					$('.tabela_preco').text((pedido.pedido_tabela_precos ? pedido.pedido_tabela_precos : 'N/A'));
					
					// Informações do Cliente
					$('.nome_cliente').text(pedido.cliente_nome);
					$('.codigo_loja_cliente').text(pedido.cliente_codigo + ' / ' + pedido.cliente_loja);
					$('.cpf_cliente').text(formatar_cpfCNPJ(pedido.cliente_cpf));
					$('#cpfOuCnpj').val(pedido.cliente_cpf);
					$('.contato').text(pedido.cliente_pessoa_contato ? pedido.cliente_pessoa_contato : 'N/A');
					
					/*Tratamento telefone*/
					var telefone = '';
					if(pedido.cliente_ddd) {
						telefone = pedido.cliente_ddd + pedido.cliente_telefone;
					} else {
						if(pedido.cliente_telefone) {
							telefone = pedido.cliente_telefone;
						}						
					}						
					$('.telefone').text(telefone ? formatar_telefone(telefone) : 'N/A');
					$('.email').text(pedido.cliente_email ? pedido.cliente_email : 'N/A');
					$('.cidade').text(pedido.cliente_cidade ? pedido.cliente_cidade : 'N/A');
					$('.estado').text(pedido.cliente_estado ? pedido.cliente_estado : 'N/A');
					$('.estado').text(pedido.cliente_bairro ? pedido.cliente_bairro : 'N/A');
					$('.endereco').text(pedido.cliente_endereco ? pedido.cliente_endereco : 'N/A');
					
					// Informações do Pedido
					$('.pedido_cliente').text(pedido.ip_pedido_cliente ? pedido.ip_pedido_cliente : 'N/A');
					$('.tipo_frete').text(obter_tipo_frete(pedido.pedido_tipo_frete));
					
					/**
					 * Cliente de Entrega
					 */
					$('.cliente_entrega').empty();
					$('.cliente_entrega').append('<b>Cliente:</b> ' + pedido.cliente_nome + '<br />');
					$('.cliente_entrega').append('<b>Endereço:</b> ' + pedido.cliente_endereco + '<br />');
					$('.cliente_entrega').append('<b>Bairro:</b> ' + pedido.cliente_bairro + '<br />');
					$('.cliente_entrega').append('<b>CEP:</b> ' + formatar_cep(pedido.cliente_cep) + '<br />');
					$('.cliente_entrega').append('<b>Cidade:</b> ' + pedido.cliente_cidade + '<br />');
					$('.cliente_entrega').append('<b>Estado:</b> ' + pedido.cliente_estado + '<br />');
					
					var total_itens = dados.rows.length;
					
					if(total_itens > 0)
					{
						// Declarando variaveis totais
						var total_preco_venda 			= 0;
						var total_quantidade 			= 0;
						var total_quantidade_faturada 	= 0;
						var total			 			= 0;
						var total_faturado		 		= 0;
						var total_ipi   				= 0;
						var total_com_ipi		 		= 0;
						var total_desconto		 		= 0;
						var total_preco_com_desconto	= 0;
						var total_geral		 			= 0;
						
						var numero_item = 1;
						
						for(i = 0; i < total_itens; i ++)
						{
							var item_pedido = dados.rows.item(i);
							
							var preco_unitario = item_pedido.ip_preco_unitario;
							if(pedido.pedido_desconto1 > 0)
							{
								preco_unitario = aplicar_descontos_cabecalho(item_pedido.ip_preco_unitario, pedido.pedido_desconto1, '0');
							}
							
							var itens_pedido = [];
							var valor_ipi = parseFloat((item_pedido.inf_ipi ? item_pedido.inf_ipi : 0));
							var valor_com_desconto = item_pedido.ip_valor_total_item - item_pedido.ip_total_desconto_item;
							
							var html = '';
							html += '<td align="center">' + (numero_item++)  + '</td>';
							html += '<td align="center">' + item_pedido.ip_codigo_produto + '</td>';
							html += '<td>' + item_pedido.ip_descricao_produto + '</td>';
							html += '<td>' + (item_pedido.ip_pedido_cliente ? item_pedido.ip_pedido_cliente : 'N/A') + '</td>';
							html += '<td align="right">' + number_format(preco_unitario, 3, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto, 3, ',', '.') + '</td>';
							html += '<td align="center">' + number_format(preco_unitario - (item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto), 3, ',', '.') + '</td>';
							html += '<td align="center">' + item_pedido.ip_quantidade_vendida_produto + '</td>';
							html += '<td align="right">' + number_format(item_pedido.ip_valor_total_item, 3, ',', '.') + '</td>';
							
							html += '<td align="center">' + item_pedido.ip_quantidade_faturada_produto + '</td>';
							
							html += '<td align="right">' + number_format(item_pedido.ip_preco_produto * item_pedido.ip_quantidade_faturada_produto, 3, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(parseFloat(valor_ipi), 3, ',', '.') + '</td>';
							html += '<td align="right">' + number_format(valor_ipi + parseFloat(item_pedido.ip_valor_total_item), 3, ',', '.') + '</td>';

							
							$('.itens_processados').parent('table').show();
							$('.itens_processados').append('<tr>' + html + '</tr>');
							
							// Somando Totais
							total_preco_venda 			+= parseFloat(preco_unitario);
							total_preco_com_desconto 	+= parseFloat(preco_unitario - (item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto));
							total_quantidade 			+= parseFloat(item_pedido.ip_quantidade_vendida_produto);
							total_quantidade_faturada	+= parseFloat(item_pedido.ip_quantidade_faturada_produto);
							total						+= parseFloat(item_pedido.ip_valor_total_item);
							total_faturado				+= parseFloat(item_pedido.ip_preco_produto * item_pedido.ip_quantidade_faturada_produto);
							total_ipi					+= parseFloat(valor_ipi);
							total_com_ipi				+= valor_ipi + parseFloat(item_pedido.ip_valor_total_item);
							total_desconto				+= parseFloat(item_pedido.ip_total_desconto_item / item_pedido.ip_quantidade_vendida_produto);
							total_geral					+= (valor_com_desconto * (item_pedido.produto_ipi / 100)) + parseFloat(valor_com_desconto);
						}
						
						//Exibindo Totais
						var totais_html = '';
						totais_html += '<td colspan="4">TOTAIS DO PEDIDO</td>';
						totais_html += '<td align="right">' + number_format(total_preco_venda, 3, ',', '.') + '</td>';
						totais_html += '<td align="right">' + number_format(total_desconto, 3, ',', '.') + '</td>';
						totais_html += '<td align="right">' + number_format(total_preco_com_desconto, 3, ',', '.') + '</td>';
						totais_html += '<td align="center">' + total_quantidade + '</td>';
						totais_html += '<td align="right">' + number_format(total, 3, ',', '.') + '</td>';
						totais_html += '<td align="center">' + total_quantidade_faturada + '</td>';
						
						totais_html += '<td align="right">' + number_format(total_faturado, 3, ',', '.') + '</td>';
						totais_html += '<td align="right">' + number_format(total_ipi, 3, ',', '.') + '</td>';
						totais_html += '<td align="right">' + number_format(total_com_ipi, 3, ',', '.') + '</td>';
						
						
						$('.itens_processados').append('<tr class="novo_grid_rodape">' + totais_html + '</tr>');
						
					}
					
					alterarCabecalhoListagem('#itens_processados_tabela');
					alterarCabecalhoTabelaConteudo();
					
					
					// informações gerais
					
					$('.oc').text(pedido.ordem_de_compra);
					$('.numero').text(pedido.exportado ? pedido.codigo : 'N/D');
					$('.tp_frete').text(strtoupper(pedido.tipo_de_frete));
					$('.emissao').text(date('d/m/Y', pedido.timestamp));
					$('.forma_pgto').text(pedido.descricao_da_forma_de_pagamento);
					$('.observacao').text(pedido.observacao ? pedido.observacao : 'N/D');
					
					var dt_ent = pedido.data_de_entrega;
					
					if (dt_ent)
					{
						var dia = substr(dt_ent, 6, 2);
						var mes = substr(dt_ent, 4, 2);
						var ano = substr(dt_ent, 0, 4);
						
						$('.dt_ent').text(dia + '/' + mes + '/' + ano);
					}
					else
					{
						$('.dt_ent').text('N/D');
					}
					
					// prospect/cliente
					
					if (pedido.id_pro)
					{
						$('#cli').hide();
						$('#pro').show();
						
						x.executeSql(
							'SELECT * FROM prospects WHERE id = ?', [pedido.id_pro], function(x, dados) {
								if (dados.rows.length)
								{
									var prospect = dados.rows.item(0);
	
									$('.rs_cli').text(prospect.nome);
									$('.cnpj_cli').text(prospect.cgc);
									$('.tel_cli').text(prospect.telefone);
									$('.email_cli').text(prospect.email);
									$('.end_cli').text(prospect.endereco);
									$('.bairro_cep_cli').text(prospect.bairro + (prospect.cep ? '/' + prospect.cep : ''));
									$('.contato_cli').text(prospect.contato);
									$('.mun_est_cli').text(prospect.municipio + (prospect.estado ? '/' + prospect.estado : ''));
								}
							}
						);
					}
					else
					{
						$('#cli').show();
						$('#pro').hide();
						
						x.executeSql(
							'SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [pedido.codigo_do_cliente, pedido.loja_do_cliente], function(x, dados) {
								if (dados.rows.length)
								{
									var cliente = dados.rows.item(0);
	
									$('.rs_cli').text(cliente.razao_social);
									$('.cnpj_cli').text(cliente.cnpj);
									$('.tel_cli').text(cliente.telefone);
									$('.cod_cli').text(cliente.codigo);
									$('.lj_cli').text(cliente.loja);
									$('.email_cli').text(cliente.email);
									$('.end_cli').text(cliente.endereco);
									$('.bairro_cep_cli').text(cliente.bairro + (cliente.cep ? '/' + cliente.cep : ''));
									$('.contato_cli').text(cliente.pessoa_de_contato);
									$('.mun_est_cli').text(cliente.municipio + (cliente.estado ? '/' + cliente.estado : ''));
								}
							}
						);
					}
					
					// produtos
					
					x.executeSql(
						'SELECT * FROM pedidos WHERE codigo = ? ORDER BY timestamp ASC', [pedido.codigo], function(x, dados) {
							if (dados.rows.length)
							{
								var qtd_ven = 0;
								var qtd_fat = 0;
								
								for (i = dados.rows.length - 1; i >= 0; i--)
								{
									var dado = dados.rows.item(i);
									
									qtd_ven += dado.quantidade;
									qtd_fat += dado.quantidade_faturada;
									
									$('.itens').append('<tr><td style="text-align: center;">' + str_pad(i + 1, 2, 0, 'STR_PAD_LEFT') + '</td><td>' + dado.codigo_do_produto + '</td><td>' + dado.descricao_do_produto + '</td><td class="nao_exibir_impressao">' + dado.um_do_produto + '</td><td class="nao_exibir_impressao">' + (dado.descricao_da_tabela_de_preco ? dado.descricao_da_tabela_de_preco : dado.codigo_da_tabela_de_preco) + '</td><td style="text-align: right;">' + number_format(dado.preco, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.preco - round(dado.preco * (dado.desconto / 100), 2), 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.quantidade, 0, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.quantidade_faturada, 0, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.total_sem_ipi, 2, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.ipi, 2, ',', '.') + '</td><td class="nao_exibir_impressao" style="text-align: right;">' + number_format(dado.total_com_ipi, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.valor_desconto, 2, ',', '.') + '</td><td style="text-align: right;">' + number_format(dado.total_final, 2, ',', '.') + '</td></tr>');
								}

								$('.quantidade_vendida').text(number_format(qtd_ven, 0, ',', '.'));
								$('.quantidade_faturada').text(number_format(qtd_fat, 0, ',', '.'));
								$('.valor_total_desconto').text(number_format(dado.desconto_total_pedido, 2, ',', '.'));
								$('.valor_total_do_pedido').text(number_format(dado.valor_total_do_pedido, 2, ',', '.'));
							}
						}
					);
				}
				else
				{
					window.location = 'index.html';
				}
			});
		});
	}
		
});


function copiar_pedido(codigo_do_pedido, codigo_da_empresa, editar, tabela, converter_pedido_orcamento) {
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM ' + tabela + ' WHERE pedido_id_pedido = ? AND pedido_filial = ?', [codigo_do_pedido, codigo_da_empresa], function(x, dados) {
			
			var dado = dados.rows.item(0);
			
			
			obter_cliente_pedido(dado, function(pedido){
				
				
				dado = pedido;
			
				//Cabeçalho pedido
				var pedido_copiado = [];
				
				pedido_copiado['dados_cliente']					= dado.cliente;
				
				
				pedido_copiado['editar'] 						= editar;
				pedido_copiado['id_pedido'] 					= codigo_do_pedido;
				
				pedido_copiado['filial']						= dado.pedido_filial;
				pedido_copiado['empresa'] 						= dado.empresa;
				pedido_copiado['cliente_prospect'] = 'C';
				pedido_copiado['codigo_cliente'] 				= dado.pedido_codigo_cliente;
				pedido_copiado['loja_cliente']					= dado.pedido_loja_cliente;
				pedido_copiado['codigo_icms_st_cliente']		= dado.cliente.codigo_icms_st_cliente;
				pedido_copiado['descricao_cliente']				= dado.pedido_codigo_cliente + '/' + dado.pedido_loja_cliente + ' - ' + dado.cliente_nome + ' - ' + dado.cliente_cpf;
				pedido_copiado['codigo_cliente_entrega']		= dado.pedido_cliente_entrega;
				pedido_copiado['loja_cliente_entrega']			= dado.pedido_loja_entrega;
				pedido_copiado['descricao_cliente_entrega']		= dado.pedido_codigo_cliente + '/' + dado.pedido_loja_cliente + ' - ' + dado.cliente_nome + ' - ' + dado.cliente_cpf;
				
				
				pedido_copiado['condicao_pagamento']			= dado.pedido_condicao_pagamento;
				pedido_copiado['observacao_comercial']			= utf8_decode(dado.pedido_observacao_comercial);
				pedido_copiado['pedido_cliente']				= dado.pedido_pedido_cliente;
				pedido_copiado['tabela_precos']					= dado.pedido_tabela_precos;
				
				pedido_copiado['produtos'] = new Array();
			
				for (var i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					if(!pedido_copiado['produtos'][dado.pedido_codigo_produto]) {
						pedido_copiado['produtos'][dado.pedido_codigo_produto] = new Array();
					}
					
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao] = new Array();
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['codigo_produto']			= dado.pedido_codigo_produto;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['descricao_produto'] 		= dado.produto_descricao;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['codigo_derivacao']		= dado.pedido_derivacao;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['descricao_derivacao']	= dado.derivacao_descricao_derivacao;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['preco_unitario']			= dado.pedido_preco_unitario;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['preco_venda']			= dado.pedido_preco_venda;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['quantidade']				= parseInt(dado.pedido_quantidade);
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['codigo_classe']			= dado.classe_codigo_classe;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['descricao_classe']		= dado.classe_descricao_classe;
					pedido_copiado['produtos'][dado.pedido_codigo_produto][dado.pedido_derivacao]['aplicada_promocao']		= dado.pedido_aplicada_promocao;
					
				}
			
				sessionStorage['sessao_tipo'] = 'sessao_pedido';
				sessionStorage['sessao_pedido'] = serialize(pedido_copiado);
				location.href="pedidos_adicionar.html";
				
			 	
				
			});
			
			
			
		});
	});
}


function copiar_pedido_processado(codigo_do_pedido, codigo_da_empresa)
{

	db.transaction(function(x) {
		x.executeSql(
			'SELECT * FROM pedidos_processados WHERE pedido_codigo = ? AND pedido_filial = ?', [codigo_do_pedido, codigo_da_empresa], 
			function(x, dados) 
			{
				
				var dado = dados.rows.item(0);
				
				//Cabeçalho pedido
				var pedido_copiado = [];
				
				pedido_copiado['codigo_cliente'] 				= dado.cliente_codigo;
				pedido_copiado['loja_cliente']					= dado.cliente_loja;
				pedido_copiado['codigo_cliente_entrega']		= dado.pedido_cliente_entrega;
				pedido_copiado['loja_cliente_entrega']			= dado.pedido_loja_entrega;
				pedido_copiado['codigo_transportadora']			= dado.transportadora_codigo;
				pedido_copiado['condicao_pagamento']			= dado.cliente_condicao_pagamento;
				pedido_copiado['data_entrega']					= (dado.pedido_data_entrega ? protheus_data2data_normal(dado.pedido_data_entrega) : '');
				pedido_copiado['desconto_cliente']				= dado.cliente_desconto;
				pedido_copiado['descricao_cliente']				= dado.cliente_nome;
				pedido_copiado['descricao_cliente_entrega']		= dado.cliente_codigo + '/' + dado.cliente_loja + ' - ' + dado.cliente_nome + ' - ' + dado.cliente_cpf;
				pedido_copiado['filial']						= dado.pedido_filial;
				pedido_copiado['mennota']						= utf8_decode(dado.pedido_mensagem);
				pedido_copiado['pedido_cliente']				= dado.ip_pedido_cliente;
				pedido_copiado['tabela_precos']					= dado.pedido_tabela_precos;
				pedido_copiado['tipo_frete']					= dado.cliente_tipo_frete;
				pedido_copiado['tipo_pedido']					= 'N';

				pedido_copiado['produtos'] = new Array();
			
				for (var i = 0; i < dados.rows.length; i++)
				{
					var dado = dados.rows.item(i);
					
					
					pedido_copiado['produtos'][dado.ip_codigo_produto] = new Array();
					
					pedido_copiado['produtos'][dado.ip_codigo_produto]['codigo'] 				= dado.ip_codigo_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['desconto'] 				= 0;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['descricao'] 			= dado.ip_descricao_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['ipi'] 					= dado.produto_ipi;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['preco_unitario'] 		= dado.ip_preco_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['preco_venda'] 			= dado.ip_preco_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['quantidade'] 			= dado.ip_quantidade_vendida_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['unidade_medida'] 		= dado.ip_unidade_medida_produto;
					pedido_copiado['produtos'][dado.ip_codigo_produto]['local'] 				= dado.ip_local;
				}
				
				sessionStorage['sessao_pedido'] = serialize(pedido_copiado);
				
				window.location = "pedidos_adicionar.html";  

			
			}
		)
	});
}


/**
* Metódo:		aplicar_descontos_cabecalho
* 
* Descrição:	Função Utilizada para aplicar os descontos do cabeçalho (Desconto do Cliente, Regra de Desconto)
* 
* Data:			23/10/2013
* Modificação:	23/10/2013
* 
* @access		public
* @param		string 					preco_produto		- Preço do Produto
* @param		string 					desconto_cliente	- Desconto do cliente
* @param		string 					regra_desconto		- Desconto da regra
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function aplicar_descontos_cabecalho(preco_produto, desconto_cliente, regra_desconto)
{
	// Aplicando Desconto do "Cliente" no preço do produto
	if(desconto_cliente > 0)
	{
		var preco = preco_produto - (preco_produto * (desconto_cliente / 100));
	}
	else
	{
		var preco = preco_produto;
	}

	// Aplicando Desconto da "Regra de desconto" no preço do produto
	if(regra_desconto  > 0)
	{
		var preco = preco - (preco * (regra_desconto / 100));
	}
	
	return preco;
}

var calcular_impostos_produtos_espelho = function(empresa, filial, codigo_icms_st_cliente, preco_unitario_venda_produto, percentual_desconto) {
	
	
	preco_unitario_venda_produto = preco_unitario_venda_produto  - (preco_unitario_venda_produto * percentual_desconto /100);
	
	if (empresa == '1') {// Empresa Kimyto

		if (filial == '1') {//Filial SC
			
			if (codigo_icms_st_cliente == '30') {
				var modificador = 1.11205;
				if(percentual_desconto == 3){
					modificador = 1.113643;
					return preco_unitario_venda_produto * modificador ;
				}
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' X Impostos: '+modificador );
				
				return preco_unitario_venda_produto * modificador ;
			
			}
			if (codigo_icms_st_cliente == '00') {
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' X Impostos: '+1.05 );
				
				return preco_unitario_venda_produto * 1.05;
			
			}
			if (codigo_icms_st_cliente == '') {
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' X Impostos: '+1.05 );
				
				return preco_unitario_venda_produto * 1.05;
			
			}
			if (codigo_icms_st_cliente == '70') {
				var modificador = 0.845;
				if(percentual_desconto == 3){
					modificador = 0.8423669;
					return preco_unitario_venda_produto / modificador ;
				}
				
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' /  Impostos: '+ modificador );
				return preco_unitario_venda_produto / modificador ;
			}
		}

		if (filial == '2') {//Filial PR
			
			return preco_unitario_venda_produto / 0.874;
		}

	}

	return preco_unitario_venda_produto;
}
