/** Aplica promoçao so produto  
 * 
 */



function aplicar_promocao(codigo_produto, codigo_derivacao, indice){
	var preco_unitario 		= $("input[name='preco_unitario[" + codigo_produto + "][" + codigo_derivacao + "]']").val()	 
	var preco_venda 		= $("input[name='preco_venda[" + codigo_produto + "][" + codigo_derivacao + "]']").val();
	var preco_promocional 	= $("input[name='preco_unitario_promocional[" + codigo_produto + "][" + codigo_derivacao + "]']").val();  
	$('#produto_selecao_form_'+indice).show();
	$('#produto_selecao_simple_'+indice).hide();
	
	
	
	/*Informa que promoção está sendo aplicada*/
	$("input[name='quantidade[" + codigo_produto + "][" + codigo_derivacao + "]']").data('aplicada_promocao', true);
	$("span.promocao[data-codigo_produto="+codigo_produto+"][data-derivacao_codigo_derivacao=" + codigo_derivacao + "]").addClass('ativa');
	
	
	
	/*Altera o preço de venda*/
	$("input[name='preco_venda[" + codigo_produto + "][" + codigo_derivacao + "]']").val(preco_promocional);
	
	$("input[name='quantidade[" + codigo_produto + "][" + codigo_derivacao + "]']").keyup();
}


function remover_promocao(codigo_produto, codigo_derivacao, indice){
	
	
	var preco_unitario 		= $("input[name='preco_unitario[" + codigo_produto + "][" + codigo_derivacao + "]']").val()	 
	var preco_venda 		= $("input[name='preco_venda[" + codigo_produto + "][" + codigo_derivacao + "]']").val();
	var preco_promocional 	= $("input[name='preco_unitario_promocional[" + codigo_produto + "][" + codigo_derivacao + "]']").val();  
	
	
	/*Informa que promoção está sendo aplicada*/
	$("input[name='quantidade[" + codigo_produto + "][" + codigo_derivacao + "]']").data('aplicada_promocao', false);
	$("span.promocao[data-codigo_produto="+codigo_produto+"][data-derivacao_codigo_derivacao=" + codigo_derivacao + "]").removeClass('ativa');
	$('#produto_selecao_form_'+indice).show();
	$('#produto_selecao_simple_'+indice).hide();
	
	/*Altera o preço de venda*/
	$("input[name='preco_venda[" + codigo_produto + "][" + codigo_derivacao + "]']").val(preco_unitario);
	
	$("input[name='quantidade[" + codigo_produto + "][" + codigo_derivacao + "]']").keyup();
}




/***/


function obter_dados_produto(codigo_produto, derivacao_codigo_derivacao, tabela_precos, callback){
	
	
	
	var sql =	"SELECT "+ 
	"	produtos.* , " +
	"	produtos_promocao.tb_promocional as produto_promocional, " +
	"	produtos_promocao.ptp_preco		 as produto_promocional_preco "	 +
	"FROM produtos " +
	"LEFT JOIN produtos AS produtos_promocao "  +
	"	ON ( " +
	"		produtos_promocao.tb_promocional = 'S' " +
	"		AND produtos_promocao.derivacao_classe_codigo_classe = produtos.derivacao_classe_codigo_classe "  +
	"		AND produtos_promocao.tb_filial = produtos.tb_filial "  +
	"		AND produtos_promocao.produto_codigo = produtos.produto_codigo  "  +
	") " +
	" WHERE     produtos.tb_codigo = '"+tabela_precos+"' AND produtos.derivacao_classe_codigo_classe = '"+intval(derivacao_codigo_derivacao)+"'  AND produtos.produto_codigo = '"+codigo_produto+"' " +  
	
	" AND produtos.tb_promocional = 'N' " +
	
	" ORDER BY produto_descricao, derivacao_descricao_derivacao";
	
	
	
	
	db.transaction(function(x) {
		x.executeSql(sql, [], function(x, dados) {
			
			
			
			if (dados.rows.length > 0) {
				
				
				
				var dado =  dados.rows.item(0);
				callback(dado);
			}else{
				
			
				callback({});
			}
		},
		function(tx, error) {
			console.log("Query Error: " + error.message); 
	    }
		
		
		)
	});
	
	
}

