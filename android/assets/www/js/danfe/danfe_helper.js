function gerar_danfe(codigo_empresa, codigo_filial, codigo_NFE,callback){	
	$.ajax({
		type: 	"GET",
		url: 	config.dw_pdr + 'gerar_danfe/gerar_ajax',
		data: { 
			codigoEmpresa : codigo_empresa, 
			codigoFilial : codigo_filial,
			numeroNFE: codigo_NFE
		},
		success: function(data) {
			var nome_arquivo = data;
			
			
			
			var caminho_danfe = config.danfe_url+ nome_arquivo;
			console.log('Iniciado download');
			console.log('caminho_danfe:'+ caminho_danfe);
			baixar_danfe(caminho_danfe, nome_arquivo, function(nome_arquivo){
				console.log('download realizado com sucesso');
				callback(nome_arquivo);
				
				
			});
			
			
			
		},
		error: function() {
			$("#aguarge_dialog").remove();
			mensagem('Não foi possível se conectar com o servidor, verifique sua conexão com a internet. Por favor tente novamente.');
		}
	});
	
}
	
 
function baixar_danfe(caminho, nome_arquivo, callback){
	var download = new DownloadZip(caminho, function(){	
		callback(nome_arquivo);
	}
	,'pdf'
	, function(){
		
			confirmar('O download não foi concluído. Você deseja tentar novamente?', 
			function () {
				$(this).dialog('close');
				
				//------------------
				//---------------------------------
		
				
				baixar_danfe(caminho, callback);
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
			},
			function () {
				$(this).dialog('close');
				$("#aguarge_dialog").remove();
				//------------------
				//---------------------------------
		
				
				
				//---------------------------------
				//------------------
				
				$("#confirmar_dialog").remove();
			});
		
		
		}
		
		
	);
	
	download.iniciarDownload();
}



function obter_numero_nota_fiscal_pedido(codigo_pedido, codigo_filial, callback){
	var pedido = {};
	
	
	
	
	
	db.transaction(function(x) {
		x.executeSql("SELECT * FROM pedidos_processados WHERE pedido_codigo = '"+codigo_pedido+"' AND pedido_filial = '"+codigo_filial+"' ORDER BY ip_codigo_item DESC  limit 1", [], function(x, dados) {
		
		console.log('passou');
			if (dados.rows.length > 0) {
				pedido = 	dados.rows.item(0);
				callback(pedido);
				
			}
	
		}, function(transaction, error) {
			alert("Error : " + error.message);
			//$("#aguarge_dialog").close();
			$("#aguarge_dialog").remove();
			
			
			
		});
		
		
		
	});
}


function abrirPDF(caminho)
{
	window.plugins.webintent.startActivity({
        action: window.plugins.webintent.ACTION_VIEW,
        url: caminho,
        type: 'application/pdf'
        },
        function(){
        },
        function(e){
        	console.log(e);
        	console.log('Error launching app update <' + caminho +'>');
        	
        	mensagem('Nenhum leitor de arquivos PDF encontrado.', function(){
        		
        	});
        }
    );     
	
}