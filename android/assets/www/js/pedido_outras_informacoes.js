$(document).ready(function(){

	// Desativar todos os campos quando clicar em id_informacao_cliente
	
	$('#id_outras_informacoes').click(function(){
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',2);
		
		ativar_tabs();
	});
	
	$('#avancar_passo_3').click(function(e){
		e.preventDefault();
		
		salvar_sessao('observacao_comercial', $('textarea[name=observacao_comercial]').val());
		salvar_sessao('pedido_cliente', $('input[name=pedido_cliente]').val());
		
		// Ativando Botao "Finalizar"
		$('#id_finalizar').removeClass('tab_desativada');
		
		ativar_tabs();
		
		//Chamando as funções do arquivo finalizar
		finalizar();
		
		// Acionando (Click) botao "finalizar"
		$("#id_finalizar").click();
	});
	
	// Pegando valores da sessao e adicionado no html
	$('input[name=pedido_cliente]').val(obter_valor_sessao('pedido_cliente') ? obter_valor_sessao('pedido_cliente') : '');
	$('input[name=codigo_cliente_entrega]').val(obter_valor_sessao('codigo_cliente_entrega') ? obter_valor_sessao('codigo_cliente_entrega') : '');
	$('textarea[name=observacao_comercial]').val(obter_valor_sessao('observacao_comercial') ? obter_valor_sessao('observacao_comercial') : '');

});

function outras_informacoes() {
	
	var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	
	exibir_informacoes_cliente_entrega(sessao_pedido.codigo_cliente, sessao_pedido.loja_cliente, sessao_pedido.empresa);
	
}

/**
* Metódo:		exibir_informacoes_cliente_entrega
* 
* Descrição:	Função Utilizada para exibir endereço do cliente de entrega
* 
* Data:			26/10/2013
* Modificação:	26/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_cliente_entrega(codigo, loja, empresa)
{
	if(codigo && loja)
	{
		
		var where = " AND empresa = '" + empresa + "'";
		
	
		db.transaction(function(x) {	
			x.executeSql(
				'SELECT * FROM clientes WHERE codigo = ? AND loja = ? ' + where, [codigo, loja], function(x, dados) {
					
					if(dados.rows.length)
					{
						var dado = dados.rows.item(0);
						
						$('.endereco_entrega').html(dado.endereco ? dado.endereco : 'N/A');
						$('.bairro_entrega').html(dado.bairro ? dado.bairro : 'N/A');
						$('.cep_entrega').html(dado.cep ? dado.cep : 'N/A');
						$('.cidade_entrega').html(dado.cidade ? dado.cidade : 'N/A');
						$('.estado_entrega').html(dado.estado ? dado.estado : 'N/A');
						
					}
					
				}
			);
		});
	}
}

