$(document).ready(function(){
	
	sessionStorage['sessao_tipo'] = 'sessao_pedido';
	
	var sessao_pedido  = obter_dados_pedido(); 
	
	
	// Ativando Biblioteca "TABS"
	$( "#tabs" ).tabs();
	
	// Desativar todos os campos quando clicar em id_informacao_cliente
	$('#id_informacao_cliente').click(function(){
		
		$('#id_adicionar_produtos').addClass('tab_desativada');
		$('#id_outras_informacoes').addClass('tab_desativada');
		$('#id_finalizar').addClass('tab_desativada');
		
		$("#tabs").tabs('select',0);
		
		ativar_tabs();
	});
	
	ativar_tabs();
	//E150/2015
	
	obter_filiais(sessao_pedido.codigo_cliente, sessao_pedido.loja_cliente, sessao_pedido.filial, sessao_pedido.empresa);
	

	
	
	
	/**
	* 
	* Classe:		select_gravar_sessao
	*
	* Descrição:	Classe utilizada para "select", com a finalizade de gravar na sessão o valor do campo quando o usuário usar o "change"
	*
	*/
	$('.select_gravar_sessao').live('change', function(){
		salvar_sessao($(this).attr('name'), $(this).val());
		if($(this).attr('name') == 'filial')
		{
			obter_clientes($(this).val());
		}
	});
	
	
	
	obter_tabela_precos(sessao_pedido);
	
	obter_condicao_pagamento(sessao_pedido.empresa, sessao_pedido.dados_cliente);
	
	remover_disabled();
	
	/**
	* 
	* ID:			trocar_cliente
	*
	* Descrição:	Utilizado para apagar todos os da
	* dos do cliente na sessão, e forçar que o representante selecione outro cliente
	*
	*/
	$('#trocar_cliente').live('click', function(){
		
		salvar_sessao('descricao_cliente', null);
		salvar_sessao('codigo_cliente', null);
		salvar_sessao('loja_cliente', null);
		salvar_sessao('codigo_icms_st_cliente', null);
		
		salvar_sessao('condicao_pagamento', null);
		salvar_sessao('tabela_precos', null);
		
		$('input[name="cliente"]').val("");
		$('input[name="codigo_cliente"]').val("");
		$('input[name="loja_cliente"]').val("");
		$('input[name="codigo_icms_st_cliente"]').val("");
		
		rotina_cliente();
		
		$(this).hide();
		$('#info_cli').hide();
		$('input[name="cliente"]').focus();
		
	});
	
	
	$('select[name="cliente_prospect"]').live('change', function(){
			
		if($(this).val() == 'P')
		{
			$('#campo_cliente').hide();
			$('#campo_prospect').show();
			
			//Limpando Dados do Cliente e Prospect
			$('#trocar_cliente').click();
			$('#trocar_cliente_entrega').click();
			
		}
		else
		{
			$('#campo_prospect').hide();
			$('#campo_cliente').show();
			
			//Limpando Dados do Cliente e Prospect
			$('#trocar_prospect').click();
		}
	});

	
	/**
	* 
	* CLASSE:		avancar
	*
	* Descrição:	Utilizado para validar os campos, se estiver tudo correto, passar para a proxima ABA
	*
	*/
	$('#avancar_passo_1').live('click', function(e){
		e.preventDefault();
		
		if(sessionStorage[sessionStorage['sessao_tipo']])
		{
			var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		}
		else
		{
			var sessao_pedido = [];
		}
		
		if(!sessao_pedido.filial)
		{
			mensagem('Selecione uma <strong>Filial</strong>.');
		}
		else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido')
		{
			mensagem('Selecione um <strong>Cliente</strong>.');
		}
		else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null))
		{
			mensagem('Selecione um <strong>Cliente</strong>.');
		}
		else if(!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P')
		{
			mensagem('Selecione um <strong>Prospect</strong>.');
		}
		else if(!sessao_pedido.tabela_precos)
		{
			mensagem('Selecione uma <strong>Tabela de Preços</strong>.');
		}
		else if(!sessao_pedido.condicao_pagamento)
		{
			mensagem('Selecione uma <strong>Condição de Pagamento</strong>.');
		}
		else
		{
			
			salvar_dados_forma_pagamento(sessao_pedido.condicao_pagamento);
			
			salvar_dados_cliente(sessao_pedido.codigo_cliente, sessao_pedido.loja_cliente);
			
			// Ativando Botao "Adicionar Produtos"
			$('#id_adicionar_produtos').removeClass('tab_desativada');
			
			ativar_tabs();
			
			//Chamando as funções do arquivo adicionar produtos
			adicionar_produtos();
			
			// Acionando (Click) botao "Adicionar Produtos"
			$("#id_adicionar_produtos").click();
		}
		
	});
	
	
	$('.cancelar').click(function(e){
		e.preventDefault();
		
		
		confirmar('Deseja cancelar esse pedido?', 
			function () {
				$(this).dialog('close');
				
				var cliente = obter_valor_sessao('dados_cliente');
				
				//-----------------

				obter_regiao(cliente.codigo_regiao_venda,function(regiao){					
					sessionStorage[sessionStorage['sessao_tipo']] = '';
					window.location = 'pedidos_adicionar_cliente_regiao.html#'+regiao.codigo+'|'+regiao.data_validade;
	
					//-----------------
	
					$("#confirmar_dialog").remove();
				} );
			}
		);
		
		
	});
	
	

});


/**
* Metódo:		ativar_tabs
* 
* Descrição:	Função Utilizada ativar a biblioteca, e desativar tabs com a classe tab_desativada
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function ativar_tabs()
{
	// Desativando tabs com classe tab_desativada
	var tabs_desativar = [];
			
	$( "#tabs ul li a" ).each(function(e, i){
		var classname = i.className;
		
		if(classname === 'tab_desativada')
		{
			tabs_desativar.push(e);
		}
	});
	
	$("#tabs").tabs({disabled: tabs_desativar});

}


/**
* Metódo:		obter_filiais
* 
* Descrição:	Função Utilizada para retornar as filiais
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_filiais(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa) {

	console.log('Código empresa: '+ codigo_empresa);
	
	
	
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM filiais WHERE empresa = ?', [codigo_empresa], function(x, dados) {
			var total = dados.rows.length;
			
			$('select[name=filial]').append('<option value="">Selecione...</option>');
			
			for(i = 0; i < total; i++) {
				var dado = dados.rows.item(i);
				$('select[name=filial]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.razao_social + '</option>');
			}
			
			// Selecionar campo se existir na sessão
			if(obter_valor_sessao('filial')) {
				$('select[name="filial"] option[value="' + obter_valor_sessao('filial') + '"]').attr('selected', 'selected');
			} else {
				if (total == 1) {
					// Se não existir valor na session entao seleciona a primeira opção se nao houver mais de uma
				    $('select[name="filial"] option:eq(1)').attr('selected', 'selected');
				    salvar_sessao('filial', $('select[name="filial"] option:eq(1)').val());
				    
				}	
			}
			
			//if(obter_valor_sessao('filial')) {
				obter_clientes(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa);
			//}
			
		});
	});
}



/**
* Metódo:		obter_tabela_precos
* 
* Descrição:	Função Utilizada para retornar Tabelas Preços
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_tabela_precos(sessao_pedido) {
	
	var codigo_empresa = sessao_pedido.empresa;
	var codigo_tabela_preco  = sessao_pedido.tabela_precos;
	var codigo_tabela_preco_promo =  sessao_pedido.codigo_tabela_preco_promo;
	
	
	var where = " WHERE codigo > 0 ";
	
	if(codigo_empresa) {
		where += " AND empresa = '" + codigo_empresa + "' ";
	}
	
	var codigos =  [];
	if(!!codigo_tabela_preco ){		
		codigos.push(codigo_tabela_preco);
	}
	
	if(!! codigo_tabela_preco_promo ){
		codigos.push(codigo_tabela_preco_promo);
		
	}
	
	where += ' AND codigo IN ('+implode(', ', codigos)+') '
	//CUSTOM E150/2015 - Não exibir tabelas de preços promocionais
	where += " AND promocional != 'S' ";
	//DATA INICIO DE VIGENCIA DA TABELA DE PREÇO
	where += " AND vigencia_inicio <= '" + date('Ymd') + "'";
	
	
	//DATA FINAL DE VIGENCIA DA TABELA DE PREÇO
	where += " AND (vigencia_final >= '" + date('Ymd') + "' OR vigencia_final = '0')";
	
	
	var sql = 'SELECT * FROM tabelas_preco ' + where;
	console.log(sql);
	
	db.transaction(function(x) {	
		x.executeSql(sql, [], function(x, dados) {
				
			var total = dados.rows.length;
			
			if(total > 0 ){
				$('select[name=tabela_precos]').html('<option value="">Selecione...</option>');
			
				
				
				for(i = 0; i < total; i++) {
					var dado = dados.rows.item(i);
					$('select[name=tabela_precos]').append('<option value="' + dado.codigo + '" data-condicao_pagamento="' + dado.condicao_pagamento + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
				
				//Seleciona o valor que estiver na sesão
				var tabela_precos_sessao = obter_valor_sessao('tabela_precos');
				if(tabela_precos_sessao) {
					$('select[name="tabela_precos"] option[value="' + tabela_precos_sessao + '"]').attr('selected', 'selected');
				}
				console.log(total);
				if(total == 1){
					$('select[name=tabela_precos]').attr('disabled', 'disabled');
				}else{
					$('select[name=tabela_precos]').removeAttr('disabled', 'disabled');
				}
			
			
			}else{
				$('select[name=tabela_precos]').html('<option>Nenhuma tabela de preços vinculada.</option>');
				
			}
		});
	});
}


/**
* Metódo:		obter_condicao_pagamento
* 
* Descrição:	Função Utilizada para retornar Condições de pagamento
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_condicao_pagamento(codigo_empresa, cliente)
{
	//consultar prazo médio caso haja condição de pagamento do cliente
	var entrou = '0';
	var prazoMedio = '0';
	//alert(cliente.condicao_pagamento+' - cliente.condicao_pagamento');
	if(cliente.condicao_pagamento != '')
	{
		//alert('entrou validação correta');
		entrou = '1';
		var where_ = " WHERE codigo = '"+ cliente.condicao_pagamento + "' order by prazo_medio ASC ";
		db.transaction(function(x) 
		{	
			x.executeSql('SELECT * FROM formas_pagamento ' + where_, [], function(x, prazo_medio) 
			{
				
				var dado = prazo_medio.rows.item(0);
				prazoMedio = dado.prazo_medio;
				prazoMedioLimite = dado.prazo_medio_limite;
				
				//alert(prazoMedio+' - prazoMedio');
				$('select[name="condicao_pagamento"]').removeAttr('disabled','disabled');
				$("#aviso_condicao_pagamento").html('');
				
				var where = " WHERE codigo <> '011' AND codigo <> '14' ";
				
				if(cliente.condicao_pagamento != '002' && cliente.condicao_pagamento != '015' && cliente.condicao_pagamento != '017')
				{
					where += " AND codigo <> '002' AND codigo <> '015' AND codigo <> '017' ";	
				}
				else if(cliente.condicao_pagamento == '002')
				{
					where += " AND codigo <> '015' AND codigo <> '017' ";	
				}
				else if(cliente.condicao_pagamento == '015')
				{
					where += " AND codigo <> '002' AND codigo <> '017' ";	
				}
				if(cliente.condicao_pagamento == '017')
				{
					where += " AND codigo <> '002' AND codigo <> '015' ";	
				}
				
				if(prazoMedio != '0')
				{
					where += " AND CAST(prazo_medio_limite as Integer ) <= " + prazoMedioLimite + "  ";
				}
				
				if(!!cliente)
				{
					where += " OR codigo = '"+cliente.condicao_pagamento+"'  ";
				}
				//where += " AND (CAST(codigo as Integer) <=  ? OR  prazo_medio IS NULL) ";
				
				console.log('SELECT * FROM formas_pagamento' + where);
				db.transaction(function(x) 
				{	
					x.executeSql('SELECT * FROM formas_pagamento ' + where + ' order by prazo_medio ASC ', [], function(x, dados) 
					{	
						var total = dados.rows.length;
						$('select[name=condicao_pagamento]').append('<option value="">Selecione...</option>');
						for(i = 0; i < total; i++)
						{
							var dado = dados.rows.item(i);
							$('select[name=condicao_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
						}
						
						// Selecionar campo se existir na sessão
						var condicao_pagamento = obter_valor_sessao('condicao_pagamento');
						if(condicao_pagamento) 
						{
							$('select[name="condicao_pagamento"] option[value="' + condicao_pagamento + '"]').attr('selected', 'selected');
							$('select[name="condicao_pagamento"]').change();
							
							if(condicao_pagamento === '001')
							{
								$('select[name="condicao_pagamento"]').attr('disabled','disabled');
								$("#aviso_condicao_pagamento").html('Cliente possui restrições.');
							}
						}
						
					});
				});
			});
		});
	}
	if(entrou == '0')
	{
		//alert('não podia entrar aqui');
		$('select[name="condicao_pagamento"]').removeAttr('disabled','disabled');
		$("#aviso_condicao_pagamento").html('');
		
		var where = " WHERE codigo <> '011' AND codigo <> '14' ";
		
		if(cliente.condicao_pagamento != '002' && cliente.condicao_pagamento != '015' && cliente.condicao_pagamento != '017')
		{
			where += " AND codigo <> '002' AND codigo <> '015' AND codigo <> '017' ";	
		}
		else if(cliente.condicao_pagamento == '002')
		{
			where += " AND codigo <> '015' AND codigo <> '017' ";	
		}
		else if(cliente.condicao_pagamento == '015')
		{
			where += " AND codigo <> '002' AND codigo <> '017' ";	
		}
		if(cliente.condicao_pagamento == '017')
		{
			where += " AND codigo <> '002' AND codigo <> '015' ";	
		}
		
		if(prazoMedio != '0')
		{
			where += " AND CAST(prazo_medio as Integer ) <= " + prazoMedio + "  ";
		}
		
		if(!!cliente)
		{
			where += " OR codigo = '"+cliente.condicao_pagamento+"'  ";
		}
		//where += " AND (CAST(codigo as Integer) <=  ? OR  prazo_medio IS NULL) ";
		
		console.log('SELECT * FROM formas_pagamento' + where);
		db.transaction(function(x) 
		{	
			x.executeSql('SELECT * FROM formas_pagamento ' + where + ' order by prazo_medio ASC ', [], function(x, dados) 
			{	
				var total = dados.rows.length;
				$('select[name=condicao_pagamento]').append('<option value="">Selecione...</option>');
				for(i = 0; i < total; i++)
				{
					var dado = dados.rows.item(i);
					$('select[name=condicao_pagamento]').append('<option value="' + dado.codigo + '">' + dado.codigo + ' - ' + dado.descricao + '</option>');
				}
				
				// Selecionar campo se existir na sessão
				var condicao_pagamento = obter_valor_sessao('condicao_pagamento');
				if(condicao_pagamento) 
				{
					$('select[name="condicao_pagamento"] option[value="' + condicao_pagamento + '"]').attr('selected', 'selected');
					$('select[name="condicao_pagamento"]').change();
					
					if(condicao_pagamento === '001')
					{
						$('select[name="condicao_pagamento"]').attr('disabled','disabled');
						$("#aviso_condicao_pagamento").html('Cliente possui restrições.');
					}
				}
				
			});
		});
	}
}

/**
* Metódo:		obter_clientes
* 
* Descrição:	Função Utilizada para retornar Todos os Clientes
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_clientes(codigo_cliente, codigo_loja, codigo_filial, codigo_empresa) {
	
	if(codigo_empresa) {
		var wheres = " WHERE empresa = '" + codigo_empresa + "'";
	}
	
	if(codigo_filial) {
		wheres += " AND (filial = '" + codigo_filial + "' OR filial = '')";
	}
	
	if(codigo_cliente){
		wheres += " AND (codigo = '" + codigo_cliente+ "')";
		
	}
	
	if(codigo_loja){
		wheres += " AND (loja = '" + codigo_loja+ "')";
		
	}
	
	$('#autocomplete_selecionar_cliente').hide();
	$('#carregando_listagem_clientes').show();
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM clientes ' + wheres, [], function(x, dados) {
			if (dados.rows.length) {
				var clientes = [];
				
				for (i = 0; i < dados.rows.length; i++) {
					var dado = dados.rows.item(i);
					
					clientes.push({ label: dado.codigo  + ' | ' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf, codigo: dado.codigo, loja: dado.loja, tabela_preco: dado.tabela_preco, desconto: dado.desconto, condicao_pagamento: dado.condicao_pagamento, status: dado.status, codigo_icms_st_cliente: dado.codigo_icms_st_cliente});
				}
				
				buscar_clientes(clientes);
				
				$('#carregando_listagem_clientes').hide();
				$('#autocomplete_selecionar_cliente').show();
				
				// Selecionar campo se existir na sessão
				if(obter_valor_sessao('codigo_cliente')) {
					$('input[name=cliente]').val(obter_valor_sessao('descricao_cliente'));
					$('input[name=codigo_cliente]').val(obter_valor_sessao('codigo_cliente'));
					$('input[name=loja_cliente]').val(obter_valor_sessao('loja_cliente'));
					$('input[name=codigo_icms_st_cliente]').val(obter_valor_sessao('codigo_icms_st_cliente'));
					
					exibir_informacoes_cliente(obter_valor_sessao('codigo_cliente'), obter_valor_sessao('loja_cliente'), obter_valor_sessao('empresa'), obter_valor_sessao('filial'));
					
					// Bloquear campo quando selecionar cliente
					$('input[name=cliente]').attr('disabled', 'disabled');
					$('#trocar_cliente').show();
				}
			}
		});
	});

}


/**
* Metódo:		buscar_clientes
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_clientes(clientes) {
	$('input[name=cliente]').autocomplete({
		minLength: 3,
		source: clientes,
		position : { my : "left bottom", at: "left top", collision : "none"},
		select: function( event, ui ) {
		
			if(ui.item.status == 'I') {
				mensagem('O cliente <br/><b>' + ui.item.label + '</b><br/> está inativo no momento<br/><br/>Não será possível prosseguir com a inclusão do pedido.');
				$('#trocar_cliente').trigger('click');
			} else {
				$('input[name=cliente]').val(ui.item.label);
				$('input[name=codigo_cliente]').val(ui.item.codigo);
				$('input[name=loja_cliente]').val(ui.item.loja);
				$('input[name=codigo_icms_st_cliente]').val(ui.item.codigo_icms_st_cliente);
				
				salvar_sessao('descricao_cliente', ui.item.label);
				salvar_sessao('codigo_cliente', ui.item.codigo);
				salvar_sessao('loja_cliente', ui.item.loja);
				salvar_sessao('codigo_icms_st_cliente', ui.item.codigo_icms_st_cliente);
				salvar_sessao('desconto_cliente', ui.item.desconto);
				
				//--------------
				// Salvando Cliente de Entrega
				salvar_sessao('descricao_cliente_entrega', ui.item.label);
				salvar_sessao('codigo_cliente_entrega', ui.item.codigo);
				salvar_sessao('loja_cliente_entrega', ui.item.loja);
				//--------------
				
				exibir_informacoes_cliente(ui.item.codigo, ui.item.loja);
				
				rotina_cliente(ui.item);
				
				// Bloquear campo quando selecionar cliente
				$('input[name=cliente]').attr('disabled', 'disabled');
				$('#trocar_cliente').show();
				
			}
			return false;
		}
	});
}

/**
* Metódo:		exibir_informacoes_cliente
* 
* Descrição:	Função Utilizada para exibir informações do cliente
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function exibir_informacoes_cliente(codigo, loja, codigo_empresa, codigo_filial) {
	if(codigo && loja) {
		if(codigo_empresa) {
			var where = " AND empresa = '" + codigo_empresa + "'";
		}
	
		db.transaction(function(x) {
			x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ? ' + where, [codigo, loja], function(x, dados) {
					
				if(dados.rows.length) {
					
					var dado = dados.rows.item(0);
					
					var html = '';
					
					$('.info_nome').html(dado.nome);
					$('.info_cpf').html(dado.cpf);
					
					obter_ultimo_pedido_cliente(codigo, loja);
					
					$('.info_limite_credito').html(number_format(dado.limite_credito, 3, ',', '.'));
					$('.info_titulos_aberto').html(number_format(dado.total_titulos_aberto, 3, ',', '.'));
					$('.info_titulos_vencidos').html(number_format(dado.total_titulos_ventidos, 3, ',', '.'));
					$('.info_credito_disponivel').html(number_format(dado.limite_credito - dado.total_titulos_aberto, 3, ',', '.'));
					
					$('.info_endereco').html(dado.endereco);
					$('.info_bairro').html(dado.bairro);
					$('.info_cidade').html(dado.cidade);
					$('.info_estado').html(dado.estado);
					
					$('.info_cep').html(dado.cep);
					
					$('.info_proximidade').html(dado.proximidade);
					$('.info_horario_entrega').html(dado.horario_entrega);
					
					/*Tratamento telefone*/
					var telefone = '';
					if(dado.ddd) {
						telefone = '('+dado.ddd+') ' + dado.telefone;
					} else {
						if(dado.telefone) {
							telefone = dado.telefone;
						}						
					}
					/*Tratamento telefone*/
					$('.info_telefone').html(telefone);
					
					$('#info_cli').show();
				}
				
			});
		});
	}
}

/**
* Metódo:		salvar_sessao
* 
* Descrição:	Função Utilizada para salvar dados do pedido na sessão
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		String 			var nome_campo		- Nome do campo que será utilizado na sessao
* @param		String 			var valor			- Valor do campo que será utilizado na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_sessao(nome_campo, valor) {
	var sessao_pedido = [];
		
	// Se existir algum dado na sessão, pegar esses dados e colocar no array "sessao_pedido"
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	// Adicionar novo valor no array
	sessao_pedido[nome_campo] = valor;
	
	// Gravar dados do Array na sessão
	sessionStorage[sessionStorage['sessao_tipo']] = serialize(sessao_pedido);
	
	remover_disabled();
}

/**
* Metódo:		obter_valor_sessao
* 
* Descrição:	Função Utilizada para retornar o valor de campo da sessão
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @param		String 			var campo		- Nome do campo que será utilizado para retornar o valor na sessao
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_valor_sessao(campo) {
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		var sessao = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}
	
	if(sessao) {
		if(sessao[campo]) {
			return sessao[campo];
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/**
* Metódo:		obter_ultimo_pedido_cliente
* 
* Descrição:	Função Utilizada para pegar o ultimo pedido do cliente e exibir os valores nas informações do cliente
* 
* Data:			10/10/2013
* Modificação:	10/10/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do Cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_ultimo_pedido_cliente(codigo, loja) {
	db.transaction(function(x) {
		x.executeSql('SELECT SUM(ip_valor_total_item) AS valor_pedido, SUM(ip_total_desconto_item) AS valor_desconto, * FROM pedidos_processados WHERE cliente_codigo = ? AND cliente_loja = ? GROUP BY pedido_codigo ORDER BY pedido_codigo DESC', [codigo, loja], function(x, dados) {
			if(dados.rows.length) {
				$('#conteudo_ultimo_pedido ul').show();
				$('#conteudo_ultimo_pedido div').hide();
				
				var dado = dados.rows.item(0);
				
				$('.info_pedido_data_emissao').html(protheus_data2data_normal(dado.pedido_data_emissao));
				$('.info_pedido_codigo').html(dado.pedido_codigo);
				$('.info_pedido_codigo').attr('href', 'pedidos_espelho.html#' + dado.pedido_codigo + '|' + dado.pedido_filial);
				$('.info_forma_pagamento').html(dado.forma_pagamento_descricao);
				$('.info_valor_pedido').html('R$ ' + number_format(dado.valor_pedido, 3, ',', '.'));
				$('.info_valor_desconto').html('R$ ' + number_format(dado.valor_desconto, 3, ',', '.'));
			} else {
				$('#conteudo_ultimo_pedido ul').hide();
				$('#conteudo_ultimo_pedido div').show();
			}
		});
	});
}


//---------------------------------------------------------------------------
//----------  ----------  Rotinas (Regras) da Incusão do Pedido ----------  ----------
//---------------------------------------------------------------------------

/**
* Metódo:		rotina_cliente
* 
* Descrição:	Função Utilizada para selecionar a tabela de preços do cliente (Se existir, se não seleciona a opção vazia "Selecione..." e bloqueia o campo e condicao_pagamento)
* 
* Data:			11/10/2013
* Modificação:	11/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function rotina_cliente(dados_cliente) {
	
	var tabela_precos = (dados_cliente ? dados_cliente.tabela_preco : false);
	var condicao_pagamento = (dados_cliente ? dados_cliente.condicao_pagamento : false);
	
	
	
	
	//Sugere a tabela de preços do cliente
	if(tabela_precos) {
		$('select[name="tabela_precos"] option[value="' + tabela_precos + '"]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
	} else {
		$('select[name="tabela_precos"] option[value=""]').attr('selected', 'selected');
		$('select[name="tabela_precos"]').change();
		$('select[name="tabela_precos"]').attr('disabled', 'disabled');
		
		$('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
		$('select[name="condicao_pagamento"]').change();
		$('select[name="condicao_pagamento"]').attr('disabled', 'disabled');
	}
	
	//Sugere a condição de pagamento do cliente
	if(condicao_pagamento) {
		$('select[name="condicao_pagamento"] option[value="' + condicao_pagamento + '"]').attr('selected', 'selected');
		$('select[name="condicao_pagamento"]').change();
	} else {
		$('select[name="condicao_pagamento"] option[value=""]').attr('selected', 'selected');
		$('select[name="condicao_pagamento"]').change();
	}
}

/**
* Metódo:		remover_disabled
* 
* Descrição:	Função Utilizada para habilitar os botoes que estao bloqueados
* 
* Data:			09/10/2013
* Modificação:	09/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function remover_disabled() {
	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
		
		// Se existir Filial na sessao, habilitar TIPO DE PEDIDO
		if(sessao_pedido.filial && !sessao_pedido.codigo_cliente) {
			$('input[name=cliente]').removeAttr('disabled');
		}
		
		// Se existir Cliente na sessao, habilitar TABELA de PREÇOS
		if(sessao_pedido.codigo_cliente) {
			//Tabela de preço sempre será fixa pelo cliente.
			//$('select[name=tabela_precos]').removeAttr('disabled');
		}
		
		//Habilita o campo de condição de pagamento
		if(sessao_pedido.tabela_precos) {
			$('select[name=condicao_pagamento]').removeAttr('disabled');
		}
		
		//-------------------------------------------------------------------------------------------------------------
		//------------ Desativar botao "Adicionar Produtos" se uma opção nao for selecionada --------------------------
		//-------------------------------------------------------------------------------------------------------------
		
		if(!sessao_pedido.filial) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_pedido') {
			
			$('#id_adicionar_produtos').addClass('tab_desativada');
		
		} else if(!sessao_pedido.codigo_cliente && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && (sessao_pedido.cliente_prospect == 'C' || sessao_pedido.cliente_prospect == null)){
			
			$('#id_adicionar_produtos').addClass('tab_desativada');
		
		} else if(!sessao_pedido.codigo_prospect && sessionStorage['sessao_tipo'] == 'sessao_orcamento' && sessao_pedido.cliente_prospect == 'P') {
			
			$('#id_adicionar_produtos').addClass('tab_desativada');
			
		}
		//------------------------------------------
		//----------------------------
		
		
		
		if(!sessao_pedido.tabela_precos) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		if(!sessao_pedido.condicao_pagamento) {
			$('#id_adicionar_produtos').addClass('tab_desativada');
		}
		
		ativar_tabs();

	}
}


/**
* Metódo:		salvar_dados_cliente
* 
* Descrição:	Função Utilizada para salvar dados do cliente
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo do Cliente
* @param		String 			var loja		- Loja do cliente
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_cliente(codigo, loja) {
	if(codigo && loja) {
		db.transaction(function(x) {	
			x.executeSql('SELECT * FROM clientes WHERE codigo = ? AND loja = ?', [codigo, loja], function(x, dados) {
				if(dados.rows.length) {
					var dado = dados.rows.item(0);

					salvar_sessao('dados_cliente', dado);
				}
			});
		});
	}
}


/**
* Metódo:		salvar_dados_forma_pagamento
* 
* Descrição:	Função Utilizada para salvar dados da forma de pagamento
* 
* Data:			07/11/2013
* Modificação:	07/11/2013
* 
* @access		public
* @param		String 			var codigo		- Codigo forma de pagamento
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function salvar_dados_forma_pagamento(codigo) {
	if(codigo) {
		db.transaction(function(x) {	
			x.executeSql('SELECT * FROM formas_pagamento WHERE codigo = ?', [codigo], function(x, dados) {
				if(dados.rows.length) {
					var dado = dados.rows.item(0);
					
					salvar_sessao('dados_forma_pagamento', dado);
				}
			});
		});
	}
}

/**
* Metódo:		obter_descricao_pedido
* 
* Descrição:	Função Utilizada para obter a descrição do pedido
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/

function obter_descricao_pedido(tipo) {
	var descricao_tipo_pedido = 'Pedido';
	
	if(tipo == 'upper') {
		
		return descricao_tipo_pedido.toUpperCase();
	
	} else if(tipo == 'lower') {
		
		return descricao_tipo_pedido.toLowerCase();
	
	} else {
		
		return descricao_tipo_pedido;
	}
}

/*
 * 
 * Calculo de Impostos.
 * 
 *  Parametros utilizados: Empresa, Filial, Codigo ICMS ST do Cliente e 
 *  Preço de Venda do Produto com Descontos
 * 
 */
var calcular_impostos_produtos = function(preco_unitario_venda_produto , percentual_desconto) {
	var sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	var filial =  obter_valor_sessao('filial');
	console.log('Preço Unitário sem impostos: '+preco_unitario_venda_produto );

	
	preco_unitario_venda_produto = preco_unitario_venda_produto  - (preco_unitario_venda_produto * percentual_desconto /100);
	
	if (sessao_pedido.empresa == '1') {// Empresa Kimyto

		if (filial  == '1') {//Filial SC
			
			if (obter_valor_sessao('codigo_icms_st_cliente') == '30') {
				var modificador = 1.11205;
				if(percentual_desconto == 3){
					modificador = 1.113643;
					return preco_unitario_venda_produto * modificador ;
				}
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' X Impostos: '+modificador );
				
				return preco_unitario_venda_produto * modificador ;
			}
			if (obter_valor_sessao('codigo_icms_st_cliente') == '00') {
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' X Impostos: '+1.05 );
				
				return preco_unitario_venda_produto * 1.05;
			}
			if (obter_valor_sessao('codigo_icms_st_cliente') == '') {
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' X Impostos: '+1.05 );
				return preco_unitario_venda_produto * 1.05;
			}
			if (obter_valor_sessao('codigo_icms_st_cliente') == '70') {
				var modificador = 0.845;
				if(percentual_desconto == 3){
					modificador = 0.8423669;
					return preco_unitario_venda_produto / modificador ;
				}
				
				console.log('Memoria de Cálculo:  - Percentual desconto: '+percentual_desconto + ' /  Impostos: '+ modificador );
				return preco_unitario_venda_produto / modificador ;
			}
		}

		if (filial == '2') {//Filial PR//Filial PR
			
			return preco_unitario_venda_produto * 1.191; 
		}

	}
	

	return preco_unitario_venda_produto;
}




/**
 * Obtém dados dos pedidos   
 * 
 * 
 */

function obter_dados_pedido(){

	var sessao_pedido = [];

	if(sessionStorage[sessionStorage['sessao_tipo']]) {
		sessao_pedido = unserialize(sessionStorage[sessionStorage['sessao_tipo']]);
	}

	return sessao_pedido;
	
}

