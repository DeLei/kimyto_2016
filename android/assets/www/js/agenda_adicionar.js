"use strict";

$(document).ready(function() {
	
	//Clientes
	obter_clientes();
	//Prospects
	obter_prospects();
	
	//-------------------------------------------------------
	//SELECIONAR PARA QUE É O AGENDAMENTO
	//-------------------------------------------------------
	$('.toolbar > .btn-group > button').click(function(e){
		
		//Para o evento
		e.preventDefault();
		
		//Esconder todos os div
		$('#clientePara, #prospectPara, #diversosPara').hide();
		
		//Remover de todos
		$('.toolbar > .btn-group > button').removeClass('active');
		
		//Ativar botão
		$(this).addClass('active');
		
		//Obter para quem é o agendamento
		var para = $(this).data('para');
		
		//-------------------------------
		//Tipo
		var tipo = $(this).data('tipo');
		$('#tipo').val(tipo);
		//Tipo
		//-------------------------------	
		
		//Exibir campo
		$('#'+para).show();
	});
	//-------------------------------------------------------
	//SELECIONAR PARA QUE É O AGENDAMENTO
	//-------------------------------------------------------
	
	//-----------------------------
	// INICIO - SALVAR AGENDA
	//-----------------------------
	$('form[name=formAgenda]').submit(function() {
		if (!$('input[name=tipo]').val())
		{
			mensagem('Selecione uma opção. <strong>Cliente, Prospect ou Diversos/Outros</strong>.');
		}
		else if (($('input[name=tipo]').val() == 'Cliente') && !$('input[name=cliente]').val())
		{
			mensagem('Campo <strong>CLIENTE</strong> deve ser preenchido.');
		}
		else if (($('input[name=tipo]').val() == 'Prospects') && !$('input[name=prospect]').val())
		{
			mensagem('Campo <strong>PROSPECT</strong> deve ser preenchido.');
		}
		else if (($('input[name=tipo]').val() == 'Outros') && !$('input[name=diversos]').val())
		{
			mensagem('Campo <strong>DIVERSOS/OUTROS</strong> deve ser preenchido.');
		}
		else if (!$('input[name=data]').val())
		{
			mensagem('Campo <strong>DATA</strong> deve ser preenchido.');
		}
		else if (!$('input[name=hora]').val())
		{
			mensagem('Campo <strong>HORA</strong> deve ser preenchido.');
		}
		else if (!$('input[name=titulo]').val())
		{
			mensagem('Campo <strong>TÍTULO</strong> deve ser preenchido.');
		}
		else if (!$('select[name=class]').val())
		{
			mensagem('Selecione uma opção em <strong>TIPO</strong>.');
		}
		else if (!$('textarea[name=descricao]').val())
		{
			mensagem('Campo <strong>DESCRIÇÃO</strong> deve ser preenchido.');
		}
		else
		{
			db.transaction(function(x) {
				//SELECT EM AGENDA
				x.executeSql('SELECT id FROM pendencias ORDER BY id DESC LIMIT 1', [], function(x, dados) {
					$('input[name], textarea[name], select[name]').each(function() {

						var codigo = uniqid();
						
						var tmp_1 = ['id'];
						var tmp_2 = ['?'];
						var tmp_3 = [codigo];
						
						$('input[name], textarea[name], select[name]').each(function() {
							var campo_nome 	= $(this).attr('name');
							var campo_valor = $(this).val();
							
							
							if(campo_nome == 'data')
							{
								if(campo_valor)
								{
									campo_valor = data_normal2protheus(campo_valor);
								}
							}
							
							tmp_1.push(campo_nome);
							tmp_2.push('?');
							tmp_3.push(campo_valor);
						});
						
						//////////////////////////////////////////////////////
						tmp_1.push('status');
						tmp_2.push('?');
						tmp_3.push('agendado');
						
						tmp_1.push('timestamp');
						tmp_2.push('?');
						tmp_3.push(time());
						
						tmp_1.push('latitude');
						tmp_2.push('?');
						tmp_3.push(localStorage.getItem('gps_latitude'));
						
						tmp_1.push('longitude');
						tmp_2.push('?');
						tmp_3.push(localStorage.getItem('gps_longitude'));
						
						tmp_1.push('versao');
						tmp_2.push('?');
						tmp_3.push(config.versao);				
						//////////////////////////////////////////////////////
						
					});
				});
			});
		}
		
		return false;
	});
	//-----------------------------
	// FIM - SALVAR AGENDA
	//-----------------------------
	
	/**
	* 
	* ID:			trocar_cliente
	*
	* Descrição:	Utilizado para apagar todos os dados do cliente na sessão, e forçar que o representante selecione outro cliente
	*
	*/
	$('#trocar_cliente').live('click', function(){
		
		$('input[name="cliente"]').val("");
		$('input[name="codigo_cliente"]').val("");
		$('input[name="loja_cliente"]').val("");
		
		//Remover
		$('input[name="cliente"]').removeAttr('disabled');
		
		$(this).hide();		
	});

	/**
	* 
	* ID:			trocar_prospect
	*
	* Descrição:	Utilizado para apagar todos os dados do prospect na sessão, e forçar que o representante selecione outro prospect
	*
	*/
	$('#trocar_prospect').live('click', function(){
		
		$('input[name="prospect"]').val("");
		$('input[name="codigo_prospect"]').val("");
		$('input[name="loja_prospect"]').val("");
		
		//Remover
		$('input[name="prospect"]').removeAttr('disabled');
		
		$(this).hide();		
	});
	
	
});


/**
* Metódo:		obter_clientes
* 
* Descrição:	Função Utilizada para retornar Todos os Clientes
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_clientes()
{
	if(info.empresa)
	{
		var wheres = " WHERE empresa = '" + info.empresa + "'";
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM clientes ' + wheres, [], function(x, dados) {
				if (dados.rows.length)
				{
					var clientes = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						clientes.push({ label: dado.codigo + '/' + dado.loja + ' - ' + dado.nome + ' - ' + dado.cpf, codigo: dado.codigo, loja: dado.loja, tabela_preco: dado.tabela_preco, desconto: dado.desconto, condicao_pagamento: dado.condicao_pagamento});
					}
					
					buscar_clientes(clientes);
				
				}
			}
		);
	});

}
/**
* Metódo:		buscar_clientes
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_clientes(clientes)
{
	$('input[name=cliente]').autocomplete({
		minLength: 3,
		source: clientes,
		position : { my : "left bottom", at: "left bottom", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=cliente]').val(ui.item.label);
			$('input[name=codigo_cliente]').val(ui.item.codigo);
			$('input[name=loja_cliente]').val(ui.item.loja);
			
			// Bloquear campo quando selecionar cliente
			$('input[name=cliente]').attr('disabled', 'disabled');
			$('#trocar_cliente').show();

			return false;
		}
	});
}

/**
* Metódo:		obter_prospects
* 
* Descrição:	Função Utilizada para retornar Todos os Prospects
* 
* Data:			13/11/2013
* Modificação:	13/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function obter_prospects()
{
	var wheres = "WHERE status = 'A' OR status IS NULL";
	
	if(info.empresa)
	{
		wheres += " AND empresa = '" + info.empresa + "' ";
	}
	
	db.transaction(function(x) {
		x.executeSql('SELECT * FROM prospects ' + wheres, [], function(x, dados) {
				if (dados.rows.length)
				{
					var prospects = [];
					
					for (i = 0; i < dados.rows.length; i++)
					{
						var dado = dados.rows.item(i);
						
						prospects.push({ label: dado.codigo + '/' + dado.codigo_loja + ' - ' + dado.nome + ' - ' + dado.cgc, codigo: dado.codigo, loja: dado.codigo_loja});
					}
					
					buscar_prospects(prospects);
		
				}
			}
		);
	});
	
}

/**
* Metódo:		buscar_prospects
* 
* Descrição:	Função Utilizada para buscar os clientes digitados no autocomplete
* 
* Data:			08/10/2013
* Modificação:	08/10/2013
* 
* @access		public
* @param		array 			var clientes		- Todos os clientes do representante
* @version		1.0
* @author 		DevelopWeb Soluções Web
* 
*/
function buscar_prospects(prospects)
{	
	$('input[name=prospect]').autocomplete({
		minLength: 3,
		source: prospects,
		position : { my : "left bottom", at: "left bottom", collision : "none"},
		select: function( event, ui ) {
		
			$('input[name=prospect]').val(ui.item.label);
			$('input[name=codigo_prospect]').val(ui.item.codigo);
			$('input[name=loja_prospect]').val(ui.item.loja);
			
			// Bloquear campo quando selecionar prospect
			$('input[name=prospect]').attr('disabled', 'disabled');
			$('#trocar_prospect').show();

			return false;
		}
	});
}
