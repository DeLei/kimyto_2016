//------------------------------------------------------------------------//
//------------------------------ MÉTODOS ---------------------------------//
//------------------------------------------------------------------------//
//	Autor:		William Reis Fernandes									--//
//	Data:		27/09/2013												--//
//	Versão:		1.0														--//
//																		--//
//	Descrição: SINCRONIZAR OS REGISTROS QUE O REPRESENTANTE DESEJA		--//
// 																		--//
//------------------------------------------------------------------------//
$(document).ready(function() {
	
	//====================================================================
	// INICIAR - FUNÇÕES PARA POPULAR LISTAGEM
	//====================================================================

	//************************************
	//Obter os históricos de clientes
	//************************************
	obter_clientes_historicos();
	//************************************
	//Obter os históricos de clientes
	//************************************
	
	
	//************************************
	//Obter pendências
	//************************************
	obter_pendencias();
	obter_pendencias_mensagens();
	//************************************
	//Obter pendências
	//************************************
	
	//************************************
	//Obter Agenda
	//************************************
	obter_agenda();	
	//************************************
	//Obter Agenda
	//************************************
	
	obter_regioes_vendas_aguardando();
	obter_pedidos_aguardando();
	
	//====================================================================
	// FINAL - FUNÇÕES PARA POPULAR LISTAGEM
	//====================================================================
	
	//====================================================================
	// INICIAR - FUNÇÕES PARA SELECIONAR OS REGISTROS E MODULOS
	//====================================================================
	//--------------------------------------------------------------------
	//BOTÃO SELECIONAR TODOS
	//--------------------------------------------------------------------
	$("#btnSelecionarTodos").live('click', function(e){	
		e.preventDefault();
		
		var checkbox = $('#selecionar_todos').is(':checked');	
		if(checkbox == false)
		{
			checkbox = true;
		}
		else
		{
			checkbox = false;
		}
		
		$('#selecionar_todos').attr({checked: checkbox});
		
		if(checkbox){
			$("input[name='modulos[]']").attr({checked: checkbox});
			$("input[type='checkbox']").button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$("input[name='modulos[]']").attr({checked: checkbox});
			$("input[type='checkbox']").button("option", "icons", {primary: "ui-icon-circle-close"});
		}

	});
	

	//--------------------------------------------------------------------
	//BOTÃO SELECIONAR TODOS
	//--------------------------------------------------------------------	
	
	//--------------------------------------------------------------------
	// Setar checks para Jquery UI 
	//--------------------------------------------------------------------
	$(  "input[name='prospects[]'], " +
		"input[name='historicos-prospects[]'], " +
		"input[name='pendencias[]'], " +
		"input[name='clientes[]']" 
		).button({ 
				icons: {primary:'ui-icon-circle-close'} 
	});
	//--------------------------------------------------------------------
	// Setar checks para Jquery UI 
	//--------------------------------------------------------------------
	
	//--------------------------------------------------------------------
	//
	//--------------------------------------------------------------------
	$("input[name='prospects[]'], input[name='historicos-prospects[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#modulos-1").attr({checked: 'true'});
	});
	
	//--------------------------------------------------------------------
	//
	//--------------------------------------------------------------------
	$("input[name='clientes[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#modulos-2").attr({checked: 'true'});
	});
	//--------------------------------------------------------------------
	
	
	
	//--------------------------------------------------------------------
	// pedidos pendentes
	//--------------------------------------------------------------------
	$("input[name='pedidos_pendentes[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#pedidos_pendentes").attr({checked: 'true'});
	});
	
	
	
	//--------------------------------------------------------------------
	// regioes pendentes
	//--------------------------------------------------------------------
	$("input[name='regioes_pendentes[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#regioes_pendentes").attr({checked: 'true'});
	});
	
	
	//--------------------------------------------------------------------
	//
	//--------------------------------------------------------------------
	$("input[name='pendencias[]']").live('click', function(){	
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');
		//Icone do campo
		if(checkbox){
			$(this).button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$(this).button("option", "icons", {primary: "ui-icon-circle-close"});
		}
		//Marca todos os campos
		$("#pendencias").attr({checked: 'true'});
	});
	
	//--------------------------------------------------------------------
	//Se o usuário clicar no modulo selecionar todos os registros da lista
	//--------------------------------------------------------------------
	$("input[name='modulos[]']").live('click', function(){	
		
		// Capturar modulo que o usuário clicou
		var modulo = $(this).data('modulo');
		
		// Verificar se o campo foi checado
		var checkbox = $(this).is(':checked');	
			
		//Marca todos os campos
		$("input[name='"+modulo+"[]"+"']").attr({checked: checkbox});
		$("input[name='historicos-"+modulo+"[]"+"']").attr({checked: checkbox});
		//Icones
		if(checkbox){
			$("input[name='"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-check"});
			$("input[name='historicos-"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-check"});
		}else{
			$("input[name='"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-close"});
			$("input[name='historicos-"+modulo+"[]"+"']").button("option", "icons", {primary: "ui-icon-circle-close"});
		}		
	});
	//--------------------------------------------------------------------
	//Se o usuário clicar no modulo selecionar todos os registros da lista
	//--------------------------------------------------------------------	
	
	//====================================================================
	// FINAL - FUNÇÕES PARA SELECIONAR OS REGISTROS E MODULOS
	//====================================================================
});


function obter_clientes_historicos()
{
	db.transaction(function(x) {
		
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM historico_clientes WHERE id > 0 AND (exportado IS NULL OR editado = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-historicos-clientes').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					var prospect = '<input type="checkbox" value="'+item.id+'" name="historicos-clientes[]" id="clientes-prospects-'+i+'" /><label for="clientes-prospects-'+i+'" ' + style_erro + '>'+item.pessoa_contato+' - '+(item.cargo ? item.cargo : 'N\A')+' - '+(item.email ? item.email : 'N\A')+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-historicos-clientes').append('<li>'+prospect+'</li>');
				}
			}else{
				$('#lista-historicos-clientes').append('<li>Nenhum histórico de clientes para sincronizar.</li>');
			}
			$("input[name='historicos-clientes[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

function obter_agenda()
{
	db.transaction(function(x) {
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM agenda WHERE (exportado IS NULL OR editado = 1 OR remover = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-agenda').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					if(item.remover == 1)
					{
						style_erro += ' class="erro" ';
						msg_erro += ' Excluir compromisso.';
					}
					
					
					var pendencia = '<input type="checkbox" value="'+item.id+'" name="compromissos[]" id="compromissos-'+i+'" /><label for="compromissos-'+i+'" ' + style_erro + '>'+item.titulo+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-agenda').append('<li>'+pendencia+'</li>');
				}
			}else{
				$('#lista-agenda').append('<li>Nenhum compromisso para sincronizar.</li>');
			}
			$("input[name='compromissos[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

function obter_pendencias()
{
	db.transaction(function(x) {
		
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM pendencias WHERE (exportado IS NULL OR editado = 1 OR remover = 1 OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-pendencias').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					if(item.remover == 1)
					{
						style_erro += ' class="erro" ';
						msg_erro += ' Excluir pendência.';
					}
					
					if(item.status == 'aguardando_encerramento')
					{
						style_erro += ' class="erro" ';
						msg_erro += ' Solicitar encerramento.';
					}
					
					
					var pendencia = '<input type="checkbox" value="'+item.id+'" name="pendencias[]" id="pendencias-'+i+'" /><label for="pendencias-'+i+'" ' + style_erro + '>'+item.titulo+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-pendencias').append('<li>'+pendencia+'</li>');
				}
			}else{
				$('#lista-pendencias').append('<li>Nenhuma pendência para sincronizar.</li>');
			}
			$("input[name='pendencias[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

function obter_pendencias_mensagens()
{
	db.transaction(function(x) {
		
		// Exibir Prospects Historicos
		x.executeSql('SELECT * FROM pendencias_mensagens WHERE (exportado IS NULL OR erro = 1)', [], function(x, dados){
			
			if(dados.rows.length)
			{
				$('#lista-pendencias-mensagens').empty();
			
				for (i = 0; i < dados.rows.length; i++)
				{
					var item = dados.rows.item(i);										
					
					var style_erro = '';
					var msg_erro = '';
					if(item.erro == 1)
					{
						style_erro = ' class="erro" ';
						msg_erro = 'Problema ao sincronizar.';
					}
					
					var pendencia = '<input type="checkbox" value="'+item.id+'" name="pendencias-mensagens[]" id="pendencias-mensagens-'+i+'" /><label for="pendencias-mensagens-'+i+'" ' + style_erro + '>'+item.id+' - '+date('d/m/Y H:i:s', item.timestamp)+(msg_erro ? ' - '+msg_erro : '')+'</label>';
					
					$('#lista-pendencias-mensagens').append('<li>'+pendencia+'</li>');
				}
			}else{
				$('#lista-pendencias-mensagens').append('<li>Nenhuma mensagem de pendência para sincronizar.</li>');
			}
			$("input[name='pendencias-mensagens[]']").button({ icons: {primary:'ui-icon-circle-close'} });
		});
	});
}

/**
* Metódo:		obter_pedidos_aguardando()
* 
* Descrição:	Função Utilizada para obter os pedidos pendentes ainda 
* 				não sincronizados pelo representante
* 
* Data:			09/11/2013
* Modificação:	09/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web / Robson
* 
*/
function obter_pedidos_aguardando()
{
	
	//obter_pedidos_envio_regiao(function(regioes){
		var where  = '';
		var sql = 'SELECT DISTINCT ' +
						' pedido_id_pedido, ' +
						' cliente_nome ' + 
					' FROM ' + 
					'	pedidos_pendentes ' + 
						
		//			' LEFT JOIN clientes ON ( ' +
						
		//			'	pedido_codigo_cliente = clientes.codigo AND ' +
		//			'	pedido_loja_cliente   = clientes.loja  ' +
		//			' )	 ' +
					 'WHERE  ' +
		//			' clientes.codigo_regiao_venda IN (\''+regioes.join('\',\'')+'\')  AND ' +
					' pedido_id_pedido != 0 AND (exportado IS NULL OR editado = 1 OR erro = 1) ' 
		
		
		db.transaction(function(x) {
			
			// Exibir Prospects
			x.executeSql(sql, [], function(x, dados){
				
				if(dados.rows.length)
				{
					$('#lista-pedidos_pendentes').empty();
				
					for (i = 0; i < dados.rows.length; i++)
					{
						var item = dados.rows.item(i);
						
						var style_erro = '';
						var msg_erro = '';
						if(item.erro == 1)
						{
							style_erro = ' class="erro" ';
							msg_erro = 'Problema ao sincronizar.';
						}
						
						var pedido = '<input type="checkbox" value="'+item.pedido_id_pedido+'" name="pedidos_pendentes[]" id="pedidos_pendentes-'+i+'" /><label for="pedidos_pendentes-'+i+'" ' + style_erro + '>' + item.pedido_id_pedido + ' - ' + item.cliente_nome + '</label>';
						
						$('#lista-pedidos_pendentes').append('<li>'+pedido+'</li>');
					}
				}else{
					$('#lista-pedidos_pendentes').append('<li>Nenhum pedido para sincronizar.</li>');
				}
				
				$("input[name='pedidos_pendentes[]']").button({ icons: {primary:'ui-icon-circle-close'} });
				
			}, function(transaction, error) {
			
			});
		});
	
	//});
}




/**
* Metódo:		obter_pedidos_aguardando()
* 
* Descrição:	Função Utilizada para obter os pedidos pendentes ainda 
* 				não sincronizados pelo representante
* 
* Data:			09/11/2013
* Modificação:	09/11/2013
* 
* @access		public
* @version		1.0
* @author 		DevelopWeb Soluções Web / Robson
* 
*/
function obter_regioes_vendas_aguardando()
{
	
	obter_pedidos_envio_regiao(function(regioes){
		console.log('regioes::::::: '+ JSON.stringify(regioes));
		
		var where  = '';
		var sql = 'SELECT * ' +
						
					' FROM ' + 
					'	regioes_vendas ' + 
						
					' WHERE ativa != \'C\' AND ' +
					' codigo IN (\''+regioes.join('\',\'')+'\')';
			console.log(sql);		 
		db.transaction(function(x) {
			// Exibir Prospects
			x.executeSql(sql, [], function(x, dados){
				if(dados.rows.length)
				{
					$('#lista-regioes_pendentes').empty();
				
					for (i = 0; i < dados.rows.length; i++)
					{
						var item = dados.rows.item(i);
						
						var style_erro = '';
						var msg_erro = '';
						if(item.erro == 1)
						{
							style_erro = ' class="erro" ';
							msg_erro = 'Problema ao sincronizar.';
						}
						
						var regiao = '<input type="checkbox" value="'+item.codigo+ '" name="regioes_pendentes[]" id="regioes_pendentes-'+i+'" /><label for="regioes_pendentes-'+i+'" ' + style_erro + '>' + item.codigo + ' - ' + item.nome + '</label>';
						
						$('#lista-regioes_pendentes').append('<li>'+regiao+'</li>');
					}
				}else{
					$('#lista-regioes_pendentes').append('<li>Nenhuma região para sincronizar.</li>');
				}
				
				$("input[name='regioes_pendentes[]']").button({ icons: {primary:'ui-icon-circle-close'} });
				
			}, function(transaction, error) {
				
			});
		});
	
	});
}